FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["PrintmillManagement/Server/PrintmillManagement.Server.csproj", "PrintmillManagement/Server/"]
COPY ["PrintmillManagement/Client/PrintmillManagement.Client.csproj", "PrintmillManagement/Client/"]
COPY ["PrintmillManagement/Modules/PrintmillManagement.Modules.csproj", "PrintmillManagement/Modules/"]
COPY ["PrintmillManagement/Infrastructure/PrintmillManagement.Infrastructure.csproj", "PrintmillManagement/Infrastructure/"]

RUN dotnet restore "PrintmillManagement/Server/PrintmillManagement.Server.csproj"
COPY . .
WORKDIR "/src/PrintmillManagement/Server"
RUN dotnet build "PrintmillManagement.Server.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "PrintmillManagement.Server.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "PrintmillManagement.Server.dll"]