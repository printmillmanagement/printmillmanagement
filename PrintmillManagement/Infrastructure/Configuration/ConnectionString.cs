﻿namespace PrintmillManagement.Infrastructure.Configuration
{
    public class ConnectionString : IAppConfiguration
    {
        public string PrintmillManagementDB { get; set; }
        public string UserManagementDB { get; set; }
    }
}
