﻿namespace PrintmillManagement.Infrastructure.Configuration
{
    public class AppSettingsBase : IAppConfiguration
    {
        public AppSettingsBase()
        { }

        public string ApplicationName { get; set; }
        public string ServerIP { get; set; }
        public string ServerName { get; set; }
    }
}
