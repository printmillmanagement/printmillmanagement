﻿namespace PrintmillManagement.Infrastructure.Configuration
{
    public class AppSettings
    {
        public AppSettingsBase AppSettingsBase { get; set; }
        public ConnectionString ConnectionString { get; set; }
        public AuthenticationSettings AuthenticationSettings { get; set; }
        public MailSettings MailSettings { get; set; }
    }
}
