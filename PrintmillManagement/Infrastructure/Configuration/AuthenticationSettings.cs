﻿namespace PrintmillManagement.Infrastructure.Configuration
{
    public class AuthenticationSettings : IAppConfiguration
    {
        public string JwtSecurityKey { get; set; }
        public string JwtIssuer { get; set; }
        public int JwtExpiryInDays { get; set; }
    }
}
