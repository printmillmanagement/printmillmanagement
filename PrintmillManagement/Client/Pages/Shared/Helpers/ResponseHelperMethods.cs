﻿using PrintmillManagement.Client.Pages.Shared.Models;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Shared.Helpers
{
    public class ResponseHelperMethods
    {
        /// <summary>
        /// Method for serializing Http Response Messages in the new model.
        /// </summary>
        /// <param name="response"><see cref="HttpResponseMessage"/>/param>
        /// <param name="errorMessage"><see cref="string"/>/param>
        /// <returns><see cref="ResponseViewModel"/>Contains serialized Http Response Messages for the further processing</returns>
        public static async Task<ResponseViewModel> SerializeHttpResponseMessage(HttpResponseMessage response, string errorMessage)
        {
            var responseViewModel = new ResponseViewModel
            {
                Success = false,
                Message = errorMessage
            };

            if (response.IsSuccessStatusCode)
            {
                try
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    responseViewModel = JsonSerializer.Deserialize<ResponseViewModel>(responseContent, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
                }
                catch (Exception)
                {
                    return responseViewModel;
                }
            }

            return responseViewModel;
        }
    }
}
