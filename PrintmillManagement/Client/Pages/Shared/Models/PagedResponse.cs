﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Shared.Models
{
    public class PagedResponse<T>
    {
        public PagedResponse()
        {
            Paging = new PagingResponse();
        }

        [JsonProperty(PropertyName = "items")]
        public ICollection<T> Items { get; set; }

        [JsonProperty(PropertyName = "paging")]
        public PagingResponse Paging { get; set; }
    }
}
