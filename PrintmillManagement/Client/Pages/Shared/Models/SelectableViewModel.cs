﻿using Newtonsoft.Json;

namespace PrintmillManagement.Client.Pages.Shared.Models
{
    public class SelectableViewModel
    {
        [JsonProperty(PropertyName = "key")]
        public int Key { get; set; }

        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        [JsonProperty(PropertyName = "pair")]
        public string Pair { get; set; }
    }
}
