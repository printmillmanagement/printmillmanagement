﻿using Newtonsoft.Json;

namespace PrintmillManagement.Client.Pages.Shared.Models
{
    public class PagingResponse
    {
        [JsonProperty(PropertyName = "orderBy")]
        public string OrderBy { get; set; }

        [JsonProperty(PropertyName = "orderDirection")]
        public string OrderDirection { get; set; }

        [JsonProperty(PropertyName = "page")]
        public int Page { get; set; }

        [JsonProperty(PropertyName = "pageSize")]
        public int PageSize { get; set; }

        [JsonProperty(PropertyName = "totalCount")]
        public int TotalCount { get; set; }

        [JsonProperty(PropertyName = "totalPages")]
        public int TotalPages { get; set; }
    }
}
