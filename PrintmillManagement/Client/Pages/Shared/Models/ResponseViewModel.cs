﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Shared.Models
{
    public class ResponseViewModel
    {
        public ResponseViewModel()
        {
            Errors = new List<KeyValuePair<string, string>>();
        }

        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public List<KeyValuePair<string, string>> Errors { get; set; }
    }
}
