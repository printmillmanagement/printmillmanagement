﻿using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Models;
using PrintmillManagement.Client.Pages.Shared.Helpers;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Actions
{
    public class DeliveryService : IDeliveryServiceProvider
    {
        private readonly string _apiUrlBase;

        private readonly HttpClient _httpClient;
        private readonly IJSRuntime _jsRuntime;

        public DeliveryService(
            HttpClient httpClient,
            IJSRuntime jsRuntime,
            IConfiguration configuration)
        {
            _httpClient = httpClient;
            _jsRuntime = jsRuntime;
            _apiUrlBase = configuration.GetSection("ApiBaseUrl").Value;
        }

        public async Task<DeliveryInitialDataView> SelectInitialDeliveryDataAsync(string userGuid)
        {
            var initialData = new DeliveryInitialDataView();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.SelectInitialDeliveryDataMethod}/{userGuid}";

            var response = await _httpClient.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                initialData = JsonSerializer.Deserialize<DeliveryInitialDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return initialData;
        }

        public async Task<ResponseViewModel> CreateDeliveryAsync(DeliveryCreateRequest request)
        {
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.CreateDeliveryMethod}";
            var requestContent = new StringContent(JsonSerializer.Serialize(request), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(apiUrl, requestContent);

            return await ResponseHelperMethods.SerializeHttpResponseMessage(response, "Error Ocurred while creating new Delivery.");
        }

        public async Task<PagedResponse<DeliveryDataView>> SelectAllDeliveriesByFilterAsync(int page, int pageSize, DeliveryFilterRequest request)
        {
            var deliveries = new PagedResponse<DeliveryDataView>();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.SelectAllDeliveriesByFilterMethod}/{page}/{pageSize}";
            var requestContent = new StringContent(JsonSerializer.Serialize(request), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(apiUrl, requestContent);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                deliveries = JsonSerializer.Deserialize<PagedResponse<DeliveryDataView>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return deliveries;
        }

        public async Task<DeliveryDetailedDataView> SelectDeliveryByGuidAsync(string deliveryGuid)
        {
            var delivery = new DeliveryDetailedDataView();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.SelectDeliveryByGuidMethod}/{deliveryGuid}";
            var response = await _httpClient.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                delivery = JsonSerializer.Deserialize<DeliveryDetailedDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return delivery;
        }
    }
}
