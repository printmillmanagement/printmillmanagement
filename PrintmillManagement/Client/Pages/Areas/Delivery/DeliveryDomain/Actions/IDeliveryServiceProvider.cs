﻿using PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Models;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Actions
{
    public interface IDeliveryServiceProvider
    {
        /// <summary>
        ///  Method that calls api resource for retrieving all initial data for Delivery.
        /// </summary>
        /// <param name="userGuid"><see cref="string"/>/param>
        /// <returns><see cref="DeliveryInitialDataView"/>Contains all data for retrieved Delivery</returns>
        Task<DeliveryInitialDataView> SelectInitialDeliveryDataAsync(string userGuid);

        /// <summary>
        /// Method that calls api resource for creating new Delivery.
        /// </summary>
        /// <param name="request"><see cref="DeliveryCreateRequest"/>/param>
        /// <returns><see cref="ResponseViewModel"/>Contains result from api resource if Delivery is created or not</returns>
        Task<ResponseViewModel> CreateDeliveryAsync(DeliveryCreateRequest request);

        /// <summary>
        ///  Method that calls api resource for retrieving all Deliveries by provided filter.
        /// </summary>
        /// <param name="page"><see cref="int"/>/param>
        /// <param name="pageSize"><see cref="int"/>/param>
        /// <param name="request"><see cref="DeliveryFilterRequest"/>/param>
        /// <returns><see cref="DeliveryDataView"/>Contains all data for Delivery as list</returns>
        Task<PagedResponse<DeliveryDataView>> SelectAllDeliveriesByFilterAsync(int page, int pageSize, DeliveryFilterRequest request);

        /// <summary>
        ///  Method that calls api resource for retrieving single Delivery details by provided Guid.
        /// </summary>
        /// <param name="deliveryGuid"><see cref="string"/>/param>
        /// <returns><see cref="DeliveryDetailedDataView"/>Contains all data for retrieved Delivery</returns>
        Task<DeliveryDetailedDataView> SelectDeliveryByGuidAsync(string deliveryGuid);
    }
}
