﻿namespace PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Constants
{
    public static class ClientUrls
    {
        public const string BaseUrl = "Delivery";
        public const string DeliveryCreate = "DeliveryCreate";
        public const string DeliveryView = "DeliveryView";
        public const string DeliveryDetails = "DeliveryDetails";
    }
}
