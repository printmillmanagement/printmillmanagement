﻿namespace PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Constants
{
    public static class ApiUrls
    {
        public const string ApiController = "/api/DeliveryAPI/";
        public const string SelectInitialDeliveryDataMethod = "SelectInitialDeliveryData";
        public const string CreateDeliveryMethod = "CreateDelivery";
        public const string SelectAllDeliveriesByFilterMethod = "SelectAllDeliveriesByFilter";
        public const string SelectDeliveryByGuidMethod = "SelectDeliveryByGuid";
    }
}
