﻿namespace PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Models
{
    public class DeliveryFilterRequest
    {
        public string Guid { get; set; }
        public int? CreatedByUserId { get; set; }
        public string CreatedByUserGuid { get; set; }
        public string CreatedByUsername { get; set; }
        public string CreatedForUserGuid { get; set; }
        public string CreatedForUsername { get; set; }
    }
}
