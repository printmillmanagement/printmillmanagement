﻿using Newtonsoft.Json;
using PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models;
using PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Models
{
    public class DeliveryInitialDataView
    {
        public DeliveryInitialDataView()
        {
            CreatedForUser = new UserDataView();
            Tickets = new List<TicketDataView>();
        }

        [JsonProperty(PropertyName = "createdForUser")]
        public UserDataView CreatedForUser { get; set; }

        [JsonProperty(PropertyName = "ticketDataView")]
        public List<TicketDataView> Tickets { get; set; }
    }
}
