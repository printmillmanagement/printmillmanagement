﻿using PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models;
using PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Models
{
    public class DeliveryDetailedDataView
    {
        public DeliveryDetailedDataView()
        {
            Delivery = new DeliveryDataView();
            CreatedByUser = new UserDataView();
            CreatedForUser = new UserDataView();
            Tickets = new List<TicketDataView>();
        }

        public DeliveryDataView Delivery { get; set; }
        public UserDataView CreatedByUser { get; set; }
        public UserDataView CreatedForUser { get; set; }
        public List<TicketDataView> Tickets { get; set; }
    }
}
