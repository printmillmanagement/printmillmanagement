﻿using Newtonsoft.Json;
using PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Models
{
    public class DeliveryCreateRequest
    {
        public DeliveryCreateRequest()
        {
            Tickets = new List<TicketDataView>();
        }

        [JsonProperty(PropertyName = "createdForUserId")]
        public int CreatedForUserId { get; set; }

        [JsonProperty(PropertyName = "ticketDataView")]
        public List<TicketDataView> Tickets { get; set; }
    }
}
