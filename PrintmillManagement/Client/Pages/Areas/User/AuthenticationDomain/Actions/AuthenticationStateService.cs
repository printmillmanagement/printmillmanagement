﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Helpers;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Actions
{
    public class AuthenticationStateService : AuthenticationStateProvider, IAuthenticationStateServiceProvider
    {
        private readonly HttpClient _httpClient;
        private readonly ILocalStorageService _localStorage;
        private readonly AuthenticationState _anonymous;

        public AuthenticationStateService(
            HttpClient httpClient, 
            ILocalStorageService localStorage)
        {
            this._httpClient = httpClient;
            this._localStorage = localStorage;
            _anonymous = new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()));
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {

            string token = String.Empty;

            var expiry = await _localStorage.GetItemAsync<string>(AuthenticationConstants.AuthTokenExpiry);
            if (expiry != null)
            {
                if(DateTime.Parse(expiry.ToString()) > DateTime.Now)
                {
                    token = await _localStorage.GetItemAsync<string>(AuthenticationConstants.AuthTokenName);
                }
                else
                {
                    await _localStorage.RemoveItemAsync(AuthenticationConstants.AuthTokenName);
                    await _localStorage.RemoveItemAsync(AuthenticationConstants.AuthTokenExpiry);
                }
            }

            if (string.IsNullOrWhiteSpace(token)) return _anonymous;

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(AuthenticationConstants.Bearer, token);

            return new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity(JwtParser.ParseClaimsFromJwt(token), AuthenticationConstants.AuthType)));
        }

        public void NotifyUserAuthentication(string token)
        {
            var authenticatedUser = new ClaimsPrincipal(new ClaimsIdentity(JwtParser.ParseClaimsFromJwt(token), AuthenticationConstants.AuthType));
            var authState = Task.FromResult(new AuthenticationState(authenticatedUser));
            NotifyAuthenticationStateChanged(authState);
        }

        public void NotifyUserLogout()
        {
            var authState = Task.FromResult(_anonymous);
            NotifyAuthenticationStateChanged(authState);
        }
    }
}
