﻿using Microsoft.AspNetCore.Components.Authorization;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Actions
{
    public interface IAuthenticationStateServiceProvider
    {
        Task<AuthenticationState> GetAuthenticationStateAsync();
        void NotifyUserAuthentication(string token);
        void NotifyUserLogout();
    }
}
