﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Configuration;
using PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Helpers;
using PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Models;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Actions
{
    public class AuthenticationService : IAuthenticationServiceProvider
    {
        public bool IsLoggedIn { get; private set; }
        private readonly string _apiUrlBase;

        private readonly HttpClient _httpClient;
        private readonly AuthenticationStateProvider _authenticationStateProvider;
        private readonly ILocalStorageService _localStorage;       

        public AuthenticationService(
            HttpClient httpClient,
            AuthenticationStateProvider authenticationStateProvider,
            ILocalStorageService localStorage,
            IConfiguration configuration)
        {
            _httpClient = httpClient;
            _authenticationStateProvider = authenticationStateProvider;
            _localStorage = localStorage;
            _apiUrlBase = configuration.GetSection("ApiBaseUrl").Value;
        }

        public async Task<AuthenticationResponse> LoginAsync(AuthenticationRequest request)
        {
            var content = JsonSerializer.Serialize(request);
            var bodyContent = new StringContent(content, Encoding.UTF8, "application/json");
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.AuthenticationMethod}";

            var authResult = await _httpClient.PostAsync(apiUrl, bodyContent);
            if (!authResult.IsSuccessStatusCode) return new AuthenticationResponse { Success = false };

            var authContent = await authResult.Content.ReadAsStringAsync();
            var result = JsonSerializer.Deserialize<AuthenticationResponse>(authContent, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            if (!result.Success) return new AuthenticationResponse { Success = result.Success, Message = result.Message };

            await _localStorage.SetItemAsync(AuthenticationConstants.AuthTokenName, result.Data.Token);
            await _localStorage.SetItemAsync(AuthenticationConstants.AuthTokenExpiry, result.Data.Expiry);

            ((IAuthenticationStateServiceProvider)_authenticationStateProvider).NotifyUserAuthentication(result.Data.Token);
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(AuthenticationConstants.Bearer, result.Data.Token);

            return new AuthenticationResponse { Success = result.Success };
        }

        public async Task LogoutAsync()
        {
            await _localStorage.RemoveItemAsync(AuthenticationConstants.AuthTokenName);
            await _localStorage.RemoveItemAsync(AuthenticationConstants.AuthTokenExpiry);

            ((IAuthenticationStateServiceProvider)_authenticationStateProvider).NotifyUserLogout();
            _httpClient.DefaultRequestHeaders.Authorization = null;
        }

        public async Task<string> GetUserRole()
        {
            var jwtToken = await _localStorage.GetItemAsync<string>(AuthenticationConstants.AuthTokenName);
            if (string.IsNullOrEmpty(jwtToken)) return string.Empty;

            var claims = JwtParser.ParseClaimsFromJwt(jwtToken);
            return claims.Where(x => x.Type == ClaimTypes.Role).Select(y => y.Value).SingleOrDefault();
        }
    }
}
