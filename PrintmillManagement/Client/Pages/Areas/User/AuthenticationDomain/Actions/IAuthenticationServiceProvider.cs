﻿using PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Actions
{
    public interface IAuthenticationServiceProvider
    {
        Task<AuthenticationResponse> LoginAsync(AuthenticationRequest request);
        Task LogoutAsync();
        Task<string> GetUserRole();
    }
}
