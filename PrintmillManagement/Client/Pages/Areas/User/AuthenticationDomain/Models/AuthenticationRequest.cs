﻿using System.ComponentModel.DataAnnotations;

namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Models
{
    public class AuthenticationRequest
    {
        [Required]
        [EmailAddress]
        [StringLength(50, ErrorMessage = "Email is too long. Maximum length are {1} characters.")]
        public string Email { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Password is too long. Maximum length are {1} characters.")]
        public string Password { get; set; }
    }
}
