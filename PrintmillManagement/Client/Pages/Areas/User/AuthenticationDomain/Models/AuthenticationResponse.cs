﻿using Newtonsoft.Json;

namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Models
{
    public class AuthenticationResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "data")]
        public JwtToken Data { get; set; }
    }
}
