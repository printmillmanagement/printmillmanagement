﻿using Newtonsoft.Json;
using System;

namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Models
{
    public class JwtToken
    {
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }
        [JsonProperty(PropertyName = "expiry")]
        public DateTime Expiry { get; set; }
    }
}
