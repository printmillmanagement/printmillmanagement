﻿namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Constants
{
    public static class AuthenticationConstants
    {
        public const string AuthTokenName = "authToken";
        public const string Bearer = "Bearer";
        public const string AuthType = "jwtAuthType";
        public const string AuthTokenExpiry = "authTokenExpiry";
    }
}
