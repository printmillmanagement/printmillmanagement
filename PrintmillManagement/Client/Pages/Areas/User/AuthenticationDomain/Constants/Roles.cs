﻿namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Constants
{
    public static class Roles
    {
        public const string SUPERUSER = "SUPERUSER";
        public const string ADMIN = "ADMIN";
        public const string EMPLOYEE = "EMPLOYEE";
        public const string CUSTOMER = "CUSTOMER"; 

        public const string ADMINOREMPLOYEE = ADMIN + "," + EMPLOYEE;
    }
}
