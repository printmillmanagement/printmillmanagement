﻿namespace PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Constants
{
    public static class ApiUrls
    {
        public const string ApiController = "/api/UserAuthenticationAPI/";
        public const string AuthenticationMethod = "Authentication";
    }
}
