﻿namespace PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models
{
    public class UserFilterRequest
    {
        public string Guid { get; set; }
        public string Uid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
