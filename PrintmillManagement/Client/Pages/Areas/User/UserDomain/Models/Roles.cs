﻿namespace PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models
{
    public enum Roles
    {
        SUPERUSER = 1,
        ADMIN = 2,
        EMPLOYEE = 3,
        CUSTOMER = 4
    }
}
