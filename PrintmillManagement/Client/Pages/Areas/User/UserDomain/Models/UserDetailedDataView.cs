﻿namespace PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models
{
    public class UserDetailedDataView
    {
        public UserDetailedDataView()
        {
            User = new UserDataView();
        }

        public UserDataView User { get; set; }
    }
}
