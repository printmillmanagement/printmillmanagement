﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models
{
    public class UserCreateRequest
    {
        [JsonProperty(PropertyName = "firstName")]
        [StringLength(50, ErrorMessage = "Must be between 2 and 50 characters", MinimumLength = 2)]
        [Required(ErrorMessage = "Value for First Name field is required.")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastName")]
        [StringLength(50, ErrorMessage = "Must be between 2 and 50 characters", MinimumLength = 2)]
        [Required(ErrorMessage = "Value for Last Name field is required.")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "email")]
        [StringLength(50, ErrorMessage = "Must be between 7 and 50 characters", MinimumLength = 7)]
        [Required(ErrorMessage = "Value for Email field is required.")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "phoneNumber")]
        [StringLength(14, ErrorMessage = "Must be between 5 and 14 characters", MinimumLength = 5)]
        [Required(ErrorMessage = "Value for Phone Number field is required and must be in E.164 standard.")]
        public string PhoneNumber { get; set; }

        [JsonProperty(PropertyName = "role")]
        [StringLength(10, ErrorMessage = "Must be between 5 and 10 characters", MinimumLength = 5)]
        [Required(ErrorMessage = "Value for Role field is required.")]
        public string Role { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(50, ErrorMessage = "Must be between 7 and 50 characters", MinimumLength = 7)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(50, ErrorMessage = "Must be between 7 and 50 characters", MinimumLength = 7)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}
