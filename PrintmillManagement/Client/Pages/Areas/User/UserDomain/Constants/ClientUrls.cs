﻿namespace PrintmillManagement.Client.Pages.Areas.User.UserDomain.Constants
{
    public static class ClientUrls
    {
        public const string BaseUrl = "User";
        public const string UserDetails = "UserDetails";
        public const string UserView = "UserView";
        public const string UserCreate = "UserCreate";
        public const string UserEdit = "UserEdit";
        public const string UserChangePassword = "UserChangePassword";
    }
}
