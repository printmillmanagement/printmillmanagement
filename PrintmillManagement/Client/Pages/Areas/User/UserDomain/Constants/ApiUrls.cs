﻿namespace PrintmillManagement.Client.Pages.Areas.User.UserDomain.Constants
{
    public static class ApiUrls
    {
        public const string ApiController = "/api/UserAPI/";
        public const string CreateUserMethod = "CreateUser";
        public const string GetAllUsersMethod = "GetAllUsers";
        public const string GetUserByGuidMethod = "GetUserByGuid";
    }
}
