﻿using PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.User.UserDomain.Actions
{
    public interface IUserServiceProvider
    {
        /// <summary>
        /// Method that calls api resource for creating new user.
        /// </summary>
        /// <param name="request"><see cref="UserCreateRequest"/>/param>
        /// <returns><see cref="ResponseViewModel"/>Contains result from api resource if user is created or not</returns>
        Task<ResponseViewModel> CreateUserAsync(UserCreateRequest request);

        /// <summary>
        ///  Method that calls api resource for retrieving all users by provided filter.
        /// </summary>
        /// <param name="page"><see cref="int"/>/param>
        /// <param name="pageSize"><see cref="int"/>/param>
        /// <param name="request"><see cref="UserFilterRequest"/>/param>
        /// <returns><see cref="UserDataView"/>Contains all data for users as list</returns>
        Task<PagedResponse<UserDataView>> GetAllUsersByFilterAsync(int page, int pageSize, UserFilterRequest request);

        /// <summary>
        ///  Method that calls api resource for retrieving single user details by provided Guid.
        /// </summary>
        /// <param name="userGuid"><see cref="string"/>/param>
        /// <returns><see cref="UserDetailedDataView"/>Contains all data for retrieved user</returns>
        Task<UserDetailedDataView> GetUserByGuidAsync(string userGuid);
    }
}
