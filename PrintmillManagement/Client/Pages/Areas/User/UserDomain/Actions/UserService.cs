﻿using Microsoft.Extensions.Configuration;
using PrintmillManagement.Client.Pages.Areas.User.UserDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models;
using PrintmillManagement.Client.Pages.Shared.Helpers;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.User.UserDomain.Actions
{
    public class UserService : IUserServiceProvider
    {
        private readonly string _apiUrlBase;

        private readonly HttpClient _httpClient;

        public UserService(
            HttpClient httpClient,
            IConfiguration configuration)
        {
            _httpClient = httpClient;
            _apiUrlBase = configuration.GetSection("ApiBaseUrl").Value;
        }

        public async Task<ResponseViewModel> CreateUserAsync(UserCreateRequest userCreateRequest)
        {
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.CreateUserMethod}";
            var requestContent = new StringContent(JsonSerializer.Serialize(userCreateRequest), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(apiUrl, requestContent);

            return await ResponseHelperMethods.SerializeHttpResponseMessage(response, "Error Ocurred while creating new User.");
        }

        public async Task<PagedResponse<UserDataView>> GetAllUsersByFilterAsync(int page, int pageSize, UserFilterRequest request)
        {
            var users = new PagedResponse<UserDataView>();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetAllUsersMethod}/{page}/{pageSize}";
            var requestContent = new StringContent(JsonSerializer.Serialize(request), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(apiUrl, requestContent);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                users = JsonSerializer.Deserialize<PagedResponse<UserDataView>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return users;
        }

        public async Task<UserDetailedDataView> GetUserByGuidAsync(string userGuid)
        {
            var user = new UserDetailedDataView();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetUserByGuidMethod}/{userGuid}";
            var response = await _httpClient.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                user = JsonSerializer.Deserialize<UserDetailedDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return user;
        }
    }
}
