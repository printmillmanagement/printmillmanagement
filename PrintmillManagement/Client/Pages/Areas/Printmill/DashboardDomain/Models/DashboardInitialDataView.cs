﻿namespace PrintmillManagement.Client.Pages.Areas.Printmill.DashboardDomain.Models
{
    public class DashboardInitialDataView
    {
        public int OpenTicketsCount { get; set; }
        public int AcceptedTicketsCount { get; set; }
        public int InProgressTicketsCount { get; set; }
        public int FinishedTicketsCount { get; set; }
        public int NeedsAdditionalWorkTicketsCount { get; set; }
        public int InDeliveryTicketsCount { get; set; }
    }
}
