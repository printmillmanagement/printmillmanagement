﻿namespace PrintmillManagement.Client.Pages.Areas.Printmill.DashboardDomain.Constants
{
    public static class ApiUrls
    {
        public const string ApiController = "/api/DashboardAPI/";
        public const string SelectInitialDashboardDataMethod = "SelectInitialDashboardData";
    }
}
