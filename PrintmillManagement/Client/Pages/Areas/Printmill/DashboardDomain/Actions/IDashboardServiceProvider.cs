﻿using PrintmillManagement.Client.Pages.Areas.Printmill.DashboardDomain.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.DashboardDomain.Actions
{
    public interface IDashboardServiceProvider
    {
        /// <summary>
        ///  Method that calls api resource for retrieving all initial data for dashboard.
        /// </summary>
        /// <returns><see cref="DashboardInitialDataView"/>Contains all data for retrieved dashboard</returns>
        Task<DashboardInitialDataView> SelectInitialDashboardDataAsync();
    }
}
