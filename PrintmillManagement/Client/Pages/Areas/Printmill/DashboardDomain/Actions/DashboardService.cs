﻿using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using PrintmillManagement.Client.Pages.Areas.Printmill.DashboardDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.Printmill.DashboardDomain.Models;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.DashboardDomain.Actions
{
    public class DashboardService : IDashboardServiceProvider
    {
        private readonly string _apiUrlBase;

        private readonly HttpClient _httpClient;
        private readonly IJSRuntime _jsRuntime;

        public DashboardService(
            HttpClient httpClient,
            IJSRuntime jsRuntime,
            IConfiguration configuration)
        {
            _httpClient = httpClient;
            _jsRuntime = jsRuntime;
            _apiUrlBase = configuration.GetSection("ApiBaseUrl").Value;
        }

        public async Task<DashboardInitialDataView> SelectInitialDashboardDataAsync()
        {
            var initialData = new DashboardInitialDataView();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.SelectInitialDashboardDataMethod}";

            var response = await _httpClient.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                initialData = JsonSerializer.Deserialize<DashboardInitialDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return initialData;
        }
    }
}
