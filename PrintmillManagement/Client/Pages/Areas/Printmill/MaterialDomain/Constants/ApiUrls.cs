﻿
namespace PrintmillManagement.Client.Pages.Areas.Printmill.MaterialDomain.Constants
{
    public static class ApiUrls
    {
        public const string ApiController = "/api/MaterialAPI/";
        public const string GetMaterialsByServiceTypeIdData = "GetMaterialsByServiceTypeId";
    }
}
