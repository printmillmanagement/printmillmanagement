﻿using PrintmillManagement.Client.Pages.Areas.Printmill.MaterialDomain.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.MaterialDomain.Actions
{
    public interface IMaterialServiceProvider
    {
        Task<MaterialDataView> GetMaterialsByServiceTypeIdAsync(int serviceTypeId);
    }
}
