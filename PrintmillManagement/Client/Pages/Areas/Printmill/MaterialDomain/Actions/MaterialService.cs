﻿using Microsoft.Extensions.Configuration;
using PrintmillManagement.Client.Pages.Areas.Printmill.MaterialDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.Printmill.MaterialDomain.Models;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.MaterialDomain.Actions
{
    public class MaterialService : IMaterialServiceProvider
    {
        public bool Success { get; private set; } = false;
        public string Message { get; private set; }
        private readonly string _apiUrlBase;

        private readonly HttpClient _httpClient;

        public MaterialService(
            HttpClient httpClient,
            IConfiguration configuration)
        {
            _httpClient = httpClient;
            _apiUrlBase = configuration.GetSection("ApiBaseUrl").Value;
        }

        public async Task<MaterialDataView> GetMaterialsByServiceTypeIdAsync(int serviceTypeId)
        {
            var materialDataView = new MaterialDataView();
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetMaterialsByServiceTypeIdData}/{serviceTypeId}";

            var response = await _httpClient.GetAsync(apiUrl);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                materialDataView = JsonSerializer.Deserialize<MaterialDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }
            
            return materialDataView;
        }
    }
}
