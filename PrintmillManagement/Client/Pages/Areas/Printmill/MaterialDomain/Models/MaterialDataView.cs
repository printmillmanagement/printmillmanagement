﻿using Newtonsoft.Json;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.MaterialDomain.Models
{
    public class MaterialDataView
    {
        public MaterialDataView()
        {
            Materials = new List<SelectableViewModel>();
        }

        [JsonProperty(PropertyName = "materials")]
        public List<SelectableViewModel> Materials { get; set; }
    }
}
