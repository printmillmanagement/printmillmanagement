﻿using PrintmillManagement.Client.Pages.Areas.Printmill.ServiceTypeDomain.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.ServiceTypeDomain.Actions
{
    public interface IServiceTypeServiceProvider
    {
        Task<ServiceTypeDataView> GetAllServiceTypesAsync();
    }
}
