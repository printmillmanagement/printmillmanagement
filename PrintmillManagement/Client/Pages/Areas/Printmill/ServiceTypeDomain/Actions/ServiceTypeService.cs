﻿using Microsoft.Extensions.Configuration;
using PrintmillManagement.Client.Pages.Areas.Printmill.ServiceTypeDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.Printmill.ServiceTypeDomain.Models;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.ServiceTypeDomain.Actions
{
    public class ServiceTypeService : IServiceTypeServiceProvider
    {
        public bool Success { get; private set; } = false;
        public string Message { get; private set; }
        private readonly string _apiUrlBase;

        private readonly HttpClient _httpClient;

        public ServiceTypeService(
            HttpClient httpClient,
            IConfiguration configuration)
        {
            _httpClient = httpClient;
            _apiUrlBase = configuration.GetSection("ApiBaseUrl").Value;
        }

        public async Task<ServiceTypeDataView> GetAllServiceTypesAsync()
        {
            var serviceTypeDataView = new ServiceTypeDataView();
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetAllServicesMethod}";

            var response = await _httpClient.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                serviceTypeDataView = JsonSerializer.Deserialize<ServiceTypeDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }
            
            return serviceTypeDataView;
        }
    }
}
