﻿namespace PrintmillManagement.Client.Pages.Areas.Printmill.ServiceTypeDomain.Constants
{
    public static class ApiUrls
    {
        public const string ApiController = "/api/ServiceTypeAPI/";
        public const string GetAllServicesMethod = "GetAllServices";
    }
}
