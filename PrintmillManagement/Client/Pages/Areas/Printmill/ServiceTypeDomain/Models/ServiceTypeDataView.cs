﻿using Newtonsoft.Json;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.ServiceTypeDomain.Models
{
    public class ServiceTypeDataView
    {
        public ServiceTypeDataView()
        {
            ServiceTypes = new List<SelectableViewModel>();
        }

        [JsonProperty(PropertyName = "serviceTypes")]
        public List<SelectableViewModel> ServiceTypes { get; set; }
    }
}
