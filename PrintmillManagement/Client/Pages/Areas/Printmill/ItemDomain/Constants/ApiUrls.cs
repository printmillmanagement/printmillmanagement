﻿namespace PrintmillManagement.Client.Pages.Areas.Printmill.ItemDomain.Constants
{
    public static class ApiUrls
    {
        public const string ApiController = "/api/ItemAPI/";
        public const string GetAllItemsDataMethod = "GetAllItems";
        public const string GetItemsByServiceTypeAndMaterialTypeMethod = "GetItemsByServiceTypeAndMaterialType";
    }
}
