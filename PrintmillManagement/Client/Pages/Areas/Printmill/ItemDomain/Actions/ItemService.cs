﻿using Microsoft.Extensions.Configuration;
using PrintmillManagement.Client.Pages.Areas.Printmill.ItemDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.Printmill.ItemDomain.Models;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.ItemDomain.Actions
{
    public class ItemService : IItemServiceProvider
    {
        public bool Success { get; private set; } = false;
        public string Message { get; private set; }
        private readonly string _apiUrlBase;

        private readonly HttpClient _httpClient;

        public ItemService(
            HttpClient httpClient,
            IConfiguration configuration)
        {
            _httpClient = httpClient;
            _apiUrlBase = configuration.GetSection("ApiBaseUrl").Value;
        }

        public async Task<ItemDataView> GetAllItemsAsync()
        {
            var itemDataView = new ItemDataView();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetAllItemsDataMethod}";

            var response = await _httpClient.GetAsync(apiUrl);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                itemDataView = JsonSerializer.Deserialize<ItemDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }
            
            return itemDataView;
        }

        public async Task<ItemDataView> GetItemsByServiceTypeAndMaterialTypeAsync(int serviceTypeId, int materialTypeId)
        {
            var itemDataView = new ItemDataView();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetItemsByServiceTypeAndMaterialTypeMethod}/{serviceTypeId}/{materialTypeId}";

            var response = await _httpClient.GetAsync(apiUrl);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                itemDataView = JsonSerializer.Deserialize<ItemDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return itemDataView;
        }
    }
}
