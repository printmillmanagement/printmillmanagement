﻿using PrintmillManagement.Client.Pages.Areas.Printmill.ItemDomain.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.ItemDomain.Actions
{
    public interface IItemServiceProvider
    {
        Task<ItemDataView> GetAllItemsAsync();
        Task<ItemDataView> GetItemsByServiceTypeAndMaterialTypeAsync(int serviceTypeId, int materialTypeId);
    }
}
