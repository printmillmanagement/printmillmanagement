﻿using Newtonsoft.Json;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.ItemDomain.Models
{
    public class ItemDataView
    {
        public ItemDataView()
        {
            Items = new List<SelectableViewModel>();
        }

        [JsonProperty(PropertyName = "items")]
        public List<SelectableViewModel> Items { get; set; }
    }
}
