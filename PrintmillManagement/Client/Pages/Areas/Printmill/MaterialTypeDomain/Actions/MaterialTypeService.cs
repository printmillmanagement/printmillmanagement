﻿using Microsoft.Extensions.Configuration;
using PrintmillManagement.Client.Pages.Areas.Printmill.MaterialTypeDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.Printmill.MaterialTypeDomain.Models;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.MaterialTypeDomain.Actions
{
    public class MaterialTypeService : IMaterialTypeServiceProvider
    {
        public bool Success { get; private set; } = false;
        public string Message { get; private set; }
        private readonly string _apiUrlBase;

        private readonly HttpClient _httpClient;

        public MaterialTypeService(
            HttpClient httpClient,
            IConfiguration configuration)
        {
            _httpClient = httpClient;
            _apiUrlBase = configuration.GetSection("ApiBaseUrl").Value;
        }

        public async Task<MaterialTypeDataView> GetMaterialTypesByMaterialIdAsync(int materialId)
        {
            var materialTypeDataView = new MaterialTypeDataView();
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetMaterialTypesByMaterialIdMethod}/{materialId}";

            var response = await _httpClient.GetAsync(apiUrl);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                materialTypeDataView = JsonSerializer.Deserialize<MaterialTypeDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }
           
            return materialTypeDataView;
        }
    }
}
