﻿using PrintmillManagement.Client.Pages.Areas.Printmill.MaterialTypeDomain.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.MaterialTypeDomain.Actions
{
    public interface IMaterialTypeServiceProvider
    {
        Task<MaterialTypeDataView> GetMaterialTypesByMaterialIdAsync(int materialId);
    }
}
