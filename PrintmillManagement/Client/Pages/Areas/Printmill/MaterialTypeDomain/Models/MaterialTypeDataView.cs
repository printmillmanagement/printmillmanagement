﻿using Newtonsoft.Json;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.MaterialTypeDomain.Models
{
    public class MaterialTypeDataView
    {
        public MaterialTypeDataView()
        {
            MaterialTypes = new List<SelectableViewModel>();
        }

        [JsonProperty(PropertyName = "materialTypes")]
        public List<SelectableViewModel> MaterialTypes { get; set; }
    }
}
