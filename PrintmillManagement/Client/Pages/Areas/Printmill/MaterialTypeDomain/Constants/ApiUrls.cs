﻿namespace PrintmillManagement.Client.Pages.Areas.Printmill.MaterialTypeDomain.Constants
{
    public static class ApiUrls
    {
        public const string ApiController = "/api/MaterialTypeAPI/";
        public const string GetMaterialTypesByMaterialIdMethod = "GetMaterialTypesByMaterialId";
    }
}
