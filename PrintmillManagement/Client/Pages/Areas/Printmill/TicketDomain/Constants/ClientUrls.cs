﻿namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Constants
{
    public static class ClientUrls
    {
        public const string BaseUrl = "Ticket";
        public const string TicketCreate = "TicketCreate";
        public const string TicketDetails = "TicketDetails";
        public const string TicketView = "TicketView";
        public const string TicketViewCustomer = "TicketViewCustomer";
        public const string TicketDetailsCustomer = "TicketDetailsCustomer";
        public const string TicketEditCustomer = "TicketEditCustomer";
    }
}
