﻿namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Constants
{
    public static class ApiUrls
    {
        public const string ApiController = "/api/TicketAPI/";
        public const string CreateTicketMethod = "CreateTicket";
        public const string EditTicketMethod = "EditTicket";
        public const string GetAllTicketsByUserCreatedMethod = "GetAllTicketsByUserCreated";
        public const string GetAllTicketsByUserProcessedMethod = "GetAllTicketsByUserProcessed";
        public const string ImportSolvedTicketMethod = "ImportSolvedTicket";
        public const string AcceptTicketMethod = "AcceptTicket";
        public const string GetTicketByGuidMethod = "GetTicketByGuid";
        public const string DownloadDocumentMethod = "DownloadDocument";
        public const string GetInitialFilterDataMethod = "GetInitialFilterData";
        public const string UpdateTicketStatusMethod = "UpdateTicketStatus";
        public const string DeleteTicketMethod = "DeleteTicket";
    }
}
