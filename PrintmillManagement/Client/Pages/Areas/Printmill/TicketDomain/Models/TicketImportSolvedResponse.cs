﻿using Newtonsoft.Json;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public class TicketImportSolvedResponse
    {
        [JsonProperty(PropertyName = "guid")]
        public string Guid { get; set; }

        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }
    }
}
