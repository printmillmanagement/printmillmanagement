﻿using Newtonsoft.Json;
using PrintmillManagement.Client.Pages.Areas.Printmill.DocumentDomain.Models;
using PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public class TicketDetailedDataView
    {
        public TicketDetailedDataView()
        {
            Ticket = new TicketDataView();
            Document = new DocumentDataView();
            CreatedByUser = new UserDataView();
        }

        [JsonProperty(PropertyName = "ticket")]
        public TicketDataView Ticket { get; set; }

        [JsonProperty(PropertyName = "document")]
        public DocumentDataView Document { get; set; }

        [JsonProperty(PropertyName = "createdByUser")]
        public UserDataView CreatedByUser { get; set; }
    }
}
