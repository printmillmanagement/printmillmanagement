﻿using Newtonsoft.Json;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public class TicketInitialFilterDataView
    {
        public TicketInitialFilterDataView()
        {
            ItemsData = new List<SelectableViewModel>();
            MaterialsData = new List<SelectableViewModel>();
            MaterialTypesData = new List<SelectableViewModel>();
            ServiceTypeData = new List<SelectableViewModel>();
        }

        [JsonProperty(PropertyName = "itemsData")]
        public List<SelectableViewModel> ItemsData { get; set; }
        
        [JsonProperty(PropertyName = "materialsData")]
        public List<SelectableViewModel> MaterialsData { get; set; }
        
        [JsonProperty(PropertyName = "materialTypesData")] 
        public List<SelectableViewModel> MaterialTypesData { get; set; }
        
        [JsonProperty(PropertyName = "serviceTypeData")]
        public List<SelectableViewModel> ServiceTypeData { get; set; }
    }
}
