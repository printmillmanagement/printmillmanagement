﻿using System.ComponentModel.DataAnnotations;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public enum TicketStatus
    {
        [Display(Name = "Open")]
        Open = 1,

        [Display(Name = "Accepted")]
        Accepted = 2,

        [Display(Name = "In Progress")]
        InProgress = 3,

        [Display(Name = "Finished")]
        Finished = 4,

        [Display(Name = "Needs Additional Work")]
        NeedsAdditionalWork = 5,

        [Display(Name = "In Delivery")]
        InDelivery = 6
    }
}
