﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public class TicketCreateRequest
    {
        public TicketCreateRequest()
        {
            Priority = TicketPriority.Low.ToString();
        }

        [JsonProperty(PropertyName = "serviceTypeId")]
        [Required(ErrorMessage = "Value for Service Type field is required.")]
        public string ServiceTypeId { get; set; }

        [JsonProperty(PropertyName = "materialId")]
        [Required(ErrorMessage = "Value for Material field is required.")]
        public string MaterialId { get; set; }

        [JsonProperty(PropertyName = "materialTypeId")]
        [Required(ErrorMessage = "Value for Material Type field is required.")]
        public string MaterialTypeId { get; set; }

        [JsonProperty(PropertyName = "itemId")]
        [Required(ErrorMessage = "Value for Item field is required.")]
        public string ItemId { get; set; }

        [JsonProperty(PropertyName = "numberOfUnits")]
        [Required(ErrorMessage = "Value for Number Of Units field is required.")]
        [Range(1, 32, ErrorMessage = "Value for Number Of Units field must be between {1} and {2}.")]
        public string NumberOfUnits { get; set; }

        [JsonProperty(PropertyName = "priority")]
        [Required(ErrorMessage = "Value for Priority field is required.")]
        public string Priority { get; set; }

        [JsonProperty(PropertyName = "description")]
        [StringLength(1000, ErrorMessage = "Description is too long. Maximum length are {1} characters.")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "note")]
        [StringLength(1000, ErrorMessage = "Note is too long. Maximum length are {1} characters.")]
        public string Note { get; set; }

        [JsonProperty(PropertyName = "stream")]
        [Required(ErrorMessage = "File must be uploaded.")]
        public Stream Stream { get; set; }

        [JsonProperty(PropertyName = "fileName")]
        [Required(ErrorMessage = "File must be uploaded.")]
        public string FileName { get; set; }
    }
}
