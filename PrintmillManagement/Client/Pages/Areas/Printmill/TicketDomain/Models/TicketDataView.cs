﻿using Newtonsoft.Json;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public class TicketDataView
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "guid")]
        public string Guid { get; set; }

        [JsonProperty(PropertyName = "createdByUserId")]
        public int CreatedByUserId { get; set; }

        [JsonProperty(PropertyName = "processedByUserId")]
        public int? ProcessedByUserId { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "note")]
        public string Note { get; set; }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        [JsonProperty(PropertyName = "dateCreated")]
        public string DateCreated { get; set; }

        [JsonProperty(PropertyName = "priority")]
        public string Priority { get; set; }

        [JsonProperty(PropertyName = "priorityId")]
        public int PriorityId { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "statusId")]
        public int StatusId { get; set; }

        [JsonProperty(PropertyName = "serviceTypeId")]
        public int ServiceTypeId { get; set; }

        [JsonProperty(PropertyName = "serviceType")]
        public string ServiceType { get; set; }

        [JsonProperty(PropertyName = "materialId")]
        public int MaterialId { get; set; }

        [JsonProperty(PropertyName = "material")]
        public string Material { get; set; }

        [JsonProperty(PropertyName = "materialTypeId")]
        public int MaterialTypeId { get; set; }

        [JsonProperty(PropertyName = "materialType")]
        public string MaterialType { get; set; }

        [JsonProperty(PropertyName = "itemId")]
        public int ItemId { get; set; }

        [JsonProperty(PropertyName = "item")]
        public string Item { get; set; }

        [JsonProperty(PropertyName = "numberOfUnits")]
        public int NumberOfUnits { get; set; }
    }
}
