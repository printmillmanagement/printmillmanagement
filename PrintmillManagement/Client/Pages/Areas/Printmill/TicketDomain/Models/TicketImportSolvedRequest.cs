﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public class TicketImportSolvedRequest
    {
        [Required]
        [JsonProperty(PropertyName = "stream")]
        public Stream Stream { get; set; }

        [Required]
        [JsonProperty(PropertyName = "fileName")]
        public string FileName { get; set; }
    }
}
