﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public class TicketEditRequest
    {
        [JsonProperty(PropertyName = "numberOfUnits")]
        [Required(ErrorMessage = "Value for Number Of Units field is required.")]
        [Range(1, 32, ErrorMessage = "Value for Number Of Units field must be between {1} and {2}.")]
        public string NumberOfUnits { get; set; }

        [JsonProperty(PropertyName = "description")]
        [StringLength(1000, ErrorMessage = "Description is too long. Maximum length are {1} characters.")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "note")]
        [StringLength(1000, ErrorMessage = "Note is too long. Maximum length are {1} characters.")]
        public string Note { get; set; }
    }
}
