﻿using Newtonsoft.Json;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public class TicketFilterRequest
    {
        [JsonProperty(PropertyName = "ticketGuid")]
        public string TicketGuid { get; set; }

        [JsonProperty(PropertyName = "documentGuid")]
        public string DocumentGuid { get; set; }

        [JsonProperty(PropertyName = "priority")]
        public string Priority { get; set; }

        [JsonProperty(PropertyName = "priorityId")]
        public int? PriorityId { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "statusId")]
        public int? StatusId { get; set; }

        [JsonProperty(PropertyName = "serviceType")]
        public string ServiceType { get; set; }

        [JsonProperty(PropertyName = "material")]
        public string Material { get; set; }

        [JsonProperty(PropertyName = "materialType")]
        public string MaterialType { get; set; }

        [JsonProperty(PropertyName = "item")]
        public string Item { get; set; }
    }
}
