﻿using Newtonsoft.Json;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models
{
    public class TicketFileUploadSettings
    {
        [JsonProperty(PropertyName = "fileName")]
        public string FileName { get; set; } = string.Empty;

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; } = string.Empty;

        [JsonProperty(PropertyName = "size")]
        public long? Size { get; set; }

        [JsonProperty(PropertyName = "maxFileSize")]
        public int MaxFileSize { get; set; } = 100 * 1024 * 1024; // 100MB

        [JsonProperty(PropertyName = "validFileExtension")]
        public string ValidFileExtension { get; set; } = ".STL";

        [JsonProperty(PropertyName = "validFileExtensionImport")]
        public string ValidFileExtensionImport { get; set; } = ".XML";

        [JsonProperty(PropertyName = "sizeConverted")]
        public string SizeConverted
        {
            get
            {
                return (Size / 1048576).ToString();
            }
        }
    }
}
