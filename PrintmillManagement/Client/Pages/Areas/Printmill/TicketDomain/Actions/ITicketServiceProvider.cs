﻿using PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Actions
{
    public interface ITicketServiceProvider
    {
        /// <summary>
        ///  Method that calls api resource for retrieving all initial data for filter.
        /// </summary>
        /// <returns><see cref="TicketInitialFilterDataView"/>Contains all data for retrieved filters</returns>
        Task<TicketInitialFilterDataView> GetInitialFilterDataAsync();

        /// <summary>
        /// Method that calls api resource for creating new ticket.
        /// </summary>
        /// <param name="request"><see cref="TicketCreateRequest"/>/param>
        /// <returns><see cref="ResponseViewModel"/>Contains result from api resource if ticket is created or not</returns>
        Task<ResponseViewModel> CreateTicketAsync(TicketCreateRequest request);

        /// <summary>
        /// Method that calls api resource for updating present ticket.
        /// </summary>
        /// <param name="request"><see cref="TicketEditRequest"/>/param>
        /// <param name="ticketGuid"><see cref="string"/>/param>
        /// <returns><see cref="ResponseViewModel"/>Contains result from api resource if ticket is updated or not</returns>
        Task<ResponseViewModel> EditTicketByGuidAsync(TicketEditRequest request, string ticketGuid);

        /// <summary>
        /// Method that calls api resource for retrieving all tickets by provided filter and by User Created.
        /// </summary>
        /// <param name="page"><see cref="int"/>/param>
        /// <param name="pageSize"><see cref="int"/>/param>
        /// <param name="request"><see cref="TicketFilterRequest"/>/param>
        /// <returns><see cref="TicketDataView"/>Contains all data for tickets as list</returns>
        Task<PagedResponse<TicketDataView>> GetAllTicketsByUserCreatedAsync(int page, int pageSize, TicketFilterRequest request);

        /// <summary>
        /// Method that calls api resource for retrieving all tickets by provided filter and by User Processed.
        /// </summary>
        /// <param name="page"><see cref="int"/>/param>
        /// <param name="pageSize"><see cref="int"/>/param>
        /// <param name="request"><see cref="TicketFilterRequest"/>/param>
        /// <returns><see cref="TicketDataView"/>Contains all data for tickets as list</returns>
        Task<PagedResponse<TicketDataView>> GetAllTicketsByUserProcessedAsync(int page, int pageSize, TicketFilterRequest request);

        /// <summary>
        ///  Method that calls api resource for retrieving single ticket details by provided Guid.
        /// </summary>
        /// <param name="ticketGuid"><see cref="string"/>/param>
        /// <returns><see cref="TicketDetailedDataView"/>Contains all data for retrieved ticket</returns>
        Task<TicketDetailedDataView> GetTicketByGuidAsync(string ticketGuid);

        Task<ResponseViewModel> AcceptTicketAsync(int ticketId);

        Task<ResponseViewModel> UpdateTicketStatusAsync(int ticketId, int statusId);

        Task<List<TicketImportSolvedResponse>> ImportSolvedTicketAsync(TicketImportSolvedRequest request);

        Task DownloadDocumentAsync(int ticketId);

        Task<ResponseViewModel> DeleteTicketAsync(int ticketId);
    }
}