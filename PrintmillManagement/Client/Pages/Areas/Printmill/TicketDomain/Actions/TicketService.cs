﻿using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Constants;
using PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Models;
using PrintmillManagement.Client.Pages.Shared.Helpers;
using PrintmillManagement.Client.Pages.Shared.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Actions
{
    public class TicketService : ITicketServiceProvider
    {
        private readonly string _apiUrlBase;

        private readonly HttpClient _httpClient;
        private readonly IJSRuntime _jsRuntime;

        public TicketService(
            HttpClient httpClient,
            IJSRuntime jsRuntime,
            IConfiguration configuration)
        {
            _httpClient = httpClient;
            _jsRuntime = jsRuntime;
            _apiUrlBase = configuration.GetSection("ApiBaseUrl").Value;
        }

        public async Task<ResponseViewModel> CreateTicketAsync(TicketCreateRequest request)
        {
            var validation = ValidateCreateTicketRequest(request);
            if (validation.Errors.Count > 0) return validation;

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.CreateTicketMethod}";

            var content = new MultipartFormDataContent();
            content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data");
            content.Add(new StreamContent(request.Stream, (int)request.Stream.Length), "File", request.FileName);
            content.Add(new StringContent(request.ServiceTypeId), "ServiceTypeId");
            content.Add(new StringContent(request.MaterialId), "MaterialId");
            content.Add(new StringContent(request.ItemId), "ItemId");
            content.Add(new StringContent(request.NumberOfUnits), "NumberOfUnits");
            content.Add(new StringContent(request.Priority), "Priority");
            if (!string.IsNullOrWhiteSpace(request.MaterialTypeId)) content.Add(new StringContent(request.MaterialTypeId), "MaterialTypeId");         
            if (!string.IsNullOrWhiteSpace(request.Description)) content.Add(new StringContent(request.Description), "Description");
            if (!string.IsNullOrWhiteSpace(request.Note)) content.Add(new StringContent(request.Note), "Note");
           
            var response = await _httpClient.PostAsync(apiUrl, content);

            return await ResponseHelperMethods.SerializeHttpResponseMessage(response, "Error Ocurred while creating Ticket.");
        }

        public async Task<ResponseViewModel> EditTicketByGuidAsync(TicketEditRequest request, string ticketGuid)
        {
            var validation = ValidateEditTicketRequest(request);
            if (validation.Errors.Count > 0) return validation;

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.EditTicketMethod}/{ticketGuid}";

            var content = new MultipartFormDataContent();
            content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data");
            content.Add(new StringContent(request.NumberOfUnits), "NumberOfUnits");
            if (!string.IsNullOrWhiteSpace(request.Description)) content.Add(new StringContent(request.Description), "Description");
            if (!string.IsNullOrWhiteSpace(request.Note)) content.Add(new StringContent(request.Note), "Note");

            var response = await _httpClient.PostAsync(apiUrl, content);

            return await ResponseHelperMethods.SerializeHttpResponseMessage(response, "Error Ocurred while updating Ticket.");
        }

        public async Task<List<TicketImportSolvedResponse>> ImportSolvedTicketAsync(TicketImportSolvedRequest request)
        {
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.ImportSolvedTicketMethod}";

            var ticketImportSolvedResponse = new List<TicketImportSolvedResponse>();

            var content = new MultipartFormDataContent();
            content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data");
            content.Add(new StreamContent(request.Stream, (int)request.Stream.Length), "File", request.FileName);
            content.Add(new StringContent(request.FileName), "FileName");

            var response = await _httpClient.PostAsync(apiUrl, content);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                ticketImportSolvedResponse = JsonSerializer.Deserialize<List<TicketImportSolvedResponse>>(responseContent, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return ticketImportSolvedResponse;
        }

        public async Task<TicketInitialFilterDataView> GetInitialFilterDataAsync()
        {
            var initialData = new TicketInitialFilterDataView();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetInitialFilterDataMethod}";

            var response = await _httpClient.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                initialData = JsonSerializer.Deserialize<TicketInitialFilterDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return initialData;
        }

        public async Task<PagedResponse<TicketDataView>> GetAllTicketsByUserCreatedAsync(int page, int pageSize, TicketFilterRequest request)
        {
            var tickets = new PagedResponse<TicketDataView>();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetAllTicketsByUserCreatedMethod}/{page}/{pageSize}";
            var requestContent = new StringContent(JsonSerializer.Serialize(request), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(apiUrl, requestContent);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                tickets = JsonSerializer.Deserialize<PagedResponse<TicketDataView>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }
            
            return tickets;
        }

        public async Task<PagedResponse<TicketDataView>> GetAllTicketsByUserProcessedAsync(int page, int pageSize, TicketFilterRequest request)
        {
            var tickets = new PagedResponse<TicketDataView>();

            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetAllTicketsByUserProcessedMethod}/{page}/{pageSize}";
            var requestContent = new StringContent(JsonSerializer.Serialize(request), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(apiUrl, requestContent);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                tickets = JsonSerializer.Deserialize<PagedResponse<TicketDataView>>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return tickets;
        }

        public async Task<TicketDetailedDataView> GetTicketByGuidAsync(string ticketGuid)
        {
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.GetTicketByGuidMethod}/{ticketGuid}";

            var ticket = new TicketDetailedDataView();

            var response = await _httpClient.GetAsync(apiUrl);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                ticket = JsonSerializer.Deserialize<TicketDetailedDataView>(content, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
            }

            return ticket;
        }

        public async Task<ResponseViewModel> AcceptTicketAsync(int ticketId)
        {
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.AcceptTicketMethod}/{ticketId}";

            var response = await _httpClient.PostAsync(apiUrl, new StringContent(JsonSerializer.Serialize(ticketId), Encoding.UTF8, "application/json"));

            return await ResponseHelperMethods.SerializeHttpResponseMessage(response, "Error Ocurred while accepting Ticket.");
        }

        public async Task<ResponseViewModel> UpdateTicketStatusAsync(int ticketId, int statusId)
        {
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.UpdateTicketStatusMethod}/{ticketId}/{statusId}";

            var response = await _httpClient.PostAsync(apiUrl, new StringContent(JsonSerializer.Serialize(ticketId), Encoding.UTF8, "application/json"));

            return await ResponseHelperMethods.SerializeHttpResponseMessage(response, "Error Ocurred while updating Ticket status.");
        }

        public async Task DownloadDocumentAsync(int ticketId)
        {
            string url = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.DownloadDocumentMethod}/{ticketId}";
            await _jsRuntime.InvokeVoidAsync("FileSaveAs", url);
        }

        public async Task<ResponseViewModel> DeleteTicketAsync(int ticketId)
        {
            string apiUrl = $"{_apiUrlBase}{ApiUrls.ApiController}{ApiUrls.DeleteTicketMethod}/{ticketId}";

            var response = await _httpClient.PostAsync(apiUrl, new StringContent(JsonSerializer.Serialize(ticketId), Encoding.UTF8, "application/json"));

            return await ResponseHelperMethods.SerializeHttpResponseMessage(response, "Error Ocurred while updating Ticket status.");
        }

        private ResponseViewModel ValidateCreateTicketRequest(TicketCreateRequest request)
        {
            var responseViewModel = new ResponseViewModel();

            if (string.IsNullOrEmpty(request.ServiceTypeId))
                responseViewModel.Errors.Add(new KeyValuePair<string, string>("serviceTypeId", "Service Type must be provided!"));

            if (string.IsNullOrEmpty(request.MaterialId))
                responseViewModel.Errors.Add(new KeyValuePair<string, string>("materialId", "Material must be provided!"));

            if (string.IsNullOrEmpty(request.MaterialTypeId))
                responseViewModel.Errors.Add(new KeyValuePair<string, string>("materialTypeId", "Material Type must be provided!"));

            if (string.IsNullOrEmpty(request.ItemId))
                responseViewModel.Errors.Add(new KeyValuePair<string, string>("itemId", "Item must be provided!"));

            if (string.IsNullOrEmpty(request.NumberOfUnits))
                responseViewModel.Errors.Add(new KeyValuePair<string, string>("numberOfUnits", "Number Of Units must be provided!"));

            if (!string.IsNullOrEmpty(request.NumberOfUnits))
            {
                if (int.Parse(request.NumberOfUnits) < 1 || int.Parse(request.NumberOfUnits) > 32)
                    responseViewModel.Errors.Add(new KeyValuePair<string, string>("numberOfUnits", "Number Of Units must be between 1 and 32!"));
            }

            if (string.IsNullOrEmpty(request.Priority))
                responseViewModel.Errors.Add(new KeyValuePair<string, string>("priority", "Priority must be provided!"));

            if (request.Stream is null || string.IsNullOrEmpty(request.FileName))
                responseViewModel.Errors.Add(new KeyValuePair<string, string>("stream", "File must be uploaded!"));

            if (responseViewModel.Errors.Count > 0)
            {
                responseViewModel.Success = false;
                responseViewModel.Message = "Model is not valid. Please check all fields!";
            } 

            return responseViewModel;
        }

        private ResponseViewModel ValidateEditTicketRequest(TicketEditRequest request)
        {
            var responseViewModel = new ResponseViewModel();

            if (string.IsNullOrEmpty(request.NumberOfUnits))
                responseViewModel.Errors.Add(new KeyValuePair<string, string>("numberOfUnits", "Number Of Units must be provided!"));

            if (!string.IsNullOrEmpty(request.NumberOfUnits))
            {
                if (int.Parse(request.NumberOfUnits) < 1 || int.Parse(request.NumberOfUnits) > 32)
                    responseViewModel.Errors.Add(new KeyValuePair<string, string>("numberOfUnits", "Number Of Units must be between 1 and 32!"));
            }

            if (responseViewModel.Errors.Count > 0)
            {
                responseViewModel.Success = false;
                responseViewModel.Message = "Model is not valid. Please check all fields!";
            }

            return responseViewModel;
        }
    }
}
