using Blazored.LocalStorage;
using Blazored.Toast;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using PrintmillManagement.Client.Pages.Areas.Delivery.DeliveryDomain.Actions;
using PrintmillManagement.Client.Pages.Areas.Printmill.DashboardDomain.Actions;
using PrintmillManagement.Client.Pages.Areas.Printmill.ItemDomain.Actions;
using PrintmillManagement.Client.Pages.Areas.Printmill.MaterialDomain.Actions;
using PrintmillManagement.Client.Pages.Areas.Printmill.MaterialTypeDomain.Actions;
using PrintmillManagement.Client.Pages.Areas.Printmill.ServiceTypeDomain.Actions;
using PrintmillManagement.Client.Pages.Areas.Printmill.TicketDomain.Actions;
using PrintmillManagement.Client.Pages.Areas.User.AuthenticationDomain.Actions;
using PrintmillManagement.Client.Pages.Areas.User.UserDomain.Actions;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PrintmillManagement.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });  

            ConfigureDependencyInjection(builder);
            
            builder.Services.AddBlazoredLocalStorage();
            builder.Services.AddBlazoredToast();
            builder.Services.AddAuthorizationCore();

            await builder.Build().RunAsync();
        }

        // configure Dependency Injection for application services configuration
        public static void ConfigureDependencyInjection(WebAssemblyHostBuilder builder)
        {
            builder.Services.AddScoped<IUserServiceProvider, UserService>();
            builder.Services.AddScoped<ITicketServiceProvider, TicketService>();
            builder.Services.AddScoped<IServiceTypeServiceProvider, ServiceTypeService>();
            builder.Services.AddScoped<IMaterialServiceProvider, MaterialService>();
            builder.Services.AddScoped<IMaterialTypeServiceProvider, MaterialTypeService>();
            builder.Services.AddScoped<IItemServiceProvider, ItemService>();
            builder.Services.AddScoped<IDeliveryServiceProvider, DeliveryService>();
            builder.Services.AddScoped<IDashboardServiceProvider, DashboardService>();
            builder.Services.AddScoped<IAuthenticationServiceProvider, AuthenticationService>();
            builder.Services.AddScoped<AuthenticationStateProvider, AuthenticationStateService>();
            builder.Services.AddScoped<IAuthenticationStateServiceProvider, AuthenticationStateService>();
        }
    }
}
