﻿using Newtonsoft.Json;

namespace PrintmillManagement.Modules.Shared.Paging
{
    public class PagingRequest
    {
        public PagingRequest()
        {
            Page = 1;
            PageSize = 8;
        }

        public PagingRequest(int page, int pageSize)
        {
            Page = page < 1 ? 1 : page;
            PageSize = pageSize > 8 ? 8 : pageSize;
            Offset = page == 0 ? 0 : ( ( page - 1 ) * pageSize );
        }

        [JsonProperty(PropertyName = "page")]
        public int Page { get; set; }

        [JsonProperty(PropertyName = "pageSize")]
        public int PageSize { get; set; }

        [JsonProperty(PropertyName = "offset")]
        public int Offset { get; set; }
    }
}
