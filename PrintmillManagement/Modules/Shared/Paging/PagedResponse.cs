﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace PrintmillManagement.Modules.Shared.Paging
{
    public class PagedResponse<T>
    {
        public PagedResponse()
        {

        }

        public PagedResponse(ICollection<T> items, int page, int pageSize, int totalCount)
        {
            Items = items;
            Paging = new PagingResponse
            {
                Page = page,
                PageSize = pageSize,
                TotalCount = totalCount,
                TotalPages = (int)Math.Ceiling((double)totalCount / pageSize)
            };
        }

        [JsonProperty(PropertyName = "items")]
        public ICollection<T> Items { get; set; }

        [JsonProperty(PropertyName = "paging")]
        public PagingResponse Paging { get; set; }
    }
}