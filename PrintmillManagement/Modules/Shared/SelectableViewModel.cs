﻿namespace PrintmillManagement.Modules.Shared
{
    public class SelectableViewModel
    {
        public int Key { get; set; }
        public string Value { get; set; }
        public string Pair { get; set; }
    }
}
