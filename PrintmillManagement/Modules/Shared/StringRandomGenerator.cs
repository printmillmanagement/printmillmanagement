﻿using System;

namespace PrintmillManagement.Modules.Shared
{
    public static class StringRandomGenerator
    {
        public static string GenerateRandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new string(stringChars);
        }

        public static string GenerateNewRandomString(string oldRendom)
        {
            string newRandom;

            do
            {
                newRandom = GenerateRandomString(oldRendom.Length);
            } while (newRandom == oldRendom);

            return newRandom;
        }
    }
}
