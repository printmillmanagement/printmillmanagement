﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace PrintmillManagement.Modules.Shared
{
    public static class CryptoUtility
    {
        /// <summary>
        /// Method for Decrypting provided text string and key string.
        /// </summary>
        /// <param name="cipherText">Text for Dencrypting.</param>
        /// <param name="keyString">Key for Dencrypting.</param>
        /// <returns>Encrypted string as type of string</returns>
        public static string Decrypt(string cipherText, string keyString)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0) throw new ArgumentNullException("Invalid cipher text");
            if (keyString == null || keyString.Length <= 0) throw new ArgumentNullException("Invalid key");

            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[fullCipher.Length - iv.Length]; ;

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, fullCipher.Length - iv.Length);
            var key = Encoding.UTF8.GetBytes(keyString);

            // Create an Aes object
            // with the specified key and IV.
            using (var aesAlg = Aes.Create())
            {
                // Create a decryptor to perform the stream transform.
                using (var decryptor = aesAlg.CreateDecryptor(key, iv))
                {
                    string result;

                    // Create the streams used for decryption.
                    using (var msDecrypt = new MemoryStream(cipher))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                result = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                    return result;
                }
            }
        }

        /// <summary>
        /// Method for Encrypting provided text string and key string.
        /// </summary>
        /// <param name="text">Text for Encrypting.</param>
        /// <param name="keyString">Key for Encrypting.</param>
        /// <returns>Encrypted string as type of string</returns>
        public static string Encrypt(string text, string keyString)
        {
            // Check arguments.
            if (text == null || text.Length <= 0) throw new ArgumentNullException("Invalid text");
            if (keyString == null || keyString.Length <= 0) throw new ArgumentNullException("Invalid key");

            var key = Encoding.UTF8.GetBytes(keyString);

            // Create an Aes object
            // with the specified key and IV.
            using (var aesAlg = Aes.Create())
            {
                // Create an encryptor to perform the stream transform.
                using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
                {
                    // Create the streams used for encryption.
                    using (var msEncrypt = new MemoryStream())
                    {
                        using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(text);
                        }

                        var iv = aesAlg.IV;

                        var decryptedContent = msEncrypt.ToArray();

                        var result = new byte[iv.Length + decryptedContent.Length];

                        Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                        Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);

                        // Return the encrypted bytes from the memory stream.
                        return Convert.ToBase64String(result);
                    }
                }
            }
        }
    }
}
