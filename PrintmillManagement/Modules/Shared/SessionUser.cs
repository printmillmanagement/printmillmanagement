﻿namespace PrintmillManagement.Modules.Shared
{
    public class SessionUser
    {
        public SessionUser(int userId, string userEmail, string userRole, string ip)
        {
            UserId = userId;
            UserEmail = userEmail;
            UserRole = userRole;
            Ip = ip;
        }

        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public string UserRole { get; set; }
        public string Ip { get; set; }
    }
}
