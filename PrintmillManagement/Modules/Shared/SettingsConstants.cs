﻿namespace PrintmillManagement.Modules.Shared
{
    public class SettingsConstants
    {
        public const string DateTimeFormat = "dd/MM/yyyy H:mm";
    }
}
