﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using MimeKit;
using PrintmillManagement.Infrastructure.Configuration;
using PrintmillManagement.Modules.Services.NotificationService.Models;
using PrintmillManagement.Modules.Services.NotificationService.Ports;
using PrintmillManagement.Modules.Shared;
using System;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Services.NotificationService.Adapters
{
    public class EmailServiceAdapter : IEmailServiceProvider
    {
        private readonly MailSettings _mailSettings;
        private readonly string _securityKey;
        private readonly ILogger<EmailServiceAdapter> _logger;

        public EmailServiceAdapter(
            ILogger<EmailServiceAdapter> logger,
            MailSettings mailSettings,
            string securityKey
            )
        {
            _mailSettings = mailSettings;
            _securityKey = securityKey;
            _logger = logger;
        }

        /// <summary>
        /// Method for Send Email Notification.
        /// </summary>
        /// <param name="mailRequest"><see cref="MailRequest"/>Contains model data for Email</param>
        public async Task SendEmailAsync(MailRequest mailRequest)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
            email.To.Add(MailboxAddress.Parse(mailRequest.ToEmail));
            email.Subject = mailRequest.Subject;
            
            var builder = new BodyBuilder();
            builder.HtmlBody = mailRequest.Body;
            email.Body = builder.ToMessageBody();

            try
            {
                using var smtp = new SmtpClient
                {
                    // InvalidSslCertificate => https://github.com/jstedfast/MailKit/blob/master/FAQ.md#InvalidSslCertificate
                    // Because of this line, all emails will be received as spam, a certificate on the server is needed
                    // After the certificate is added, remove line 51, and do changes on line 52 => client.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls)
                    ServerCertificateValidationCallback = (s, c, h, e) => true
                };

                await smtp.ConnectAsync(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.Auto);
                await smtp.AuthenticateAsync(_mailSettings.Mail, CryptoUtility.Decrypt(_mailSettings.Password, _securityKey));
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while sending email.", email);
                throw;
            }
        }
    }
}
