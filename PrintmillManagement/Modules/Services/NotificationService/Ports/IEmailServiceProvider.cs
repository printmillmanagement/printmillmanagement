﻿using PrintmillManagement.Modules.Services.NotificationService.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Services.NotificationService.Ports
{
    public interface IEmailServiceProvider
    {
        /// <summary>
        /// Method for Send Email Operation.
        /// </summary>
        /// <param name="mailRequest"><see cref="MailRequest"/>Contains model data for Email</param>
        Task SendEmailAsync(MailRequest mailRequest);
    }
}
