﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using System.Text;

namespace PrintmillManagement.Modules.Services.NotificationService.Models
{
    public static class NotificationBody
    {
        public static string CompositeNewSubjectNotification(TicketStatus ticketStatus)
        {
            switch (ticketStatus)
            {
                case TicketStatus.Open:
                    return NotificationStatus.NewTicketCreated;

                case TicketStatus.Accepted:
                    return NotificationStatus.NewTicketCreated;

                case TicketStatus.NeedsAdditionalWork:
                    return NotificationStatus.NewTicketCreated;

                case TicketStatus.InProgress:
                    return NotificationStatus.NewTicketCreated;

                case TicketStatus.Finished:
                    return NotificationStatus.NewTicketCreated;
                default:
                    break;
            }

            return string.Empty;
        }
        public static string CompositeNewBodyNotification(TicketStatus ticketStatus)
        {
            var notificationBody = new StringBuilder();

            switch(ticketStatus)
            {
                case TicketStatus.Open:
                    notificationBody.AppendLine($"<b>Hello,</b><br />");
                    notificationBody.AppendLine($"<b>Ticket has been Created and waiting for approval.</b><br />");
                    notificationBody.AppendLine($"<b>Kind Regards,</b><br />");
                    notificationBody.AppendLine($"<b>Print & Mill Management</b><br />");
                    return notificationBody.ToString();

                case TicketStatus.Accepted:
                    notificationBody.AppendLine($"<b>Hello,</b><br />");
                    notificationBody.AppendLine($"<b>Ticket has been Accepted.</b><br />");
                    notificationBody.AppendLine($"<b>Kind Regards,</b><br />");
                    notificationBody.AppendLine($"<b>Print & Mill Management</b><br />");
                    return notificationBody.ToString();

                case TicketStatus.NeedsAdditionalWork:
                    notificationBody.AppendLine($"<b>Hello,</b><br />");
                    notificationBody.AppendLine($"<b>Ticket Needs Additional Work.</b><br />");
                    notificationBody.AppendLine($"<b>Kind Regards,</b><br />");
                    notificationBody.AppendLine($"<b>Print & Mill Management</b><br />");
                    return notificationBody.ToString();

                case TicketStatus.InProgress:
                    notificationBody.AppendLine($"<b>Hello,</b><br />");
                    notificationBody.AppendLine($"<b>Ticket is in Working Progress.</b><br />");
                    notificationBody.AppendLine($"<b>Kind Regards,</b><br />");
                    notificationBody.AppendLine($"<b>Print & Mill Management</b><br />");
                    return notificationBody.ToString();

                case TicketStatus.Finished:
                    notificationBody.AppendLine($"<b>Hello,</b><br />");
                    notificationBody.AppendLine($"<b>Ticket is Successfully Completed.</b><br />");
                    notificationBody.AppendLine($"<b>Kind Regards,</b><br />");
                    notificationBody.AppendLine($"<b>Print & Mill Management</b><br />");
                    return notificationBody.ToString();
            }

            return notificationBody.ToString();
        }
    }
}
