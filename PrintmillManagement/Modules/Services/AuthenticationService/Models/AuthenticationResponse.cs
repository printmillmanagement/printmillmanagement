﻿using System;

namespace PrintmillManagement.Modules.Services.AuthenticationService.Models
{
    public class AuthenticationResponse
    {
        public string Token { get; set; }
        public DateTime Expiry { get; set; }
    }
}
