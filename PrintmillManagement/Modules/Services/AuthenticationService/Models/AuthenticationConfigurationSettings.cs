﻿namespace PrintmillManagement.Modules.Services.AuthenticationService.Models
{
    public class AuthenticationConfigurationSettings
    {
        public AuthenticationConfigurationSettings()
        {

        }

        public AuthenticationConfigurationSettings(string jwtSecurityKey, string jwtIssuer, int jwtExpiryInDays)
        {
            JwtSecurityKey = jwtSecurityKey;
            JwtIssuer = jwtIssuer;
            JwtExpiryInDays = jwtExpiryInDays;
        }

        public string JwtSecurityKey { get; set; }
        public string JwtIssuer { get; set; }
        public int JwtExpiryInDays { get; set; }
    }
}
