﻿using FluentResults;
using PrintmillManagement.Modules.Services.AuthenticationService.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Services.AuthenticationService
{
    public interface IAuthenticationDomainServiceProvider
    {
        /// <summary>
        /// Method for user authentication
        /// </summary>
        /// <param name="request"><see cref="AuthenticationResponse"/>/param>
        /// <param name="settings"><see cref="AuthenticationConfigurationSettings"/>/param>
        /// <returns><see cref="AuthenticationResponse"/>Contains data for authenticated user</returns>
        Task<Result<AuthenticationResponse>> Authenticate(AuthenticationRequest request, AuthenticationConfigurationSettings settings);
    }
}
