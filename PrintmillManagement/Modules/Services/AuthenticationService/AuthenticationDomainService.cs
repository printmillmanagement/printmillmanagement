﻿using FluentResults;
using Microsoft.IdentityModel.Tokens;
using PrintmillManagement.Modules.Printmill.UserManagement.DB.Ports;
using PrintmillManagement.Modules.Services.AuthenticationService.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Services.AuthenticationService
{
    public class AuthenticationDomainService : IAuthenticationDomainServiceProvider
    {
        private readonly IUserDataProvider _userDataProvider;

        public AuthenticationDomainService(
            IUserDataProvider userDataProvider)
        {
            _userDataProvider = userDataProvider;
        }

        /// <summary>
        /// Method for user authentication
        /// </summary>
        /// <param name="request"><see cref="AuthenticationResponse"/>/param>
        /// <param name="settings"><see cref="AuthenticationConfigurationSettings"/>/param>
        /// <returns><see cref="AuthenticationResponse"/>Contains data for authenticated user</returns>
        public async Task<Result<AuthenticationResponse>> Authenticate(AuthenticationRequest request, AuthenticationConfigurationSettings settings)
        {
            var userToAuthenticate = await GetUserData(request);
            if (userToAuthenticate == null) return Result.Fail("Error occurred while authenticating. Please check your email and/or password and try again.");

            var authenticationResponse = BuildToken(userToAuthenticate, settings);
            if (authenticationResponse == null) return Result.Fail("Error occurred while authenticating.");

            return Result.Ok(authenticationResponse);
        }

        /// <summary>
        /// Method for retrieving user based on provided email and password
        /// </summary>
        /// <param name="request"><see cref="AuthenticationRequest"/>/param>
        /// <returns><see cref="AuthenticationUser"/>Contains data for user</returns>
        private async Task<AuthenticationUser> GetUserData(AuthenticationRequest request)
        {
            var user = await _userDataProvider.GetUserByEmailAsync(request.Email);

            if (user == null || !BCrypt.Net.BCrypt.Verify(request.Password, user.Password))
            {
                return null;
            }

            AuthenticationUser userToAuthenticate = new AuthenticationUser
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Role = user.Role
            };

            return userToAuthenticate;
        }

        /// <summary>
        /// Method for building Bearer token based on retrieved user and Auth settings
        /// </summary>
        /// <param name="authenticationUser"><see cref="AuthenticationUser"/>/param>
        /// <param name="settings"><see cref="AuthenticationConfigurationSettings"/>/param>
        /// <returns><see cref="AuthenticationUser"/>Contains data for authentication response</returns>
        private static AuthenticationResponse BuildToken(AuthenticationUser authenticationUser, AuthenticationConfigurationSettings settings)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, authenticationUser.Id.ToString()),
                new Claim(ClaimTypes.Email , authenticationUser.Email),
                new Claim(ClaimTypes.Role, authenticationUser.Role)
            };

            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(settings.JwtSecurityKey));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiry = DateTime.Now.AddDays(settings.JwtExpiryInDays);

            var token = new JwtSecurityToken(
                settings.JwtIssuer,
                settings.JwtIssuer,
                claims,
                expires: expiry,
                signingCredentials: credentials
            );

            AuthenticationResponse authenticationResponse = new AuthenticationResponse
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiry = expiry
            };

            return authenticationResponse;
        }
    }
}
