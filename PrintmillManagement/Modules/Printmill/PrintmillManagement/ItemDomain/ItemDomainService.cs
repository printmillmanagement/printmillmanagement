﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain.Models;
using PrintmillManagement.Modules.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain
{
    public class ItemDomainService
    {
        private readonly IItemDataProvider _itemDataProvider;

        public ItemDomainService(
            IItemDataProvider itemDataProvider)
        {
            _itemDataProvider = itemDataProvider;
        }

        public async Task<IEnumerable<SelectableViewModel>> SelecAllItemsAsync()
        {
            var resultToReturn = await _itemDataProvider.SelectAllItemsAsync();

            return resultToReturn.Select(x => new SelectableViewModel { Key = x.Id, Value = x.Name });
        }

        public async Task<IEnumerable<SelectableViewModel>> SelecItemsByServiceTypeAndMaterialTypeAsync(int serviceTypeId, int materialTypeId)
        {
            var resultToReturn = await _itemDataProvider.SelecItemsByServiceTypeAndMaterialTypeAsync(serviceTypeId, materialTypeId);

            return resultToReturn.Select(x => new SelectableViewModel { Key = x.Id, Value = x.Name });
        }

        public async Task<Item> SelecItemByIdAsync(int itemId)
        {
            return await _itemDataProvider.SelectItemByIdAsync(itemId);
        }

        public async Task<Result> InsertItemAsync(SessionUser sessionUser, ItemCreateRequest itemCreateRequest)
        {
            var item = new Item
            {
                Guid = Guid.NewGuid().ToString().ToUpper(),
                Name = itemCreateRequest.Name,
                Description = itemCreateRequest.Description,
                Note = itemCreateRequest.Note,
                DateCreated = DateTime.Now.ToString(SettingsConstants.DateTimeFormat)
            };

            var result = await _itemDataProvider.InsertItemAsync(item);

            if (!result)
            {
                return Result
                    .Fail("Failed To Create Item");
            }
            else
            {
                return Result
                    .Ok()
                    .WithSuccess("Item Created");
            }
        }

        public async Task<Result> UpdateItemAsync(int itemId)
        {
            var item = new Item();

            var result = await _itemDataProvider.UpdateItemAsync(itemId, item);

            if (!result)
            {
                return Result
                    .Fail("Failed To Update Item");
            }
            else
            {
                return Result
                    .Ok()
                    .WithSuccess("Item Updated");
            }
        }

        public async Task<Result> DeleteItemAsync(int itemId)
        {
            var result = await _itemDataProvider.DeleteItemAsync(itemId);

            if (!result)
            {
                return Result
                    .Fail("Failed To Delete Item");
            }
            else
            {
                return Result
                    .Ok()
                    .WithSuccess("Item Deleted");
            }
        }
    }
}
