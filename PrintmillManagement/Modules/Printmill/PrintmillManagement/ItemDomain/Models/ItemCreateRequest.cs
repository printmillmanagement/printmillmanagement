﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain.Models
{
    public class ItemCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
    }
}
