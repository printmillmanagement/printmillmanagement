﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string DateCreated { get; set; }
    }
}
