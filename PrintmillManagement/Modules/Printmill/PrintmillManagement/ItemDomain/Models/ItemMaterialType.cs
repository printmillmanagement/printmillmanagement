﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain.Models;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain.Models
{
    public class ItemMaterialType
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string DateCreated { get; set; }

        public int ItemtId { get; set; }
        public Item Item { get; set; }

        public int MaterialTypeId { get; set; }
        public MaterialType MaterialType { get; set; }
    }
}
