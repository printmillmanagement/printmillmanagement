﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain.Models;
using PrintmillManagement.Modules.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain
{
    public interface IServiceTypeDomainServiceProvider
    {
        /// <summary>
        /// Method for retrieving all Service Types
        /// </summary>
        /// <param name="request"><see cref="TicketFilterRequest"/>/param>
        /// <returns><see cref="TicketDataView"/>Contains all data for Service Types as list</returns>
        Task<IEnumerable<SelectableViewModel>> SelecAllServicesAsync();

        /// <summary>
        /// Method for creating new Service Type
        /// </summary>
        /// <param name="user"><see cref="SessionUser"/>/param>
        /// <param name="serviceTypeCreateRequest"><see cref="ServiceTypeCreateRequest"/>/param>
        /// <returns><see cref="Result"/>Contains result if Service Type is created or not</returns>
        Task<Result> InsertServiceAsync(SessionUser sessionUser, ServiceTypeCreateRequest serviceTypeCreateRequest);
    }
}
