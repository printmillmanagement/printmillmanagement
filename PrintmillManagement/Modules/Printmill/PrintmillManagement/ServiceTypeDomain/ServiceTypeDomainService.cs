﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain.Models;
using PrintmillManagement.Modules.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain
{
    public class ServiceTypeDomainService : IServiceTypeDomainServiceProvider
    {
        private readonly IServiceTypeDataProvider _serviceTypeDataProvider;

        public ServiceTypeDomainService(
            IServiceTypeDataProvider serviceTypeDataProvider)
        {
            _serviceTypeDataProvider = serviceTypeDataProvider;
        }

        public async Task<IEnumerable<SelectableViewModel>> SelecAllServicesAsync()
        {
            var resultToReturn = await _serviceTypeDataProvider.SelectAllServiceTypesAsync();

            return resultToReturn.Select(x => new SelectableViewModel { Key = x.Id, Value = x.Name });
        }

        public async Task<Result> InsertServiceAsync(SessionUser sessionUser, ServiceTypeCreateRequest serviceTypeCreateRequest)
        {
            var serviceType = new ServiceType
            {
                Guid = Guid.NewGuid().ToString().ToUpper(),
                Name = serviceTypeCreateRequest.Name,
                Description = serviceTypeCreateRequest.Description,
                Note = serviceTypeCreateRequest.Note,
                DateCreated = DateTime.Now.ToString(SettingsConstants.DateTimeFormat)
            };

            var result = await _serviceTypeDataProvider.InsertServiceTypeAsync(serviceType);

            if (result)
            {
                return Result.Ok().WithSuccess("Service Type Created");
            }
            else
            {
                return Result.Fail("Failed To Create Service Type");
            }
        }
    }
}
