﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain.Models
{
    public class ServiceTypeCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
    }
}
