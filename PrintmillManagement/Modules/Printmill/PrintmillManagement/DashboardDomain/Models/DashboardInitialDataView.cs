﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DashboardDomain.Models
{
    public class DashboardInitialDataView
    {
        public int OpenTicketsCount { get; set; }
        public int AcceptedTicketsCount { get; set; }
        public int InProgressTicketsCount { get; set; }
        public int FinishedTicketsCount { get; set; }
        public int NeedsAdditionalWorkTicketsCount { get; set; }
        public int InDeliveryTicketsCount { get; set; }
    }
}
