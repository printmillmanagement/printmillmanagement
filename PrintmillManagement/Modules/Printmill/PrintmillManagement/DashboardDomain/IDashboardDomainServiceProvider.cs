﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.DashboardDomain.Models;
using PrintmillManagement.Modules.Shared;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DashboardDomain
{
    public interface IDashboardDomainServiceProvider
    {
        /// <summary>
        /// Method for retrieving initial data for Dashboard
        /// </summary>
        /// <param name="user"><see cref="SessionUser"/>/param>
        /// <param name="statusId"><see cref="string"/>/param>
        /// <returns><see cref="DashboardInitialDataView"/>Contains all data for Dashboard</returns>
        Task<DashboardInitialDataView> SelectTicketStatisticsForProcessedByUserAsync(SessionUser user, string statusId);

        /// <summary>
        /// Method for retrieving initial data for Dashboard
        /// </summary>
        /// <param name="user"><see cref="SessionUser"/>/param>
        /// <param name="statusId"><see cref="string"/>/param>
        /// <returns><see cref="DashboardInitialDataView"/>Contains all data for Dashboard</returns>
        Task<DashboardInitialDataView> SelectTicketStatisticsForCreatedByUserAsync(SessionUser user, string statusId);
    }
}
