﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.DashboardDomain.Models;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DashboardDomain
{
    public class DashboardDomainService : IDashboardDomainServiceProvider
    {
        private readonly ITicketDataProvider _ticketDataProvider;

        public DashboardDomainService(
            ITicketDataProvider ticketDataProvider)
        {
            _ticketDataProvider = ticketDataProvider;
        }

        public async Task<DashboardInitialDataView> SelectTicketStatisticsForProcessedByUserAsync(SessionUser user, string statusId)
        {
            var initData = new DashboardInitialDataView();

            var openTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForProcessedByUserIdAsync(null, (int)TicketStatus.Open);
            var acceptedTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForProcessedByUserIdAsync(user.UserId, (int)TicketStatus.Accepted);
            var inProgressTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForProcessedByUserIdAsync(user.UserId, (int)TicketStatus.InProgress);
            var finishedTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForProcessedByUserIdAsync(user.UserId, (int)TicketStatus.Finished);
            var needsAdditionalWorkTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForProcessedByUserIdAsync(user.UserId, (int)TicketStatus.NeedsAdditionalWork);
            var inDeliveryTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForProcessedByUserIdAsync(user.UserId, (int)TicketStatus.InDelivery);

            IEnumerable<Task> tasks = new List<Task>() {
                openTicketResponseTask,
                acceptedTicketResponseTask,
                inProgressTicketResponseTask,
                finishedTicketResponseTask,
                needsAdditionalWorkTicketResponseTask,
                inDeliveryTicketResponseTask
            };

            await Task.WhenAll(tasks);

            initData.OpenTicketsCount = openTicketResponseTask.Result;
            initData.AcceptedTicketsCount = acceptedTicketResponseTask.Result;
            initData.InProgressTicketsCount = inProgressTicketResponseTask.Result;
            initData.FinishedTicketsCount = finishedTicketResponseTask.Result;
            initData.NeedsAdditionalWorkTicketsCount = needsAdditionalWorkTicketResponseTask.Result;
            initData.InDeliveryTicketsCount = inDeliveryTicketResponseTask.Result;

            return initData;
        }

        public async Task<DashboardInitialDataView> SelectTicketStatisticsForCreatedByUserAsync(SessionUser user, string statusId)
        {
            var initData = new DashboardInitialDataView();

            var openTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForCreatedByUserIdAsync(user.UserId, (int)TicketStatus.Open);
            var acceptedTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForCreatedByUserIdAsync(user.UserId, (int)TicketStatus.Accepted);
            var inProgressTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForCreatedByUserIdAsync(user.UserId, (int)TicketStatus.InProgress);
            var finishedTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForCreatedByUserIdAsync(user.UserId, (int)TicketStatus.Finished);
            var needsAdditionalWorkTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForCreatedByUserIdAsync(user.UserId, (int)TicketStatus.NeedsAdditionalWork);
            var inDeliveryTicketResponseTask = _ticketDataProvider.SelectTicketStatisticsForCreatedByUserIdAsync(user.UserId, (int)TicketStatus.InDelivery);

            IEnumerable<Task> tasks = new List<Task>() {
                openTicketResponseTask,
                acceptedTicketResponseTask,
                inProgressTicketResponseTask,
                finishedTicketResponseTask,
                needsAdditionalWorkTicketResponseTask,
                inDeliveryTicketResponseTask
            };

            await Task.WhenAll(tasks);

            initData.OpenTicketsCount = openTicketResponseTask.Result;
            initData.AcceptedTicketsCount = acceptedTicketResponseTask.Result;
            initData.InProgressTicketsCount = inProgressTicketResponseTask.Result;
            initData.FinishedTicketsCount = finishedTicketResponseTask.Result;
            initData.NeedsAdditionalWorkTicketsCount = needsAdditionalWorkTicketResponseTask.Result;
            initData.InDeliveryTicketsCount = inDeliveryTicketResponseTask.Result;

            return initData;
        }
    }
}
