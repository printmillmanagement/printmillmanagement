﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain.Models;
using PrintmillManagement.Modules.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain
{
    public class MaterialDomainService
    {
        private readonly IMaterialDataProvider _materialDataProvider;

        public MaterialDomainService(
            IMaterialDataProvider materialDataProvider)
        {
            _materialDataProvider = materialDataProvider;
        }

        public async Task<List<Material>> SelectAllMaterialsAsync()
        {
            return await _materialDataProvider.SelectAllMaterialsAsync();
        }

        public async Task<IEnumerable<SelectableViewModel>> SelectMaterialsByServiceTypeIdAsync(int serviceTypeId)
        {
            var resultToReturn = await _materialDataProvider.SelectMaterialsByServiceTypeIdAsync(serviceTypeId);

            return resultToReturn.Select(x => new SelectableViewModel { Key = x.Id, Value = x.Name });
        }

        public async Task<Material> SelectMaterialByIdAsync(int materialId)
        {
            return await _materialDataProvider.SelectMaterialByIdAsync(materialId);
        }

        public async Task<Result> InsertMaterialAsync(SessionUser sessionUser, MaterialCreateRequest materialCreateRequest)
        {
            var material = new Material
            {
                Guid = Guid.NewGuid().ToString().ToUpper(),
                Name = materialCreateRequest.Name,
                Description = materialCreateRequest.Description,
                Note = materialCreateRequest.Note,
                DateCreated = DateTime.Now.ToString(SettingsConstants.DateTimeFormat)
            };

            var result = await _materialDataProvider.InsertMaterialAsync(material);

            if (!result)
            {
                return Result
                    .Fail("Failed To Create Material");
            }
            else
            {
                return Result
                    .Ok()
                    .WithSuccess("Material Created.");
            }
        }

        public async Task<Result> UpdateMaterialAsync(int materialId)
        {
            var material = new Material();

            var result = await _materialDataProvider.UpdateMaterialAsync(materialId, material);

            if (!result)
            {
                return Result
                    .Fail("Failed To Update Material");
            }
            else
            {
                return Result
                    .Ok()
                    .WithSuccess("Material Updated.");
            }
        }

        public async Task<Result> DeleteMaterialAsync(int materialId)
        {
            var result = await _materialDataProvider.DeleteMaterialAsync(materialId);

            if (!result)
            {
                return Result
                    .Fail("Failed To Delete Material");
            }
            else
            {
                return Result
                    .Ok()
                    .WithSuccess("Material Deleted.");
            }
        }
    }
}
