﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain.Models
{
    public class MaterialCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
    }
}
