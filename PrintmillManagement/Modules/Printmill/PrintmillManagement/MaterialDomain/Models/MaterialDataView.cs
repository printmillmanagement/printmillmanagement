﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain.Models
{
    public class MaterialDataView
    {
        public MaterialDataView()
        {

        }

        public int Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string DateCreated { get; set; }
    }
}
