﻿using Microsoft.AspNetCore.Http;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Shared;
using System;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models
{
    public class Document
    {
        public Document()
        {

        }

        public Document(TicketCreateRequest request, int ticketId, string guid)
        {
            Guid = guid;
            TicketId = ticketId;
            File = request.File;
            Size = $"{(request.File.Length / 1048576)} MegaByte";
            Name = request.File.FileName;
            DateCreated = DateTime.Now.ToString(SettingsConstants.DateTimeFormat);
        }

        public int Id { get; set; }
        public string Guid { get; set; }
        public IFormFile File { get; set; }
        public string Size { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }
        public string DateCreated { get; set; }

        public int TicketId { get; set; }
        public Ticket Ticket { get; set; }
    }
}
