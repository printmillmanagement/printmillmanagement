﻿using System.IO;
using System.Net.Mime;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models
{
    public class DocumentDownloadResponse
    {
        public string DocumentName { get; set; }
        public ContentDisposition ContentDisposition { get; set; }
        public FileStream Document { get; set; }
    }
}
