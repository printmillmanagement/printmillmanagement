﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models
{
    public class DocumentDataView
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Size { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }
        public string DateCreated { get; set; }
        public int TicketId { get; set; }
    }
}
