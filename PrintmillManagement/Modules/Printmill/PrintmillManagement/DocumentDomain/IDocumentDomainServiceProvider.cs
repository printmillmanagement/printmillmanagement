﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models;
using PrintmillManagement.Modules.Shared;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain
{
    public interface IDocumentDomainServiceProvider
    {
        /// <summary>
        /// Method for downloading document from the server by the provided Identifier
        /// </summary>
        /// <param name="id">Contains Identity Number for document, of type: <see cref="int"/>/param>
        /// <returns><see cref="DocumentDownloadResponse"/>Contains all data of downloaded document</returns>
        Task<DocumentDownloadResponse> DownloadDocumentAsync(int id);

        /// <summary>
        /// Method for uploading document to the server
        /// </summary>
        /// <param name="user">Contains data for user which creating the request, of type: <see cref="SessionUser"/>/param>
        /// <param name="document">Contains data for a document which will be processed, of type: <see cref="Document"/>/param>
        /// <returns><see cref="bool"/>Returns boolean value with a flag either if the document is uploaded or not.</returns>
        Task<bool> UploadDocumentAsync(SessionUser user, Document document);

        /// <summary>
        /// Method for deleting document from the server by the provided Identifier
        /// </summary>
        /// <param name="id">Contains Identity Number for document, of type: <see cref="int"/>/param>
        /// <returns><see cref="bool"/>Returns boolean value with a flag either if the document is deleted or not.</returns>
        Task<bool> DeleteDocumentAsync(int id);
    }
}
