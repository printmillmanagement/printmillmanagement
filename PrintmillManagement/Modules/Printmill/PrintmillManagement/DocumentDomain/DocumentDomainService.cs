﻿using Microsoft.Extensions.Logging;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models;
using PrintmillManagement.Modules.Printmill.UserManagement.DB.Ports;
using PrintmillManagement.Modules.Shared;
using System;
using System.IO;
using System.Net.Mime;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain
{
    public class DocumentDomainService : IDocumentDomainServiceProvider
    {
        private readonly IUserDataProvider _userDataProvider;
        private readonly ITicketDataProvider _ticketDataProvider;
        private readonly IDocumentDataProvider _documentDataProvider;
        private readonly ILogger<DocumentDomainService> _logger;

        public DocumentDomainService(
            IUserDataProvider userDataProvider,
            ITicketDataProvider ticketDataProvider,
            IDocumentDataProvider documentDataProvider,
            ILogger<DocumentDomainService> logger)
        {
            _userDataProvider = userDataProvider;
            _ticketDataProvider = ticketDataProvider;
            _documentDataProvider = documentDataProvider;
            _logger = logger;
        }

        public async Task<DocumentDownloadResponse> DownloadDocumentAsync(int id)
        {
            var selectedDocument = await _documentDataProvider.SelectDocumentByIdAsync(id);
            if (selectedDocument is null) throw new Exception("File is missing.");

            var selectedTicket = await _ticketDataProvider.SelectTicketByIdAsync(selectedDocument.TicketId);
            if (selectedTicket is null) throw new Exception("Ticket for provided document not found.");

            var selectedUser = await _userDataProvider.SelectUserByIdAsync(selectedTicket.CreatedByUserId);
            if (selectedUser is null) throw new Exception("User for provided document not found.");

            var content = new ContentDisposition
            {
                FileName = selectedDocument.Guid,
                Inline = false
            };

            FileStream fileToDownload;

            try
            {
                fileToDownload = File.OpenRead(selectedDocument.Path);

                if (fileToDownload is null) throw new Exception("File is missing.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while reading Document.", selectedDocument);
                throw;
            }

            return new DocumentDownloadResponse
            {
                DocumentName = string.Concat(selectedUser.Uid, "_", selectedDocument.Guid, selectedDocument.Type),
                ContentDisposition = content,
                Document = fileToDownload
            };
        }

        public async Task<bool> UploadDocumentAsync(SessionUser user, Document document)
        {
            bool insertDocumentResult = false;

            string drive = @"C:\";
            string destinationPath = @"PrintmillManagement\Resources\Documents\";
            string pathToSave = Path.Combine(drive, destinationPath);
            string userUid = (await _userDataProvider.SelectUserByIdAsync(user.UserId)).Uid;

            Directory.CreateDirectory(pathToSave);

            if (document.File != null && document.File.Length > 0)
            {
                document.Type = Path.GetExtension(document.File.FileName);
                document.Name = Path.GetFileName(document.File.FileName);
                document.Path = Path.Combine(pathToSave, string.Concat(userUid, "_", document.Guid, document.Type));

                try
                {
                    using (var stream = new FileStream(document.Path, FileMode.Create, FileAccess.Write))
                    {
                        await document.File.CopyToAsync(stream);
                    }

                    insertDocumentResult = await _documentDataProvider.InsertDocumentAsync(document);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error occurred while processing Document.", document);
                    throw;
                }
            }

            return insertDocumentResult;
        }

        public async Task<bool> DeleteDocumentAsync(int id)
        {
            var selectedDocumentResult = await _documentDataProvider.SelectDocumentByIdAsync(id);
            if (selectedDocumentResult is null) throw new Exception("File is missing.");

            try
            {
                File.Delete(selectedDocumentResult.Path);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while deleting file from drive.", selectedDocumentResult);
                return false;
            }
        }
    }
}
