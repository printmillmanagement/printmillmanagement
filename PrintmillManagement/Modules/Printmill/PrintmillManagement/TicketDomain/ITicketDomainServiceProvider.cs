﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Modules.Shared.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain
{
    public interface ITicketDomainServiceProvider
    {
        /// <summary>
        /// Method for retrieving initial data for filter
        /// </summary>
        /// <returns><see cref="TicketInitialFilterDataView"/>Contains all data for filters</returns>
        Task<TicketInitialFilterDataView> GetInitialFilterDataAsync();

        /// <summary>
        /// Method for retrieving all Tickets by provided filters
        /// </summary>
        /// <param name="page"><see cref="int"/>Page number for paging /param>
        /// <param name="pageSize"><see cref="int"/>Page size for paging /param>
        /// <param name="request"><see cref="TicketFilterRequest"/>Filters for filtering data /param>
        /// <returns><see cref="TicketDataView"/>Contains all data for tickets as list</returns>
        Task<PagedResponse<TicketDataView>> SelectAllTicketsByFilterPagingAsync(int page, int pageSize, TicketFilterRequest request);

        /// <summary>
        /// Method for retrieving Ticket details by ticket GUID
        /// </summary>
        /// <param name="ticketGuid"><see cref="string"/>/param>
        /// <returns><see cref="TicketDetailedDataView"/>Contains detailed ticket data for one ticket</returns>
        Task<TicketDetailedDataView> SelectTicketByGuidAsync(string ticketGuid);

        /// <summary>
        /// Method for creating new Ticket
        /// </summary>
        /// <param name="user"><see cref="SessionUser"/>/param>
        /// <param name="request"><see cref="TicketCreateRequest"/>/param>
        /// <returns><see cref="Result"/>Contains result if ticket is created or not</returns>
        Task<Result> InsertTicketAsync(SessionUser user, TicketCreateRequest request);

        /// <summary>
        /// Method for updating Ticket
        /// </summary>
        /// <param name="user"><see cref="SessionUser"/>/param>
        /// <param name="request"><see cref="TicketEditRequest"/>/param>
        /// <param name="ticketGuid"><see cref="string"/>/param>
        /// <returns><see cref="Result"/>Contains result if ticket is updated or not</returns>
        Task<Result> UpdateTicketAsync(SessionUser user, TicketEditRequest request, string ticketGuid);

        /// <summary>
        /// Method for importing solved Tickets from external system
        /// </summary>
        /// <param name="user"><see cref="SessionUser"/>/param>
        /// <param name="request"><see cref="TicketImportSolvedRequest"/>/param>
        /// <returns><see cref="TicketImportSolvedResponse"/>Contains response data for imported tickets</returns>
        Task<List<TicketImportSolvedResponse>> ImportSolvedTicketAsync(SessionUser user, TicketImportSolvedRequest request);

        /// <summary>
        /// Method for updating Ticket status by ticket ID
        /// </summary>
        /// <param name="ticketId"><see cref="int"/>/param>
        /// <param name="statusId"><see cref="int"/>/param>
        /// <returns><see cref="TicketImportSolvedResponse"/>Contains result if ticket status is updated or not</returns>
        Task<Result> UpdateTicketStatusAsync(int ticketId, int statusId);

        /// <summary>
        /// Method for updating Ticket status to accept by ticket ID
        /// </summary>
        /// <param name="ticketId"><see cref="int"/>/param>
        /// <param name="statusId"><see cref="int"/>/param>
        /// <param name="userId"><see cref="int"/>/param>
        /// <returns><see cref="TicketImportSolvedResponse"/>Contains result if ticket status is updated or not</returns>
        Task<Result> AcceptTicketAsync(int ticketId, int statusId, int userId);

        /// <summary>
        /// Method for selecting Ticket document by document ID
        /// </summary>
        /// <param name="documentId"><see cref="int"/>/param>
        /// <returns><see cref="Document"/>Contains detailed ticket document data</returns>
        Task<Document> SelectDocumentByIdAsync(int documentId);

        /// <summary>
        /// Method for deleting Ticket by ticket ID
        /// </summary>
        /// <param name="ticketId"><see cref="int"/>/param>
        /// <returns><see cref="Result"/>Contains result if ticket is deleted or not</returns>
        Task<Result> DeleteTicketAsync(int ticketId);
    }
}
