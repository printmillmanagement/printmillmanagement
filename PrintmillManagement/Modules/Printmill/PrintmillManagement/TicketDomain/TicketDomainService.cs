﻿using FluentResults;
using Microsoft.Extensions.Logging;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Printmill.UserManagement.DB.Ports;
using PrintmillManagement.Modules.Services.NotificationService;
using PrintmillManagement.Modules.Services.NotificationService.Models;
using PrintmillManagement.Modules.Services.NotificationService.Ports;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Modules.Shared.Paging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain
{
    public class TicketDomainService : ITicketDomainServiceProvider
    {
        private readonly ITicketDataProvider _ticketDataProvider;
        private readonly IMaterialDataProvider _materialDataProvider;
        private readonly IMaterialTypeDataProvider _materialTypeDataProvider;
        private readonly IDocumentDataProvider _documentDataProvider;
        private readonly IServiceTypeDataProvider _serviceTypeDataProvider;
        private readonly IItemDataProvider _itemDataProvider;
        private readonly IEmailServiceProvider _emailServiceProvider;
        private readonly IUserDataProvider _userDataProvider;
        private readonly IDocumentDomainServiceProvider _documentDomainService;
        private readonly ILogger<TicketDomainService> _logger;

        public TicketDomainService(
            ITicketDataProvider ticketDataProvider,
            IMaterialDataProvider materialDataProvider,
            IMaterialTypeDataProvider materialTypeDataProvider,
            IDocumentDataProvider documentDataProvider,
            IServiceTypeDataProvider serviceTypeDataProvider,
            IItemDataProvider itemDataProvider,
            IEmailServiceProvider emailServiceProvider,
            IUserDataProvider userDataProvider,
            IDocumentDomainServiceProvider documentDomainService,
            ILogger<TicketDomainService> logger)
        {
            _ticketDataProvider = ticketDataProvider;
            _materialDataProvider = materialDataProvider;
            _materialTypeDataProvider = materialTypeDataProvider;
            _documentDataProvider = documentDataProvider;
            _serviceTypeDataProvider = serviceTypeDataProvider;
            _itemDataProvider = itemDataProvider;
            _emailServiceProvider = emailServiceProvider;
            _userDataProvider = userDataProvider;
            _documentDomainService = documentDomainService;
            _logger = logger;
        }

        public async Task<TicketInitialFilterDataView> GetInitialFilterDataAsync()
        {
            var initData = new TicketInitialFilterDataView();

            var itemsDataResponseTask = _itemDataProvider.SelectAllItemsAsync();
            var materialsDataResponseTask = _materialDataProvider.SelectAllMaterialsAsync();
            var materialTypesDataResponseTask = _materialTypeDataProvider.SelectAllMaterialTypesAsync();
            var serviceTypeDataResponseTask = _serviceTypeDataProvider.SelectAllServiceTypesAsync();

            IEnumerable<Task> tasks = new List<Task>() {
                itemsDataResponseTask,
                materialsDataResponseTask,
                materialTypesDataResponseTask,
                serviceTypeDataResponseTask
            };

            await Task.WhenAll(tasks);

            initData.ItemsData = itemsDataResponseTask.Result.Select(x => new SelectableViewModel { Key = x.Id, Value = x.Name });
            initData.MaterialsData = materialsDataResponseTask.Result.Select(x => new SelectableViewModel { Key = x.Id, Value = x.Name });
            initData.MaterialTypesData = materialTypesDataResponseTask.Result.Select(x => new SelectableViewModel { Key = x.Id, Value = x.Name });
            initData.ServiceTypeData = serviceTypeDataResponseTask.Result.Select(x => new SelectableViewModel { Key = x.Id, Value = x.Name });

            return initData;
        }

        public async Task<PagedResponse<TicketDataView>> SelectAllTicketsByFilterPagingAsync(int page, int pageSize, TicketFilterRequest request)
        {
            var paging = new PagingRequest(page, pageSize);

            var ticketsDataResponseTask = _ticketDataProvider.SelectAllTicketsByFilterPagingAsync(paging, request);
            var ticketsCountDataResponseTask = _ticketDataProvider.SelectAllTicketsByFilterAsync(request);

            IEnumerable<Task> tasks = new List<Task>() {
                ticketsDataResponseTask,
                ticketsCountDataResponseTask
            };

            await Task.WhenAll(tasks);

            var ticketsDataResponseTaskResult = ticketsDataResponseTask.Result;
            var ticketsCountDataResponseTaskResult = ticketsCountDataResponseTask.Result.Count;

            return new PagedResponse<TicketDataView>(ticketsDataResponseTaskResult, paging.Page, paging.PageSize, ticketsCountDataResponseTaskResult);
        }

        public async Task<TicketDetailedDataView> SelectTicketByGuidAsync(string ticketGuid)
        {
            var ticketDetailed = new TicketDetailedDataView();

            var ticketResponse = await _ticketDataProvider.SelectTicketByGuidAsync(ticketGuid);
            if (ticketResponse is null) return ticketDetailed;

            var createdByUserResponseTask = _userDataProvider.SelectUserViewByIdAsync(ticketResponse.CreatedByUserId);
            var processedByUserResponseTask = _userDataProvider.SelectUserViewByIdAsync(ticketResponse.ProcessedByUserId);
            var documentResponseTask = _documentDataProvider.SelectDocumentByTicketIdAsync(ticketResponse.Id);

            IEnumerable<Task> tasks = new List<Task>() {
                createdByUserResponseTask,
                processedByUserResponseTask,
                documentResponseTask
            };

            await Task.WhenAll(tasks);

            ticketDetailed.Ticket = ticketResponse;
            ticketDetailed.CreatedByUser = createdByUserResponseTask.Result;
            ticketDetailed.ProcessedByUser = processedByUserResponseTask.Result;
            ticketDetailed.Document = documentResponseTask.Result;

            return ticketDetailed;
        }  

        public async Task<Result> InsertTicketAsync(SessionUser user, TicketCreateRequest ticketCreateRequest)
        {
            if (!(await ValidateUser(user))) return Result.Fail("Error occurred while validating user");

            string guid = System.Guid.NewGuid().ToString().ToUpper();

            var ticket = new Ticket(ticketCreateRequest, user.UserId, guid);
            int insertTicketResult = await _ticketDataProvider.InsertTicketAsync(ticket);
            if (insertTicketResult == 0) return Result.Fail("Error occurred while creating ticket");

            var document = new Document(ticketCreateRequest, insertTicketResult, guid);
            var uploadDocument = await _documentDomainService.UploadDocumentAsync(user, document);
            if (!uploadDocument) return Result.Fail("Error occurred while saving document");

            Task.Factory.StartNew(() => { SendNotificationAsync(userId: user.UserId, ticketStatus: TicketStatus.Open); });

            return Result.Ok().WithSuccess("Ticket Created");
        }

        public async Task<Result> UpdateTicketAsync(SessionUser user, TicketEditRequest ticketEditRequest, string ticketGuid)
        {
            if (!(await ValidateUser(user))) return Result.Fail("Error occurred while validating ticket");

            var oldTicketResult = await _ticketDataProvider.SelectTicketByGuidAsync(ticketGuid);
            if (oldTicketResult is null) return Result.Fail("Error occurred while selecting ticket to update");

            var ticket = new Ticket(ticketEditRequest, oldTicketResult.Id);

            var updateResult = await _ticketDataProvider.UpdateTicketAsync(ticket);
            if (!updateResult) return Result.Fail("Error occurred while updating ticket");

            return Result.Ok().WithSuccess("Ticket Updated");
        }

        public async Task<List<TicketImportSolvedResponse>> ImportSolvedTicketAsync(SessionUser user, TicketImportSolvedRequest request)
        {
            var ticketImportSolvedResponse = new List<TicketImportSolvedResponse>();
            XmlDocument doc = new XmlDocument();
            var ticketsToImport = new List<string>();

            using (var fileStream = request.File.OpenReadStream())
            {
                doc.Load(fileStream);
            }

            foreach (XmlNode node in doc.DocumentElement)
            {
                string attr = node.Attributes["InputFileName"]?.InnerText;
                string filename = attr.Substring(attr.LastIndexOf('\\') + 4); // TODO - better algorithm for getting substring from a string based on logged user Uid
                string extension = Path.GetExtension(filename);
                string result = filename.Substring(0, filename.Length - extension.Length);
                ticketsToImport.Add(result);
            }

            if (ticketsToImport.Count > 0)
            {
                var selectedDocuments = await _documentDataProvider.SelectDocumentsForImportSolvedAsync(ticketsToImport, user);
                var ticketsToUpdate = selectedDocuments.Select(x => x.TicketId).ToList();
                bool ticketIsUpdated = await _ticketDataProvider.UpdateTicketStatusByIdListAsync(ticketsToUpdate, (int)TicketStatus.Finished);

                var imported = selectedDocuments.Select(x => x.Guid).ToList();
                foreach (var item in imported)
                {
                    ticketImportSolvedResponse.Add(new TicketImportSolvedResponse { Guid = item, Success = true });
                }

                var notImported = ticketsToImport.Except(imported);
                foreach (var item in notImported)
                {
                    ticketImportSolvedResponse.Add(new TicketImportSolvedResponse { Guid = item, Success = false });
                }
            }

            return ticketImportSolvedResponse;
        }

        public async Task<Result> UpdateTicketStatusAsync(int ticketId, int statusId)
        {
            var ticketsToUpdate = new List<int>
            {
                ticketId
            };

            bool ticketIsUpdated = await _ticketDataProvider.UpdateTicketStatusByIdListAsync(ticketsToUpdate, statusId);

            if (!ticketIsUpdated) return Result.Fail("Error occurred while updating ticket status");

            return Result.Ok().WithSuccess("Ticket status updated");
        }

        public async Task<Result> AcceptTicketAsync(int ticketId, int statusId, int userId)
        {
            var ticket = await _ticketDataProvider.SelectTicketByIdAsync(ticketId);
            if (ticket.ProcessedByUserId > 0) return Result.Fail("Ticket is already accepted");

            bool ticketIsUpdated = await _ticketDataProvider.AcceptTicketByUserAsync(ticketId, statusId, userId);
            if (!ticketIsUpdated) return Result.Fail("Error occurred while updating ticket status");

            return Result.Ok().WithSuccess("Ticket accepted");
        }

        public async Task<Document> SelectDocumentByIdAsync(int documentId)
        {
            return await _documentDataProvider.SelectDocumentByIdAsync(documentId);
        }

        public async Task<Result> DeleteTicketAsync(int ticketId)
        {
            var selectDocumentByTicketIdResult = await _documentDataProvider.SelectDocumentByTicketIdAsync(ticketId);
            if (selectDocumentByTicketIdResult is null) return Result.Fail("Error occurred while selecting Document By Ticket Id.");

            var deleteDocumentOnDiscResult = await _documentDomainService.DeleteDocumentAsync(selectDocumentByTicketIdResult.Id);
            if (!deleteDocumentOnDiscResult) return Result.Fail("Error occurred while deleting Document on Disc.");

            var deleteDocumentResult = await _documentDataProvider.DeleteDocumentAsync(selectDocumentByTicketIdResult.Id);
            if (!deleteDocumentResult) return Result.Fail("Error occurred while deleting Document.");         

            var deleteTicketResult = await _ticketDataProvider.DeleteTicketAsync(ticketId);

            if (deleteTicketResult)
            {
                return Result.Ok().WithSuccess("Ticket Deleted.");
            }
            else
            {
                return Result.Fail("Failed To Delete Ticket");
            }
        }

        /// <summary>
        /// Method used for processing notification.
        /// </summary>
        /// <param name="userId">Contains user for which notification will be sended: <see cref="int"/>/param>
        /// <param name="ticketStatus">Contains type of notification: <see cref="TicketStatus"/>/param>
        private async Task SendNotificationAsync(int userId, TicketStatus ticketStatus)
        {
            var userData = await _userDataProvider.SelectUserByIdAsync(userId);
            if (userData is null) throw new Exception("Error occurred while fetching user for email.");

            var mailRequest = new MailRequest
            {
                ToEmail = userData.Email,
                Body = NotificationBody.CompositeNewBodyNotification(ticketStatus),
                Subject = NotificationBody.CompositeNewSubjectNotification(ticketStatus)
            };

            await _emailServiceProvider.SendEmailAsync(mailRequest);
        }

        /// <summary>
        /// Method used for validating provided user with user in system.
        /// </summary>
        /// <param name="user">Contains data for a user which is returned on authentication, of type: <see cref="SessionUser"/>/param>
        /// <returns><see cref="bool"/>Returns boolean value with a flag either if the user is valid or not.</returns>
        private async Task<bool> ValidateUser(SessionUser user)
        {
            bool userIsValid = false;

            var userOnRequest = await _userDataProvider.SelectUserByIdAsync(user.UserId);

            if (userOnRequest != null) userIsValid = user.UserId == userOnRequest.Id;

            return userIsValid;
        }
    }
}
