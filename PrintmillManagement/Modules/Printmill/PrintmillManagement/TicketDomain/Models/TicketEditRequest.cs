﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public class TicketEditRequest
    {
        public string NumberOfUnits { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
    }
}
