﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain.Models;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain.Models;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain.Models;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain.Models;
using PrintmillManagement.Modules.Shared;
using System;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public class Ticket
    {
        public Ticket()
        {

        }

        public Ticket(TicketCreateRequest request, int userId, string guid)
        {
            Guid = guid;
            CreatedByUserId = userId;
            NumberOfUnits = int.Parse(request.NumberOfUnits);
            Description = request.Description;
            Note = request.Note;
            Priority = string.IsNullOrWhiteSpace(request.Priority) ? TicketPriority.Low : (TicketPriority)Enum.Parse(typeof(TicketPriority), request.Priority);
            Status = TicketStatus.Open;
            ServiceTypeId = int.Parse(request.ServiceTypeId);
            MaterialId = int.Parse(request.MaterialId);
            MaterialTypeId = int.Parse(request.MaterialTypeId);
            ItemId = int.Parse(request.ItemId);
            DateCreated = DateTime.Now.ToString(SettingsConstants.DateTimeFormat);
        }

        public Ticket(TicketEditRequest request, int id)
        {
            Id = id;
            NumberOfUnits = int.Parse(request.NumberOfUnits);
            Description = request.Description;
            Note = request.Note;
        }

        public int Id { get; set; }
        public string Guid { get; set; }
        public int CreatedByUserId { get; set; }
        public int? ProcessedByUserId { get; set; }
        public int NumberOfUnits { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string DateCreated { get; set; }
        public TicketPriority Priority { get; set; }
        public TicketStatus Status { get; set; }
        
        public int ServiceTypeId { get; set; }
        public ServiceType ServiceType { get; set; }
        public int MaterialId { get; set; }
        public Material Material { get; set; }
        public int MaterialTypeId { get; set; }
        public MaterialType MaterialType { get; set; }
        public int ItemId { get; set; }
        public Item Item { get; set; }
    }
}
