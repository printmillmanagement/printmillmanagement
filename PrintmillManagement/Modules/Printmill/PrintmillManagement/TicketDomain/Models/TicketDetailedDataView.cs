﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models;
using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public class TicketDetailedDataView
    {
        public TicketDetailedDataView()
        {
            Ticket = new TicketDataView();
            CreatedByUser = new UserDataView();
            ProcessedByUser = new UserDataView();
            Document = new DocumentDataView();
        }

        public TicketDataView Ticket { get; set; }
        public UserDataView CreatedByUser { get; set; }
        public UserDataView ProcessedByUser { get; set; }
        public DocumentDataView Document { get; set; }
    }
}
