﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public class TicketFilterRequest
    {
        public string TicketGuid { get; set; }
        public string DocumentGuid { get; set; }
        public int? PriorityId { get; set; }
        public int? StatusId { get; set; }
        public string ServiceType { get; set; }
        public string Material { get; set; }
        public string MaterialType { get; set; }
        public string Item { get; set; }
        public int? ProcessedByUserId { get; set; }
        public int? CreatedByUserId { get; set; }

        public bool HaveFilter => !string.IsNullOrEmpty(TicketGuid)
            || !string.IsNullOrWhiteSpace(DocumentGuid)
            || !string.IsNullOrWhiteSpace(ServiceType)
            || !string.IsNullOrWhiteSpace(Material)
            || !string.IsNullOrWhiteSpace(MaterialType)
            || !string.IsNullOrWhiteSpace(Item)
            || PriorityId.HasValue
            || StatusId.HasValue
            || ProcessedByUserId.HasValue
            || CreatedByUserId.HasValue;
    }
}
