﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public class TicketDataView
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public int CreatedByUserId { get; set; }
        public int ProcessedByUserId { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string Code { get; set; }
        public string DateCreated { get; set; }
        public string Priority { get; set; }
        public int PriorityId { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
        public int ServiceTypeId { get; set; }
        public string ServiceType { get; set; }
        public int MaterialId { get; set; }
        public string Material { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialType { get; set; }
        public int ItemId { get; set; }
        public string Item { get; set; }
        public int NumberOfUnits { get; set; }
    }
}
