﻿using Microsoft.AspNetCore.Http;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public class TicketCreateRequest
    {
        public string ServiceTypeId { get; set; }
        public string MaterialId { get; set; }
        public string MaterialTypeId { get; set; }
        public string ItemId { get; set; }
        public string NumberOfUnits { get; set; }
        public string Priority { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public IFormFile File { get; set; }
    }
}
