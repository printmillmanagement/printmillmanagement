﻿using System.ComponentModel.DataAnnotations;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public enum TicketPriority
    {
        [Display(Name = "High")]
        High = 1,

        [Display(Name = "Medium")]
        Medium = 2,

        [Display(Name = "Low")]
        Low = 3
    }
}
