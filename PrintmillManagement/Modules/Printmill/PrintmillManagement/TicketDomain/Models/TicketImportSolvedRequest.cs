﻿using Microsoft.AspNetCore.Http;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public class TicketImportSolvedRequest
    {
        public IFormFile File { get; set; }
        public string FileName { get; set; }
    }
}
