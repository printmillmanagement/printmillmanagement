﻿using PrintmillManagement.Modules.Shared;
using System.Collections.Generic;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public class TicketInitialFilterDataView
    {
        public IEnumerable<SelectableViewModel> ItemsData { get; set; }
        public IEnumerable<SelectableViewModel> MaterialsData { get; set; }
        public IEnumerable<SelectableViewModel> MaterialTypesData { get; set; }
        public IEnumerable<SelectableViewModel> ServiceTypeData { get; set; }
    }
}
