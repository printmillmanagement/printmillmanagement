﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models
{
    public class TicketImportSolvedResponse
    {
        public string Guid { get; set; }
        public bool Success { get; set; }
    }
}
