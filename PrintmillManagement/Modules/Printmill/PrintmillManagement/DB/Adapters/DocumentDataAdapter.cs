﻿using Dapper;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models;
using PrintmillManagement.Modules.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Adapters
{
    public class DocumentDataAdapter : IDocumentDataProvider
    {
        private readonly string _connectionString;
        private readonly ILogger<DocumentDataAdapter> _logger;

        public DocumentDataAdapter(
            string connectionString,
            ILogger<DocumentDataAdapter> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public async Task<bool> DeleteDocumentAsync(int id)
        {
            var methodParams = new
            {
                id
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string deleteQuery = @"DELETE FROM document WHERE Id = @id";

                    await connection.ExecuteAsync(deleteQuery, new { id });
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while deleting record in Document table.", methodParams);
                throw;
            }
        }

        public async Task<bool> InsertDocumentAsync(Document document)
        {
            var methodParams = new
            {
                document
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string insertQuery = @"INSERT INTO document " +
                                          "(guid, size, name, type, path, ticketid, datecreated) " +
                                          "VALUES (@guid, @size, @name, @type, @path, @ticketid, @datecreated)";

                    await connection.ExecuteAsync(insertQuery, new
                    {
                        document.Guid,
                        document.Size,
                        document.Name,
                        document.Type,
                        document.Path,
                        document.TicketId,
                        document.DateCreated
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while inserting record in Document table.", methodParams);
                throw;
            }
        }

        public async Task<List<Document>> SelectAllDocumentsAsync()
        {
            var documents = new List<Document>();
            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string selectQuery = "SELECT * FROM document";

                    documents = (await connection.QueryAsync<Document>(selectQuery)).ToList();

                    return documents;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record from Document table.", null);
                throw;
            }
        }

        public async Task<List<Document>> SelectDocumentsForImportSolvedAsync(List<string> guids, SessionUser user)
        {
            var documents = new List<Document>();
            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string selectQuery = @"SELECT * 
                                             FROM document 
                                             LEFT JOIN ticket ON ticket.Id = document.TicketId
                                            WHERE document.Guid IN @guids
                                              AND ticket.status = 3
                                              AND ticket.ProcessedByUserId = @UserId";

                    documents = (await connection.QueryAsync<Document>(selectQuery, new { guids, user.UserId })).ToList();

                    return documents;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record from Document table by Guid List.", guids);
                throw;
            }
        }

        public async Task<Document> SelectDocumentByIdAsync(int id)
        {
            var methodParams = new
            {
                id
            };

            var document = new Document();
            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string selectQuery = "SELECT * FROM document WHERE Id = @id ";

                    document = (await connection.QueryAsync<Document>(selectQuery, new { id })).FirstOrDefault();

                    return document;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ById from Document table.", methodParams);
                throw;
            }
        }

        public async Task<DocumentDataView> SelectDocumentByTicketIdAsync(int id)
        {
            var methodParams = new
            {
                id
            };

            var document = new DocumentDataView();
            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string selectQuery = "SELECT Id, Guid, Size, Name, Type, Path, DateCreated, TicketId " +
                                           "FROM document " +
                                          "WHERE ticketid = @id ";

                    document = (await connection.QueryAsync<DocumentDataView>(selectQuery, new { id })).FirstOrDefault();

                    return document;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record TicketById from Document table.", methodParams);
                throw;
            }
        }
    }
}
