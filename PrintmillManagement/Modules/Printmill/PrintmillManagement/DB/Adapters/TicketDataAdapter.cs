﻿using Dapper;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Shared.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Adapters
{
    public class TicketDataAdapter : ITicketDataProvider
    {
        private readonly string _connectionString;
        private readonly ILogger<TicketDataAdapter> _logger;

        public TicketDataAdapter(
            string connectionString,
            ILogger<TicketDataAdapter> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public async Task<bool> DeleteTicketAsync(int ticketId)
        {
            var methodParams = new
            {
                ticketId
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string deleteQuery = @"DELETE FROM ticket WHERE Id = @TicketId";

                await connection.ExecuteAsync(deleteQuery, new { ticketId });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while deleting record in Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<int> InsertTicketAsync(Ticket printmillTicket)
        {
            var methodParams = new
            {
                printmillTicket
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string insertQuery = @"INSERT INTO ticket " +
                                          "(guid, createdbyuserid, description, note, numberofunits, status, priority, servicetypeid, materialid, materialtypeid, itemid,  datecreated) " +
                                          "VALUES (@guid, @createdbyuserid, @description, @note, @numberofunits, @status, @priority, @servicetypeid, @materialid, @materialtypeid, @itemid, @datecreated); " +
                                          "SELECT LAST_INSERT_ID();";

                    int lastInsertId = await connection.ExecuteScalarAsync<int>(insertQuery, new
                    {
                        printmillTicket.Guid,
                        printmillTicket.CreatedByUserId,
                        printmillTicket.Description,
                        printmillTicket.Note,
                        printmillTicket.NumberOfUnits,
                        printmillTicket.Status,
                        printmillTicket.Priority,
                        printmillTicket.ServiceTypeId,
                        printmillTicket.MaterialId,
                        printmillTicket.MaterialTypeId,
                        printmillTicket.ItemId,
                        printmillTicket.DateCreated
                    });

                    return lastInsertId;
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while inserting record in Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<List<TicketDataView>> SelectAllTicketsByFilterPagingAsync(PagingRequest paging, TicketFilterRequest request)
        {
            var methodParams = new
            {
                request
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = SelectTicketQueryString();
                selectQuery += FilterTicketQueryString(request);
                selectQuery += $" ORDER BY ticket.Id DESC LIMIT @PageSize OFFSET @Offset";

                return (await connection.QueryAsync<TicketDataView>(selectQuery, 
                    new 
                    { 
                        request.TicketGuid,
                        request.DocumentGuid,
                        request.StatusId,
                        request.PriorityId,
                        request.ServiceType,
                        request.Material,
                        request.MaterialType,
                        request.Item,
                        request.ProcessedByUserId,
                        request.CreatedByUserId,
                        paging.PageSize,
                        paging.Offset
                    } )).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from Ticket table with filter.", methodParams);
                throw;
            }
        }

        public async Task<List<TicketDataView>> SelectAllTicketsByFilterAsync(TicketFilterRequest request)
        {
            var methodParams = new
            {
                request
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = SelectTicketQueryString();
                selectQuery += FilterTicketQueryString(request);
                selectQuery += $" ORDER BY ticket.Id DESC ";

                return (await connection.QueryAsync<TicketDataView>(selectQuery, request)).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<TicketDataView> SelectTicketByGuidAsync(string ticketGuid)
        {
            var methodParams = new
            {
                ticketGuid
            };

            var printmillTicket = new TicketDataView();

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = SelectTicketQueryString();
                selectQuery += $" WHERE ticket.Guid = @ticketGuid";

                printmillTicket = (await connection.QueryAsync<TicketDataView>(selectQuery, new { ticketGuid })).FirstOrDefault();

                return printmillTicket;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ByGuid from Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<TicketDataView> SelectTicketByIdAsync(int ticketId)
        {
            var methodParams = new
            {
                ticketId
            };

            var printmillTicket = new TicketDataView();

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = SelectTicketQueryString();
                selectQuery += $" WHERE ticket.Id = @ticketId";

                printmillTicket = (await connection.QueryAsync<TicketDataView>(selectQuery, new { ticketId })).FirstOrDefault();

                return printmillTicket;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ById from Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<List<TicketDataView>> SelectTicketsByIdListAsync(List<int> ticketIds)
        {
            var methodParams = new
            {
                ticketIds
            };

            var printmillTicket = new List<TicketDataView>();

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = SelectTicketQueryString();
                selectQuery += $" WHERE ticket.Id IN @ticketIds";

                printmillTicket = (await connection.QueryAsync<TicketDataView>(selectQuery, new { ticketIds })).ToList();

                return printmillTicket;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ById from Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<bool> UpdateTicketStatusByIdListAsync(List<int> tickets, int status)
        {
            var methodParams = new
            {
                tickets
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string updateQuery = @"UPDATE ticket " +
                                         "SET status = @status " +
                                       "WHERE id IN @tickets ";

                await connection.ExecuteAsync(updateQuery, new { status, tickets });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating record ById in Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<bool> AcceptTicketByUserAsync(int ticketId, int status, int userId)
        {
            var methodParams = new
            {
                ticketId,
                status,
                userId
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string updateQuery = @"UPDATE ticket " +
                                         "SET status = @status, ProcessedByUserId = @userId " +
                                       "WHERE id = @ticketId AND ProcessedByUserId is null ";

                await connection.ExecuteAsync(updateQuery, new { status, userId, ticketId });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating record ById in Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<bool> UpdateTicketAsync(Ticket ticket)
        {
            var methodParams = new
            {
                ticket
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string updateQuery = @"UPDATE ticket
                                          SET description = @Description,
                                              note = @Note,
                                              numberofunits = @NumberOfUnits
                                        WHERE Id = @Id";

                await connection.ExecuteAsync(updateQuery, new { ticket.Description, ticket.Note, ticket.NumberOfUnits, ticket.Id });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating record ById in Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<int> SelectTicketStatisticsForProcessedByUserIdAsync(int? userId, int statusId)
        {
            var methodParams = new
            {
                userId,
                statusId
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = "SELECT count(*) FROM ticket";

                string filterSQL = "";

                if (userId.HasValue)
                {
                    filterSQL += @" ProcessedByUserId = @userId";
                }

                if (!String.IsNullOrWhiteSpace(filterSQL))
                {
                    filterSQL += " AND";
                }
    
                filterSQL += @" Status = @statusId";

                selectQuery += $" WHERE {filterSQL}";

                return await connection.ExecuteScalarAsync<int>(selectQuery, new { userId, statusId });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting count statistics from Ticket table.", methodParams);
                throw;
            }
        }

        public async Task<int> SelectTicketStatisticsForCreatedByUserIdAsync(int userId, int statusId)
        {
            var methodParams = new
            {
                userId,
                statusId
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = "SELECT count(*) FROM ticket " +
                                      "WHERE Status = @statusId " +
                                        "AND CreatedByUserId = @userId";

                return await connection.ExecuteScalarAsync<int>(selectQuery, new { statusId, userId });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting count statistics from Ticket table.", methodParams);
                throw;
            }
        }

        private string SelectTicketQueryString()
        {
            return @"SELECT ticket.Id,
		                    ticket.Guid,
		                    ticket.CreatedByUserId,
		                    ticket.ProcessedByUserId,
		                    ticket.Description,
		                    ticket.Note,
		                    ticket.Status AS StatusId,
		                    CASE ticket.Status
                                WHEN 1 THEN 'Open'
                                WHEN 2 THEN 'Accepted'
                                WHEN 3 THEN 'In Progress'
                                WHEN 4 THEN 'Finished'
                                WHEN 5 THEN 'Needs Add. Work'
                                WHEN 6 THEN 'In Delivery'
                            ELSE '' end Status,
                            ticket.Priority AS PriorityId,
                            CASE ticket.Priority
                                WHEN 1 THEN 'High'
                                WHEN 2 THEN 'Medium'
                                WHEN 3 THEN 'Low'
                            ELSE '' end Priority,
                            ticket.ServiceTypeId,
		                    servicetype.Name AS ServiceType,
		                    ticket.MaterialId,
		                    material.Name AS Material,
		                    ticket.MaterialTypeId,
		                    materialtype.Name AS MaterialType,
		                    ticket.ItemId,
		                    item.Name AS Item,
                            ticket.NumberOfUnits,
                            ticket.DateCreated
                        FROM ticket
                        LEFT JOIN servicetype ON ticket.ServiceTypeId = servicetype.Id
                        LEFT JOIN material ON ticket.MaterialId = material.Id
                        LEFT JOIN materialtype ON ticket.MaterialTypeId = materialtype.Id
                        LEFT JOIN item ON ticket.ItemId = item.Id
                        LEFT JOIN document ON ticket.Id = document.TicketId";
        }

        private string FilterTicketQueryString(TicketFilterRequest request)
        {
            string filterQuery = string.Empty;

            if (request.HaveFilter)
            {
                string filterSQL = "";

                if (!String.IsNullOrWhiteSpace(request.TicketGuid))
                {
                    filterSQL += @" ticket.Guid = @TicketGuid";
                }
                if (!String.IsNullOrWhiteSpace(request.DocumentGuid))
                {
                    if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";
                    filterSQL += @" document.Guid = @DocumentGuid";
                }
                if (request.StatusId.HasValue)
                {
                    if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";
                    filterSQL += @" ticket.Status = @StatusId";
                }
                if (request.PriorityId.HasValue)
                {
                    if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";
                    filterSQL += @" ticket.Priority = @PriorityId";
                }
                if (!String.IsNullOrWhiteSpace(request.ServiceType))
                {
                    if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";
                    filterSQL += @" ticket.ServiceTypeId = @ServiceType";
                }
                if (!String.IsNullOrWhiteSpace(request.Material))
                {
                    if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";
                    filterSQL += @" ticket.MaterialId = @Material";
                }
                if (!String.IsNullOrWhiteSpace(request.MaterialType))
                {
                    if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";
                    filterSQL += @" ticket.MaterialTypeId = @MaterialType";
                }
                if (!String.IsNullOrWhiteSpace(request.Item))
                {
                    if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";
                    filterSQL += @" ticket.ItemId = @Item";
                }
                
                if (request.ProcessedByUserId.HasValue)
                {
                    if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";
                    filterSQL += @" (ticket.ProcessedByUserId = @ProcessedByUserId OR ticket.ProcessedByUserId is null)";
                }
                else if (request.CreatedByUserId.HasValue)
                {
                    if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";
                    filterSQL += @" ticket.CreatedByUserId = @CreatedByUserId";
                }

                filterQuery += $" WHERE {filterSQL}";
            }

            return filterQuery;
        }
    }
}
