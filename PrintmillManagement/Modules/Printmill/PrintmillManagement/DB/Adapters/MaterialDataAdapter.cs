﻿using Dapper;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Adapters
{
    public class MaterialDataAdapter : IMaterialDataProvider
    {
        private readonly string _connectionString;
        private readonly ILogger<MaterialDataAdapter> _logger;

        public MaterialDataAdapter(
            string connectionString,
            ILogger<MaterialDataAdapter> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public async Task<bool> DeleteMaterialAsync(int materialId)
        {
            var methodParams = new
            {
                materialId
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string deleteQuery = @"DELETE FROM material WHERE Id = @materialTypeId";

                    await connection.ExecuteAsync(deleteQuery, new { materialId });
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while deleting record ById in Material table.", methodParams);
                throw;
            }
        }

        public async Task<bool> InsertMaterialAsync(Material material)
        {
            var methodParams = new
            {
                material
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string insertQuery = @"INSERT INTO material " +
                                          "(guid, name, description, note, datecreated) " +
                                          "VALUES (@guid, @name, @description, @note, @datecreated)";

                    await connection.ExecuteAsync(insertQuery, new
                    {
                        material.Guid,
                        material.Name,
                        material.Description,
                        material.Note,
                        material.DateCreated
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while inserting record in Material table.", methodParams);
                throw;
            }
        }

        public async Task<List<Material>> SelectAllMaterialsAsync()
        {
            var material = new List<Material>();
            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string selectQuery = "SELECT * FROM material";

                    material = (await connection.QueryAsync<Material>(selectQuery)).ToList();

                    return material;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from Material table.", null);
                throw;
            }
        }

        public async Task<List<Material>> SelectMaterialsByServiceTypeIdAsync(int serviceTypeId)
        {
            var material = new List<Material>();
            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string selectQuery = " SELECT * " +
                                         "   FROM material " +
                                         "  WHERE Id IN " +
                                         "(SELECT materialId " +
                                         "   FROM servicetypematerial " +
                                         "  WHERE servicetypeid = @serviceTypeId)";

                    material = (await connection.QueryAsync<Material>(selectQuery, new { serviceTypeId })).ToList();

                    return material;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from Material table.", null);
                throw;
            }
        }

        public async Task<Material> SelectMaterialByIdAsync(int materialId)
        {
            var methodParams = new
            {
                materialId
            };

            var material = new Material();
            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string selectQuery = "SELECT * FROM material WHERE Id = @materialId ";

                    material = (await connection.QueryAsync<Material>(selectQuery, new { materialId })).FirstOrDefault();

                    return material;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ById from Material table.", methodParams);
                throw;
            }
        }

        public async Task<bool> UpdateMaterialAsync(int materialId, Material material)
        {
            var methodParams = new
            {
                materialId,
                material
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string updateQuery = @"UPDATE material " +
                                          "WHERE Id = @materialId ";

                    await connection.ExecuteAsync(updateQuery, new { materialId, material });
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating record ById in Material table.", methodParams);
                throw;
            }
        }
    }
}
