﻿using Dapper;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Adapters
{
    public class ItemDataAdapter : IItemDataProvider
    {
        private readonly string _connectionString;
        private readonly ILogger<ItemDataAdapter> _logger;

        public ItemDataAdapter(
            string connectionString,
            ILogger<ItemDataAdapter> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public async Task<bool> DeleteItemAsync(int itemId)
        {
            var methodParams = new
            {
                itemId
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string deleteQuery = @"DELETE FROM item WHERE Id = @itemId";

                    await connection.ExecuteAsync(deleteQuery, new { itemId });
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while deleting in Item table.", methodParams);
                throw;
            }
        }

        public async Task<bool> InsertItemAsync(Item item)
        {
            var methodParams = new
            {
                item
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string insertQuery = @"INSERT INTO item " +
                                          "(guid, name, description, note, datecreated) " +
                                          "VALUES (@guid, @name, @description, @note, @datecreated)";

                    await connection.ExecuteAsync(insertQuery, new
                    {
                        item.Guid,
                        item.Name,
                        item.Description,
                        item.Note,
                        item.DateCreated
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while inserting in Item table.", methodParams);
                throw;
            }
        }

        public async Task<List<Item>> SelecItemsByServiceTypeAndMaterialTypeAsync(int serviceTypeId, int materialTypeId)
        {
            var methodParams = new
            {
                serviceTypeId,
                materialTypeId
            };

            var items = new List<Item>();

            try
            {
                using var connection = new MySqlConnection(_connectionString);
                string selectQuery = @"SELECT * FROM item
                                        WHERE id IN (
                                       SELECT itemId
                                         FROM servicetypematerialtypeitem
                                        WHERE serviceTypeId = @serviceTypeId
                                          AND materialTypeId = @materialTypeId )";

                items = (await connection.QueryAsync<Item>(selectQuery, new { serviceTypeId, materialTypeId })).ToList();

                return items;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting Item table by provided parameters. (method: SelecItemsByServiceTypeAndMaterialTypeAsync)", methodParams);
                throw;
            }
        }

        public async Task<List<Item>> SelectAllItemsAsync()
        {
            var items = new List<Item>();

            try
            {
                using var connection = new MySqlConnection(_connectionString);
                string selectQuery = "SELECT * FROM item";

                items = (await connection.QueryAsync<Item>(selectQuery)).ToList();

                return items;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting Item table. (method: SelectAllItemsAsync)", null);
                throw;
            }
        }

        public async Task<Item> SelectItemByIdAsync(int itemId)
        {
            var methodParams = new
            {
                itemId
            };

            var item = new Item();

            try
            {
                using var connection = new MySqlConnection(_connectionString);
                string selectQuery = "SELECT * FROM item WHERE Id = @itemId ";

                item = (await connection.QueryAsync<Item>(selectQuery, new { itemId })).FirstOrDefault();

                return item;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting Item table. (method: SelectItemByIdAsync)", methodParams);
                throw;
            }
        }

        public async Task<bool> UpdateItemAsync(int itemId, Item item)
        {
            var methodParams = new
            {
                itemId,
                item
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string updateQuery = @"UPDATE item " +
                                          "WHERE Id = @itemId ";

                    await connection.ExecuteAsync(updateQuery, new { itemId, item });
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating Item table.", methodParams);
                throw;
            }
        }
    }
}
