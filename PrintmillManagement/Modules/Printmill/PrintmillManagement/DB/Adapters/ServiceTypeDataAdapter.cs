﻿using Dapper;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Adapters
{
    public class ServiceTypeDataAdapter : IServiceTypeDataProvider
    {
        private readonly string _connectionString;
        private readonly ILogger<ServiceTypeDataAdapter> _logger;

        public ServiceTypeDataAdapter(
            string connectionString,
            ILogger<ServiceTypeDataAdapter> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public async Task<bool> InsertServiceTypeAsync(ServiceType serviceType)
        {
            var methodParams = new
            {
                serviceType
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string insertQuery = @"INSERT INTO servicetype " +
                                          "(guid, name, description, note, datecreated) " +
                                          "VALUES (@guid, @name, @description, @note, @datecreated)";

                    await connection.ExecuteAsync(insertQuery, new
                    {
                        serviceType.Guid,
                        serviceType.Name,
                        serviceType.Description,
                        serviceType.Note,
                        serviceType.DateCreated
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while inserting record in ServiceType table.", methodParams);
                throw;
            }
        }

        public async Task<List<ServiceType>> SelectAllServiceTypesAsync()
        {
            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string selectQuery = "SELECT * FROM servicetype";

                    var serviceTypes = (await connection.QueryAsync<ServiceType>(selectQuery)).ToList();

                    return serviceTypes;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from ServiceType table.", null);
                throw;
            }
        }
    }
}
