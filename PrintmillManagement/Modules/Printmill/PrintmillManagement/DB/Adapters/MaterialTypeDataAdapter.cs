﻿using Dapper;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Adapters
{
    public class MaterialTypeDataAdapter : IMaterialTypeDataProvider
    {
        private readonly string _connectionString;
        private readonly ILogger<MaterialTypeDataAdapter> _logger;

        public MaterialTypeDataAdapter(
            string connectionString,
            ILogger<MaterialTypeDataAdapter> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public async Task<bool> DeleteMaterialTypeAsync(int materialTypeId)
        {
            var methodParams = new
            {
                materialTypeId
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);
                
                string deleteQuery = @"DELETE FROM materialtype WHERE Id = @materialTypeId";

                await connection.ExecuteAsync(deleteQuery, new { materialTypeId });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while deleting record ById in MaterialType table.", methodParams);
                throw;
            }
        }

        public async Task<bool> InsertMaterialTypeAsync(MaterialType materialType)
        {
            var methodParams = new
            {
                materialType
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);
                
                string insertQuery = @"INSERT INTO materialtype " +
                                      "(guid, name, description, note, materialId, datecreated) " +
                                      "VALUES (@guid, @name, @description, @note, @materialId, @datecreated)";

                await connection.ExecuteAsync(insertQuery, new
                {
                    materialType.Guid,
                    materialType.Name,
                    materialType.Description,
                    materialType.Note,
                    materialType.MaterialId,
                    materialType.DateCreated
                });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while inserting record in MaterialType table.", methodParams);
                throw;
            }
        }

        public async Task<List<MaterialType>> SelectAllMaterialTypesAsync()
        {
            var materialType = new List<MaterialType>();
            
            try
            {
                using var connection = new MySqlConnection(_connectionString);
                
                string selectQuery = "SELECT * FROM materialtype";

                materialType = (await connection.QueryAsync<MaterialType>(selectQuery)).ToList();

                return materialType;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from MaterialType table.", null);
                throw;
            }
        }

        public async Task<MaterialType> SelectMaterialTypeByIdAsync(int materialTypeId)
        {
            var methodParams = new
            {
                materialTypeId
            };

            var materialType = new MaterialType();
            
            try
            {
                using var connection = new MySqlConnection(_connectionString);
                
                string selectQuery = "SELECT * FROM materialtype WHERE Id = @materialTypeId ";

                materialType = (await connection.QueryAsync<MaterialType>(selectQuery, new { materialTypeId })).FirstOrDefault();

                return materialType;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ById from MaterialType table.", methodParams);
                throw;
            }
        }

        public async Task<List<MaterialType>> SelectMaterialTypeByMaterialIdAsync(int materialId)
        {
            var methodParams = new
            {
                materialId
            };

            var materialType = new List<MaterialType>();
            
            try
            {
                using var connection = new MySqlConnection(_connectionString);
                
                string selectQuery = "SELECT * FROM materialtype WHERE MaterialId = @materialId ";

                materialType = (await connection.QueryAsync<MaterialType>(selectQuery, new { materialId })).ToList();

                return materialType;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ByMaterialId from MaterialType table.", methodParams);
                throw;
            }
        }

        public async Task<bool> UpdateMaterialTypeAsync(int materialTypeId, MaterialType materialType)
        {
            var methodParams = new
            {
                materialTypeId,
                materialType
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);
                
                string updateQuery = @"UPDATE materialtype " +
                                       "WHERE Id = @materialTypeId ";

                await connection.ExecuteAsync(updateQuery, new
                {
                    materialTypeId,
                    materialType
                });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating record ById in MaterialType table.", methodParams);
                throw;
            }
        }
    }
}
