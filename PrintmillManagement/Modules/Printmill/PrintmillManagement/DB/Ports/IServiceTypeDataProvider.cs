﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports
{
    public interface IServiceTypeDataProvider
    {
        /// <summary>
        /// Method for Database Select Operation toward ServiceType table
        /// </summary>
        /// <returns><see cref="ServiceType"/>Retrieves all ServiceTypes from DB</returns>
        Task<List<ServiceType>> SelectAllServiceTypesAsync();

        /// <summary>
        /// Method for Database Insert Operation toward ServiceType table.
        /// </summary>
        /// <param name="serviceType"><see cref="ServiceType"/>Contains model data for Insert operation</param>
        /// <returns>Boolean flag => true if record is inserted, false if insert fails</returns>
        Task<bool> InsertServiceTypeAsync(ServiceType serviceType);
    }
}
