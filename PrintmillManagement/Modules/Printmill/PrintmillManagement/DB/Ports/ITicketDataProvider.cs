﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Shared.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports
{
    public interface ITicketDataProvider
    {
        /// <summary>
        /// Method for Database Select By Identifier Operation toward Ticket table
        /// </summary>
        /// <param name="ticketGuid">/param>
        /// <returns><see cref="TicketDataView"/>Retrieves Ticket from DB based on Guid</returns>
        Task<TicketDataView> SelectTicketByGuidAsync(string ticketGuid);

        /// <summary>
        /// Method for Database Select By Identifier Operation toward Ticket table
        /// </summary>
        /// <param name="ticketId">/param>
        /// <returns><see cref="TicketDataView"/>Retrieves Ticket from DB based on Id</returns>
        Task<TicketDataView> SelectTicketByIdAsync(int ticketId);

        /// <summary>
        /// Method for Database Select By Identifier List Operation toward Ticket table
        /// </summary>
        /// <param name="ticketIds">/param>
        /// <returns><see cref="TicketDataView"/>Retrieves Ticket from DB based on Ids list</returns>
        Task<List<TicketDataView>> SelectTicketsByIdListAsync(List<int> ticketIds);

        /// <summary>
        /// Method for Database Select Operation toward Ticket table
        /// </summary>
        /// <param name="request"><see cref="TicketFilterRequest"/>/param>
        /// <returns><see cref="TicketDataView"/>Retrieves all Tickets from DB by filter</returns>
        Task<List<TicketDataView>> SelectAllTicketsByFilterAsync(TicketFilterRequest request);

        /// <summary>
        /// Method for Database Select Operation toward Ticket table
        /// </summary>
        /// <param name="paging"><see cref="PagingRequest"/>/param>
        /// <param name="request"><see cref="TicketFilterRequest"/>/param>
        /// <returns><see cref="TicketDataView"/>Retrieves all Tickets from DB by filter</returns>
        Task<List<TicketDataView>> SelectAllTicketsByFilterPagingAsync(PagingRequest paging, TicketFilterRequest request);

        /// <summary>
        /// Method for Database Insert Operation toward Ticket table.
        /// </summary>
        /// <param name="ticket"><see cref="Ticket"/>Contains model data for Insert operation</param>
        /// <returns>Boolean flag => true if record is inserted, false if insert fails</returns>
        Task<int> InsertTicketAsync(Ticket ticket);

        /// <summary>
        /// Method for Database Update Operation toward Ticket table
        /// </summary>
        /// <param name="ticket"><see cref="Ticket"/>Contains model data for Update operation</param>
        /// <returns>Boolean flag => true if record is updated, false if update fails</returns>
        Task<bool> UpdateTicketAsync(Ticket ticket);

        /// <summary>
        /// Method for Database Delete Operation toward Ticket table
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns>Boolean flag => true if record is deleted, false if delete fails</returns>
        Task<bool> DeleteTicketAsync(int ticketId);

        /// <summary>
        /// Method for Database Update Operation toward Ticket table
        /// </summary>
        /// <param name="tickets">Contains list of ticket which will be updated</param>
        /// <param name="status">Contains status type of ticket which will be updated</param>
        /// <returns>Boolean flag => true if record is updated, false if update fails</returns>
        Task<bool> UpdateTicketStatusByIdListAsync(List<int> tickets, int status);

        /// <summary>
        /// Method for Database Update Operation toward Ticket table
        /// </summary>
        /// <param name="tickets">Contains identifier number of ticket which will be updated</param>
        /// <param name="status">Contains status type of ticket which will be updated</param>
        /// <param name="userId">Contains identifier number of User who is processing ticket</param>
        /// <returns>Boolean flag => true if record is updated, false if update fails</returns>
        Task<bool> AcceptTicketByUserAsync(int ticketId, int status, int userId);

        /// <summary>
        /// Method for Database Select Operation toward Ticket table
        /// </summary>
        /// <param name="userId">Contains User Identifier</param>
        /// <param name="statusId">Contains status type of ticket</param>
        /// <returns>Returns number of tickets by provided parameters</returns>
        Task<int> SelectTicketStatisticsForProcessedByUserIdAsync(int? userId, int statusId);

        /// <summary>
        /// Method for Database Select Operation toward Ticket table
        /// </summary>
        /// <param name="userId">Contains User Identifier</param>
        /// <param name="statusId">Contains status type of ticket</param>
        /// <returns>Returns number of tickets by provided parameters</returns>
        Task<int> SelectTicketStatisticsForCreatedByUserIdAsync(int userId, int statusId);
    }
}
