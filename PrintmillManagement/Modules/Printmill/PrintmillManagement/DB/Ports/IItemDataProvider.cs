﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports
{
    public interface IItemDataProvider
    {
        /// <summary>
        /// Method for Database Select By Identifier Operation toward Item table.
        /// </summary>
        /// <param name="itemId">/param>
        /// <returns><see cref="Item"/>Retrieves one record from DB table based on provided Identifier.</returns>
        Task<Item> SelectItemByIdAsync(int itemId);

        /// <summary>
        /// Method for Database Select Operation toward Item table.
        /// </summary>
        /// <returns><see cref="Item"/>Retrieves all data from DB table.</returns>
        Task<List<Item>> SelectAllItemsAsync();

        /// <summary>
        /// Method for Database Select Operation toward Item table.
        /// </summary>
        /// <param name="serviceTypeId"></param>
        /// <param name="materialTypeId"></param>
        /// <returns><see cref="Item"/>Retrieves all data from DB table.</returns>
        Task<List<Item>> SelecItemsByServiceTypeAndMaterialTypeAsync(int serviceTypeId, int materialTypeId);

        /// <summary>
        /// Method for Database Insert Operation toward Item table.
        /// </summary>
        /// <param name="item"><see cref="Item"/>Contains model data for Insert operation.</param>
        /// <returns>Boolean flag => true if record is inserted, false if insert fails.</returns>
        Task<bool> InsertItemAsync(Item item);

        /// <summary>
        /// Method for Database Update Operation toward Item table.
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="item"><see cref="Item"/>Contains model data for Update operation.</param>
        /// <returns>Boolean flag => true if record is updated, false if update fails.</returns>
        Task<bool> UpdateItemAsync(int itemId, Item item);

        /// <summary>
        /// Method for Database Delete Operation toward Item table.
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns>Boolean flag => true if record is deleted, false if delete fails.</returns>
        Task<bool> DeleteItemAsync(int itemId);
    }
}
