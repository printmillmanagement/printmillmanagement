﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain.Models;
using PrintmillManagement.Modules.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports
{
    public interface IDocumentDataProvider
    {
        /// <summary>
        /// Method for Database Select By Identifier Operation toward Document table
        /// </summary>
        /// <param name="id">/param>
        /// <returns><see cref="Document"/>Retrieves one record from DB table based on provided Identifier</returns>
        Task<Document> SelectDocumentByIdAsync(int id);

        /// <summary>
        /// Method for Database Select By Identifier Operation toward Document table
        /// </summary>
        /// <param name="id">/param>
        /// <returns><see cref="Document"/>Retrieves one record from DB table based on provided Identifier</returns>
        Task<DocumentDataView> SelectDocumentByTicketIdAsync(int id);

        /// <summary>
        /// Method for Database Select Operation toward Document table
        /// </summary>
        /// <returns><see cref="Document"/>Retrieves all data from DB table based on guid list</returns>
        Task<List<Document>> SelectDocumentsForImportSolvedAsync(List<string> guids, SessionUser user);

        /// <summary>
        /// Method for Database Select Operation toward Document table
        /// </summary>
        /// <returns><see cref="Document"/>Retrieves all data from DB table</returns>
        Task<List<Document>> SelectAllDocumentsAsync();

        /// <summary>
        /// Method for Database Insert Operation toward Document table.
        /// </summary>
        /// <param name="document"><see cref="Document"/>Contains model data for Insert operation</param>
        /// <returns>Boolean flag => true if record is inserted, false if insert fails</returns>
        Task<bool> InsertDocumentAsync(Document document);

        /// <summary>
        /// Method for Database Delete Operation toward Document table
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Boolean flag => true if record is deleted, false if delete fails</returns>
        Task<bool> DeleteDocumentAsync(int id);
    }
}
