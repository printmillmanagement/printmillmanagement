﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports
{
    public interface IMaterialDataProvider
    {
        /// <summary>
        /// Method for Database Select By Identifier Operation toward Material table
        /// </summary>
        /// <param name="materialId">/param>
        /// <returns><see cref="Material"/>Retrieves Material from DB based on Identifier</returns>
        Task<Material> SelectMaterialByIdAsync(int materialId);

        /// <summary>
        /// Method for Database Select Operation toward Material table
        /// </summary>
        /// <returns><see cref="Material"/>Retrieves all Materials from DB</returns>
        Task<List<Material>> SelectAllMaterialsAsync();

        /// <summary>
        /// Method for Database Select Operation toward Material table
        /// </summary>
        /// <param name="serviceTypeId">Service type identifier</param>
        /// <returns><see cref="Material"/>Retrieves all Materials from DB based on service Type Id</returns>
        Task<List<Material>> SelectMaterialsByServiceTypeIdAsync(int serviceTypeId);

        /// <summary>
        /// Method for Database Insert Operation toward Material table.
        /// </summary>
        /// <param name="material"><see cref="Material"/>Contains model data for Insert operation</param>
        /// <returns>Boolean flag => true if record is inserted, false if insert fails</returns>
        Task<bool> InsertMaterialAsync(Material material);

        /// <summary>
        /// Method for Database Update Operation toward Material table
        /// </summary>
        /// <param name="materialId"></param>
        /// <param name="material"><see cref="Material"/>Contains model data for Update operation</param>
        /// <returns>Boolean flag => true if record is updated, false if update fails</returns>
        Task<bool> UpdateMaterialAsync(int materialId, Material material);

        /// <summary>
        /// Method for Database Delete Operation toward Material table
        /// </summary>
        /// <param name="materialId"></param>
        /// <returns>Boolean flag => true if record is deleted, false if delete fails</returns>
        Task<bool> DeleteMaterialAsync(int materialId);
    }
}
