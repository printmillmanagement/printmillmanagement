﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports
{
    public interface IMaterialTypeDataProvider
    {
        /// <summary>
        /// Method for Database Select By Identifier Operation toward MaterialType table
        /// </summary>
        /// <param name="materialTypeId">/param>
        /// <returns><see cref="MaterialType"/>Retrieves Material Type from DB based on Identifier</returns>
        Task<MaterialType> SelectMaterialTypeByIdAsync(int materialTypeId);

        /// <summary>
        /// Method for Database Select By Identifier Operation toward MaterialType table
        /// </summary>
        /// <param name="materialId">/param>
        /// <returns><see cref="MaterialType"/>Retrieves Material Type from DB based on Material Identifier</returns>
        Task<List<MaterialType>> SelectMaterialTypeByMaterialIdAsync(int materialId);

        /// <summary>
        /// Method for Database Select Operation toward MaterialType table
        /// </summary>
        /// <returns><see cref="MaterialType"/>Retrieves all Material Types from DB</returns>
        Task<List<MaterialType>> SelectAllMaterialTypesAsync();

        /// <summary>
        /// Method for Database Insert Operation toward MaterialType table.
        /// </summary>
        /// <param name="materialType"><see cref="MaterialType"/>Contains model data for Insert operation</param>
        /// <returns>Boolean flag => true if record is inserted, false if insert fails</returns>
        Task<bool> InsertMaterialTypeAsync(MaterialType materialType);

        /// <summary>
        /// Method for Database Update Operation toward MaterialType table
        /// </summary>
        /// <param name="materialTypeId"></param>
        /// <param name="materialType"><see cref="MaterialType"/>Contains model data for Update operation</param>
        /// <returns>Boolean flag => true if record is updated, false if update fails</returns>
        Task<bool> UpdateMaterialTypeAsync(int materialTypeId, MaterialType materialType);

        /// <summary>
        /// Method for Database Delete Operation toward MaterialType table
        /// </summary>
        /// <param name="materialTypeId"></param>
        /// <returns>Boolean flag => true if record is deleted, false if delete fails</returns>
        Task<bool> DeleteMaterialTypeAsync(int materialTypeId);
    }
}
