﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain.Models
{
    public class MaterialTypeCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public int MaterialId { get; set; }
    }
}
