﻿namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain.Models
{
    public class MaterialTypeDataView
    {
        public MaterialTypeDataView()
        {

        }

        public MaterialTypeDataView(MaterialType materialType)
        {
            Id = materialType.Id;
            Guid = materialType.Guid;
            Name = materialType.Name;
            Description = materialType.Description;
            Note = materialType.Note;
            DateCreated = materialType.DateCreated;
            MaterialId = materialType.MaterialId;
        }

        public int Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string DateCreated { get; set; }
        public int MaterialId { get; set; }
    }
}
