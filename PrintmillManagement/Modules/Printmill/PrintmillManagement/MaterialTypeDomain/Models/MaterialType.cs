﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain.Models;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain.Models
{
    public class MaterialType
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public string DateCreated { get; set; }

        public int MaterialId { get; set; }
        public Material Material { get; set; }
    }
}
