﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain.Models;
using PrintmillManagement.Modules.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain
{
    public class MaterialTypeDomainService
    {
        private readonly IMaterialTypeDataProvider _materialTypeDataProvider;

        public MaterialTypeDomainService(
            IMaterialTypeDataProvider materialTypeDataProvider)
        {
            _materialTypeDataProvider = materialTypeDataProvider;
        }

        public async Task<List<MaterialType>> SelectAllMaterialTypesAsync()
        {
            return await _materialTypeDataProvider.SelectAllMaterialTypesAsync();
        }

        public async Task<MaterialType> SelectMaterialTypesByIdAsync(int materialTypeId)
        {
            return await _materialTypeDataProvider.SelectMaterialTypeByIdAsync(materialTypeId);
        }

        public async Task<IEnumerable<SelectableViewModel>> SelectMaterialTypesByMaterialIdAsync(int materialId)
        {
            var resultToReturn = await _materialTypeDataProvider.SelectMaterialTypeByMaterialIdAsync(materialId);

            return resultToReturn.Select(x => new SelectableViewModel { Key = x.Id, Value = x.Name });
        }

        public async Task<Result> InsertMaterialTypeAsync(SessionUser sessionUser, MaterialTypeCreateRequest materialTypeCreateRequest)
        {
            var materialType = new MaterialType
            {
                Guid = Guid.NewGuid().ToString().ToUpper(),
                Name = materialTypeCreateRequest.Name,
                Description = materialTypeCreateRequest.Description,
                Note = materialTypeCreateRequest.Note,
                MaterialId = materialTypeCreateRequest.MaterialId,
                DateCreated = DateTime.Now.ToString(SettingsConstants.DateTimeFormat)
            };

            var result = await _materialTypeDataProvider.InsertMaterialTypeAsync(materialType);

            if (!result)
            {
                return Result
                    .Fail("Failed To Create Material Type");
            }
            else
            {
                return Result
                    .Ok()
                    .WithSuccess("Material Type Created.");
            }
        }

        public async Task<Result> UpdateMaterialTypeAsync(int materialTypeId)
        {
            var materialType = new MaterialType();

            var result = await _materialTypeDataProvider.UpdateMaterialTypeAsync(materialTypeId, materialType);

            if (!result)
            {
                return Result
                    .Fail("Failed To Update Material Type");
            }
            else
            {
                return Result
                    .Ok()
                    .WithSuccess("Material Type Updated.");
            }
        }

        public async Task<Result> DeleteMaterialTypeAsync(int materialTypeId)
        {
            var result = await _materialTypeDataProvider.DeleteMaterialTypeAsync(materialTypeId);

            if (!result)
            {
                return Result
                    .Fail("Failed To Delete Material Type");
            }
            else
            {
                return Result
                    .Ok()
                    .WithSuccess("Material Type Deleted.");
            }
        }
    }
}
