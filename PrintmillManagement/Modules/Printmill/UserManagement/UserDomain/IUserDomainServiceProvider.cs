﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Modules.Shared.Paging;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.UserManagement.UserDomain
{
    public interface IUserDomainServiceProvider
    {
        /// <summary>
        /// Method for retrieving all users by provided filters
        /// </summary>
        /// <param name="request"><see cref="UserFilterRequest"/>/param>
        /// <returns><see cref="UserDataView"/>Contains all data for users as list</returns>
        Task<PagedResponse<UserDataView>> SelectAllUsersByFilterAsync(int page, int pageSize, UserFilterRequest request);

        /// <summary>
        /// Method for retrieving user details by Identifier
        /// </summary>
        /// <param name="userId"><see cref="int"/>/param>
        /// <returns><see cref="TicketDetailedDataView"/>Contains detailed user data</returns>
        Task<User> SelectUserByIdAsync(int userId);

        /// <summary>
        /// Method for retrieving all user details by user GUID
        /// </summary>
        /// <param name="userGuid"><see cref="string"/>/param>
        /// <returns><see cref="TicketDetailedDataView"/>Contains detailed user data for one user</returns>
        Task<UserDetailedDataView> SelectUserByGuidAsync(string userGuid);


        /// <summary>
        /// Method for creating new user
        /// </summary>
        /// <param name="user"><see cref="SessionUser"/>/param>
        /// <param name="request"><see cref="UserCreateRequest"/>/param>
        /// <returns><see cref="Result"/>Contains result if user is created or not</returns>
        Task<Result> InsertUserAsync(SessionUser user, UserCreateRequest request);
    }
}
