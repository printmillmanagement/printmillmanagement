﻿namespace PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models
{
    public class UserFilterRequest
    {
        public string Guid { get; set; }
        public string Uid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }

        public bool HaveFilter => !string.IsNullOrEmpty(Guid)
            || !string.IsNullOrWhiteSpace(Uid)
            || !string.IsNullOrWhiteSpace(FirstName)
            || !string.IsNullOrWhiteSpace(LastName)
            || !string.IsNullOrWhiteSpace(Email)
            || !string.IsNullOrWhiteSpace(Role);
    }
}
