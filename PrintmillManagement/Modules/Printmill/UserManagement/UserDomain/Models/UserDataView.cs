﻿namespace PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models
{
    public class UserDataView
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string Uid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Role { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public bool IsActive { get; set; }
        public string DateCreated { get; set; }
    }
}
