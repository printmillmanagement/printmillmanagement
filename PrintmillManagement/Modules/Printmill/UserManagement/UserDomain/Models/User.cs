﻿using PrintmillManagement.Modules.Shared;
using System;

namespace PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models
{
    public class User
    {
        public User()
        {

        }

        public User(UserCreateRequest request)
        {
            Guid = System.Guid.NewGuid().ToString().ToUpper();
            Role = request.Role;
            FirstName = request.FirstName;
            LastName = request.LastName;
            Email = request.Email;
            PhoneNumber = request.PhoneNumber;
            Password = BCrypt.Net.BCrypt.HashPassword(request.Password);
            IsActive = true;
            IsEmailConfirmed = true;
            DateCreated = DateTime.Now.ToString(SettingsConstants.DateTimeFormat);
        }

        public int Id { get; set; }
        public string Guid { get; set; }
        public string Uid { get; set; }
        public string Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public bool IsActive { get; set; }
        public string DateCreated { get; set; }
    }
}
