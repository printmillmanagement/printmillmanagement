﻿namespace PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models
{
    public class UserDetailedDataView
    {
        public UserDetailedDataView()
        {
            User = new UserDataView();
        }

        public UserDataView User { get; set; }
    }
}
