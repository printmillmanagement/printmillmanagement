﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.UserManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models;
using PrintmillManagement.Modules.PrintmillManagement.UserDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Modules.Shared.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.UserManagement.UserDomain
{
    public class UserDomainService : IUserDomainServiceProvider
    {
        private readonly IUserDataProvider _userDataProvider;

        public UserDomainService(
            IUserDataProvider userDataProvider)
        {
            _userDataProvider = userDataProvider;
        }

        public async Task<PagedResponse<UserDataView>> SelectAllUsersByFilterAsync(int page, int pageSize, UserFilterRequest request)
        {
            var paging = new PagingRequest(page, pageSize);

            var usersDataResponseTask = _userDataProvider.SelectAllUsersByFilterPagingAsync(paging, request);
            var usersCountDataResponseTask = _userDataProvider.SelectAllUsersByFilterAsync(request);

            IEnumerable<Task> tasks = new List<Task>() {
                usersDataResponseTask,
                usersCountDataResponseTask
            };

            await Task.WhenAll(tasks);

            var userssDataResponseTaskResult = usersDataResponseTask.Result;
            var usersCountDataResponseTaskResult = usersCountDataResponseTask.Result.Count;

            return new PagedResponse<UserDataView>(userssDataResponseTaskResult, paging.Page, paging.PageSize, usersCountDataResponseTaskResult);
        }

        public async Task<UserDetailedDataView> SelectUserByGuidAsync(string userGuid)
        {
            var userDetailed = new UserDetailedDataView();

            var userResponse = await _userDataProvider.SelectUserByGuidAsync(userGuid);
            if (userResponse is null) return userDetailed;

            userDetailed.User = userResponse;

            return userDetailed;
        }

        public async Task<User> SelectUserByIdAsync(int userId)
        {
            return await _userDataProvider.SelectUserByIdAsync(userId);
        }

        public async Task<Result> InsertUserAsync(SessionUser user, UserCreateRequest request)
        {
            var newUser = new User(request);
            await GenerateUidForUser(newUser);
            var result = await _userDataProvider.InsertUserAsync(newUser);

            if (result)
            {
                return Result.Ok().WithSuccess("User Created");
            }
            else
            {
                return Result.Fail("Failed To Create new User");
            }
        }

        private async Task GenerateUidForUser(User newUser)
        {
            if (newUser.Role == nameof(Roles.CUSTOMER))
            {
                UserDataView selectUserByUid;

                do
                {
                    newUser.Uid = StringRandomGenerator.GenerateRandomString(2);
                    selectUserByUid = await _userDataProvider.SelectUserByUidAsync(newUser.Uid);

                } while (selectUserByUid != null);
            }
        }
    }
}
