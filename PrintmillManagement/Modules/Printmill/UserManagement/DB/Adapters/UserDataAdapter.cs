﻿using Dapper;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PrintmillManagement.Modules.Printmill.UserManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models;
using PrintmillManagement.Modules.Shared.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.UserManagement.DB.Adapters
{
    public class UserDataAdapter : IUserDataProvider
    {
        private readonly string _connectionString;
        private readonly ILogger<UserDataAdapter> _logger;

        public UserDataAdapter(
            string connectionString,
            ILogger<UserDataAdapter> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public async Task<List<UserDataView>> SelectAllUsersByFilterPagingAsync(PagingRequest paging, UserFilterRequest request)
        {
            var methodParams = new
            {
                request
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);
                
                string selectQuery = @"SELECT Id,
                                              Uid,
                                              Guid,
                                              FirstName,
                                              LastName,
                                              Email,
                                              PhoneNumber,
                                              Role,
                                              IsEmailConfirmed,
                                              IsActive,
                                              DateCreated
                                         FROM User";

                if (request.HaveFilter)
                {
                    string filterSQL = "";

                    if (!String.IsNullOrWhiteSpace(request.Guid))
                    {
                        filterSQL += @" Guid = @Guid";
                    }
                    if (!String.IsNullOrWhiteSpace(request.Uid))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Uid = @Uid";
                    }
                    if (!String.IsNullOrWhiteSpace(request.FirstName))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Firstname = @Firstname";
                    }
                    if (!String.IsNullOrWhiteSpace(request.LastName))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Lastname = @Lastname";
                    }
                    if (!String.IsNullOrWhiteSpace(request.Email))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Email = @Email";
                    }
                    if (!String.IsNullOrWhiteSpace(request.Role))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Role = @Role";
                    }

                    selectQuery += $" WHERE {filterSQL}";
                }

                selectQuery += $" ORDER BY Id DESC LIMIT @PageSize OFFSET @Offset";

                return (await connection.QueryAsync<UserDataView>(selectQuery,
                    new
                    {
                        request.Guid,
                        request.Uid,
                        request.FirstName,
                        request.LastName,
                        request.Email,
                        request.Role,
                        paging.PageSize,
                        paging.Offset
                    })).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from User table by provided Filter.", methodParams);
                throw;
            }
        }

        public async Task<List<UserDataView>> SelectAllUsersByFilterAsync(UserFilterRequest request)
        {
            var methodParams = new
            {
                request
            };

            var users = new List<UserDataView>();

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = @"SELECT Id,
                                              Uid,
                                              Guid,
                                              FirstName,
                                              LastName,
                                              Email,
                                              PhoneNumber,
                                              Role,
                                              IsEmailConfirmed,
                                              IsActive,
                                              DateCreated
                                         FROM User";

                if (request.HaveFilter)
                {
                    string filterSQL = "";

                    if (!String.IsNullOrWhiteSpace(request.Guid))
                    {
                        filterSQL += @" Guid = @Guid";
                    }
                    if (!String.IsNullOrWhiteSpace(request.Uid))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Uid = @Uid";
                    }
                    if (!String.IsNullOrWhiteSpace(request.FirstName))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Firstname = @Firstname";
                    }
                    if (!String.IsNullOrWhiteSpace(request.LastName))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Lastname = @Lastname";
                    }
                    if (!String.IsNullOrWhiteSpace(request.Email))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Email = @Email";
                    }
                    if (!String.IsNullOrWhiteSpace(request.Role))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" Role = @Role";
                    }

                    selectQuery += $" WHERE {filterSQL}";
                }

                selectQuery += $" ORDER BY Id DESC";

                users = (await connection.QueryAsync<UserDataView>(selectQuery, request)).ToList();
                return users;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from User table by provided Filter.", methodParams);
                throw;
            }
        }

        public async Task<UserDataView> SelectUserByGuidAsync(string userGuid)
        {
            var methodParams = new
            {
                userGuid
            };

            var user = new UserDataView();

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = @"SELECT Id,
                                              Uid,
                                              Guid,
                                              FirstName,
                                              LastName,
                                              Email,
                                              PhoneNumber,
                                              Role,
                                              IsEmailConfirmed,
                                              IsActive,
                                              DateCreated
                                         FROM User
                                        WHERE Guid = @userGuid";

                user = (await connection.QueryAsync<UserDataView>(selectQuery, new { userGuid })).FirstOrDefault();

                return user;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ByGuid from User table.", methodParams);
                throw;
            }
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            var methodParams = new
            {
                email
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = "SELECT * FROM User WHERE Email = @Email";

                var resultToReturn = (await connection.QueryAsync<User>(selectQuery, new { email })).FirstOrDefault();

                return resultToReturn;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record by Email from User table.", methodParams);
                throw;
            }
        }

        public async Task<bool> DeleteUserAsync(int userId)
        {
            var methodParams = new
            {
                userId
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string deleteQuery = @"DELETE FROM User WHERE Id = @UserId";

                await connection.ExecuteAsync(deleteQuery, new { userId });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while deleting record in User table.", methodParams);
                throw;
            }
        }

        public async Task<bool> InsertUserAsync(User user)
        {
            var methodParams = new
            {
                user
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);
                
                string insertQuery = @"INSERT INTO User " +
                                      "(Guid, Uid, Role, FirstName, LastName, Email, Password, PhoneNumber, IsEmailConfirmed, IsActive, DateCreated) " +
                                      "VALUES (@Guid, @Uid, @Role, @FirstName, @LastName, @Email, @Password, @PhoneNumber, @IsEmailConfirmed, @IsActive, @DateCreated)";

                await connection.ExecuteAsync(insertQuery, new
                {
                    user.Guid,
                    user.Uid,
                    user.Role,
                    user.FirstName,
                    user.LastName,
                    user.Email,
                    user.Password,
                    user.PhoneNumber,
                    user.IsEmailConfirmed,
                    user.IsActive,
                    user.DateCreated
                });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while inserting record in User table.", methodParams);
                throw;
            }
        }

        public async Task<User> SelectUserByIdAsync(int userId)
        {
            var methodParams = new
            {
                userId
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = "SELECT * FROM User WHERE Id = @userId ";

                var resultToReturn = (await connection.QueryAsync<User>(selectQuery, new { userId })).FirstOrDefault();

                return resultToReturn;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ById from User table.", methodParams);
                throw;
            }
        }

        public async Task<UserDataView> SelectUserByUidAsync(string userUid)
        {
            var methodParams = new
            {
                userUid
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = @"SELECT Id,
                                              Uid,
                                              Guid,
                                              FirstName,
                                              LastName,
                                              Email,
                                              PhoneNumber,
                                              Role,
                                              IsEmailConfirmed,
                                              IsActive,
                                              DateCreated
                                         FROM User
                                        WHERE Uid = @userUid ";

                var resultToReturn = (await connection.QueryAsync<UserDataView>(selectQuery, new { userUid })).FirstOrDefault();

                return resultToReturn;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ByUid from User table.", methodParams);
                throw;
            }
        }

        public async Task<UserDataView> SelectUserViewByIdAsync(int userId)
        {
            var methodParams = new
            {
                userId
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = @"SELECT Id,
                                              Uid,
                                              Guid,
                                              FirstName,
                                              LastName,
                                              Email,
                                              PhoneNumber,
                                              Role,
                                              IsEmailConfirmed,
                                              IsActive,
                                              DateCreated
                                         FROM User
                                        WHERE Id = @userId ";

                var resultToReturn = (await connection.QueryAsync<UserDataView>(selectQuery, new { userId })).FirstOrDefault();

                return resultToReturn;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ById from User table.", methodParams);
                throw;
            }
        }

        public async Task<bool> UpdateUserAsync(int userId, User user)
        {
            var methodParams = new
            {
                userId,
                user
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string updateQuery = @"UPDATE User WHERE Id = @ticketId ";

                await connection.ExecuteAsync(updateQuery, new
                {
                    userId,
                    user
                });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating record ById in User table.", methodParams);
                throw;
            }
        }
    }
}
