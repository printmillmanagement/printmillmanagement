﻿using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models;
using PrintmillManagement.Modules.Shared.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.UserManagement.DB.Ports
{
    public interface IUserDataProvider
    {
        /// <summary>
        /// Method for Database Select By Filter Operation toward User table with sensitive data
        /// </summary>
        /// <param name="request"><see cref="UserFilterRequest"/>/param>
        /// <returns><see cref="UserDataView"/>Retrieves all Users from DB by filter</returns>
        Task<List<UserDataView>> SelectAllUsersByFilterPagingAsync(PagingRequest paging, UserFilterRequest request);

        /// <summary>
        /// Method for Database Select By Filter Operation toward User table with sensitive data
        /// </summary>
        /// <param name="request"><see cref="UserFilterRequest"/>/param>
        /// <returns><see cref="UserDataView"/>Retrieves all Users from DB by filter</returns>
        Task<List<UserDataView>> SelectAllUsersByFilterAsync(UserFilterRequest request);
        
        /// <summary>
        /// Method for Database Select By Identifier Operation toward User table with sensitive data
        /// </summary>
        /// <param name="userId">/param>
        /// <returns><see cref="User"/>Retrieves one record from DB table based on provided Identifier</returns>
        Task<User> SelectUserByIdAsync(int userId);

        /// <summary>
        /// Method for Database Select By Identifier Operation toward User table with sensitive data
        /// </summary>
        /// <param name="userUid">/param>
        /// <returns><see cref="User"/>Retrieves one record from DB table based on provided Identifier</returns>
        Task<UserDataView> SelectUserByUidAsync(string userUid);

        /// <summary>
        /// Method for Database Select By Identifier Operation toward User table without sensitive data
        /// </summary>
        /// <param name="userId">/param>
        /// <returns><see cref="User"/>Retrieves one record from DB table based on provided Identifier</returns>
        Task<UserDataView> SelectUserViewByIdAsync(int userId);

        /// <summary>
        /// Method for Database Select By Identifier Operation toward User table without sensitive data
        /// </summary>
        /// <param name="userId">/param>
        /// <returns><see cref="User"/>Retrieves one record from DB table based on provided Identifier</returns>
        Task<UserDataView> SelectUserByGuidAsync(string userGuid);

        /// <summary>
        /// Method for Database Insert Operation toward User table.
        /// </summary>
        /// <param name="user"><see cref="User"/>Contains model data for Insert operation</param>
        /// <returns>Boolean flag => true if record is inserted, false if insert fails</returns>
        Task<bool> InsertUserAsync(User user);

        /// <summary>
        /// Method for Database Update Operation toward User table
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="user"><see cref="User"/>Contains model data for Update operation</param>
        /// <returns>Boolean flag => true if record is updated, false if update fails</returns>
        Task<bool> UpdateUserAsync(int userId, User user);

        /// <summary>
        /// Method for Database Delete Operation toward User table
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Boolean flag => true if record is deleted, false if delete fails</returns>
        Task<bool> DeleteUserAsync(int userId);

        /// <summary>
        /// Method for Database Select By Email and Password Operation toward User table for authentication
        /// </summary>
        /// <param name="email">/param>
        /// <returns><see cref="User"/>Retrieves one record from DB table based on provided parameters</returns>
        Task<User> GetUserByEmailAsync(string email);
    }
}
