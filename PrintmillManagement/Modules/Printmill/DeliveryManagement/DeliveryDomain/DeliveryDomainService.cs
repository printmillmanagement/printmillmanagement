﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryTicketDomain.Models;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Printmill.UserManagement.DB.Ports;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Modules.Shared.Paging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain
{
    public class DeliveryDomainService : IDeliveryDomainServiceProvider
    {
        private readonly IDeliveryDataProvider _deliveryDataProvider;
        private readonly IDeliveryTicketDataProvider _deliveryTicketDataProvider;
        private readonly ITicketDataProvider _ticketDataProvider;
        private readonly IUserDataProvider _userDataProvider;

        public DeliveryDomainService(
            IDeliveryDataProvider deliveryDataProvider,
            IDeliveryTicketDataProvider deliveryTicketDataProvider,
            ITicketDataProvider ticketDataProvider,
            IUserDataProvider userDataProvider)
        {
            _deliveryDataProvider = deliveryDataProvider;
            _deliveryTicketDataProvider = deliveryTicketDataProvider;
            _ticketDataProvider = ticketDataProvider;
            _userDataProvider = userDataProvider;
        }

        public async Task<DeliveryInitialDataView> SelectInitialDeliveryDataAsync(SessionUser user, string userGuid)
        {
            var initData = new DeliveryInitialDataView();

            var userDataResponse = await _userDataProvider.SelectUserByGuidAsync(userGuid);

            var ticketFilterRequest = new TicketFilterRequest
            {
                ProcessedByUserId = user.UserId,
                CreatedByUserId = userDataResponse.Id,
                StatusId = (int)TicketStatus.Finished
            };

            var ticketsDataResponse = await _ticketDataProvider.SelectAllTicketsByFilterAsync(ticketFilterRequest);

            initData.CreatedForUser = userDataResponse;
            initData.Tickets = ticketsDataResponse;

            return initData;
        }

        public async Task<PagedResponse<DeliveryDataView>> SelectAllDeliveriesByFilterAsync(int page, int pageSize, DeliveryFilterRequest request)
        {
            var paging = new PagingRequest(page, pageSize);

            var deliveriesDataResponseTask = _deliveryDataProvider.SelectAllDeliveriesByFilterPagingAsync(paging, request);
            var deliveriesCountDataResponseTask = _deliveryDataProvider.SelectAllDeliveriesByFilterAsync(request);

            IEnumerable<Task> tasks = new List<Task>() {
                deliveriesDataResponseTask,
                deliveriesCountDataResponseTask
            };

            await Task.WhenAll(tasks);

            var deliveriesDataResponseTaskResult = deliveriesDataResponseTask.Result;
            var deliveriesCountDataResponseTaskResult = deliveriesCountDataResponseTask.Result.Count;

            return new PagedResponse<DeliveryDataView>(deliveriesDataResponseTaskResult, paging.Page, paging.PageSize, deliveriesCountDataResponseTaskResult);
        }

        public async Task<DeliveryDetailedDataView> SelectDeliveryByGuidAsync(string deliveryGuid)
        {
            var deliveryDetailed = new DeliveryDetailedDataView();

            var deliveryResponse = await _deliveryDataProvider.SelectDeliveryByGuidAsync(deliveryGuid);
            if (deliveryResponse is null) return deliveryDetailed;

            var createdByUserResponse = await _userDataProvider.SelectUserViewByIdAsync(deliveryResponse.CreatedByUserId);
            var createdForUserResponse = await _userDataProvider.SelectUserViewByIdAsync(deliveryResponse.CreatedForUserId);
            var deliveryTicketsResponse = await _deliveryTicketDataProvider.SelectAllDeliveryTicketsByDeliveryIdAsync(deliveryResponse.Id);
            var ticketsInDeliveryReponse = await _ticketDataProvider.SelectTicketsByIdListAsync(deliveryTicketsResponse.Select(ticketIds => ticketIds.TicketId).ToList());

            deliveryDetailed.Delivery = deliveryResponse;
            deliveryDetailed.CreatedByUser = createdByUserResponse;
            deliveryDetailed.CreatedForUser = createdForUserResponse;
            deliveryDetailed.Tickets = ticketsInDeliveryReponse;

            return deliveryDetailed;
        }

        public async Task<Result> InsertDeliveryAsync(SessionUser user, DeliveryCreateRequest request)
        {
            if (!(await ValidateUser(user))) return Result.Fail("Error occurred while validating user");

            string guid = System.Guid.NewGuid().ToString().ToUpper();

            var delivery = new Delivery(request, user.UserId, guid);
            int insertDeliveryResult = await _deliveryDataProvider.InsertDeliveryAsync(delivery);
            if (insertDeliveryResult == 0) return Result.Fail("Error occurred while creating Delivery");

            foreach (var ticket in request.Tickets)
            {
                var deliveryTicket = new DeliveryTicket
                {
                    DeliveryId = insertDeliveryResult,
                    TicketId = ticket.Id
                };

                var insertDeliveryTicketResult = await _deliveryTicketDataProvider.InsertDeliveryTicketsAsync(deliveryTicket);
                if (!insertDeliveryTicketResult) return Result.Fail("Error occurred while creating Delivery Tickets");

                var ticketsToUpdate = new List<int>() { deliveryTicket.TicketId };
                var ticketUpdateResult = await _ticketDataProvider.UpdateTicketStatusByIdListAsync(ticketsToUpdate, (int)TicketStatus.InDelivery);
                if (!ticketUpdateResult) return Result.Fail("Error occurred while updating Ticket status");
            }

            //TODO send email to customer with delivery statistics

            return Result.Ok().WithSuccess("Delivery Created");
        }

        /// <summary>
        /// Method used for validating provided user with user in system.
        /// </summary>
        /// <param name="user">Contains data for a user which is returned on authentication, of type: <see cref="SessionUser"/>/param>
        /// <returns><see cref="bool"/>Returns boolean value with a flag either if the user is valid or not.</returns>
        private async Task<bool> ValidateUser(SessionUser user)
        {
            bool userIsValid = false;

            var userOnRequest = await _userDataProvider.SelectUserByIdAsync(user.UserId);

            if (userOnRequest != null) userIsValid = user.UserId == userOnRequest.Id;

            return userIsValid;
        }
    }
}
