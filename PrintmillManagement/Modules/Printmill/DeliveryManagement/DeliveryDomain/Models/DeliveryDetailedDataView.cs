﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models
{
    public class DeliveryDetailedDataView
    {
        public DeliveryDetailedDataView()
        {
            Delivery = new DeliveryDataView();
            CreatedByUser = new UserDataView();
            CreatedForUser = new UserDataView();
            Tickets = new List<TicketDataView>();
        }

        public DeliveryDataView Delivery { get; set; }
        public UserDataView CreatedByUser { get; set; }
        public UserDataView CreatedForUser { get; set; }
        public List<TicketDataView> Tickets { get; set; }
    }
}
