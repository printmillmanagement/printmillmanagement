﻿namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models
{
    public class DeliveryFilterRequest
    {
        public string Guid { get; set; }
        public int? CreatedByUserId { get; set; }
        public string CreatedByUserGuid { get; set; }
        public string CreatedByUsername { get; set; }
        public string CreatedForUserGuid { get; set; }
        public string CreatedForUsername { get; set; }

        public bool HaveFilter => !string.IsNullOrEmpty(Guid)
            || CreatedByUserId.HasValue
            || !string.IsNullOrWhiteSpace(CreatedByUserGuid)
            || !string.IsNullOrWhiteSpace(CreatedByUsername)
            || !string.IsNullOrWhiteSpace(CreatedForUserGuid)
            || !string.IsNullOrWhiteSpace(CreatedForUsername);
    }
}
