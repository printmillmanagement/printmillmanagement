﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models
{
    public class DeliveryInitialDataView
    {
        public DeliveryInitialDataView()
        {
            CreatedForUser = new UserDataView();
            Tickets = new List<TicketDataView>();
        }

        public UserDataView CreatedForUser { get; set; }
        public List<TicketDataView> Tickets { get; set; }
    }
}
