﻿namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models
{
    public class DeliveryDataView
    {
        public int Id { get; set; }
        public string Guid { get; set; }
        public string CreatedByUserGuid { get; set; }
        public int CreatedByUserId { get; set; }
        public string CreatedByUserName { get; set; }
        public string CreatedForUserGuid { get; set; }
        public int CreatedForUserId { get; set; }
        public string CreatedForUserName { get; set; }
        public int NumberOfTickets { get; set; }
        public string DateCreated { get; set; }
    }
}
