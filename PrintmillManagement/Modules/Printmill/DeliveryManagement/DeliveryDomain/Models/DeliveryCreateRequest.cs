﻿using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using System.Collections.Generic;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models
{
    public class DeliveryCreateRequest
    {
        public DeliveryCreateRequest()
        {
            Tickets = new List<TicketDataView>();
        }

        public int CreatedForUserId { get; set; }
        public List<TicketDataView> Tickets { get; set; }
    }
}
