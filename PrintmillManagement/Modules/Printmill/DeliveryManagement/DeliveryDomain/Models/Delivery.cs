﻿using PrintmillManagement.Modules.Shared;
using System;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models
{
    public class Delivery
    {
        public Delivery()
        {

        }

        public Delivery(DeliveryCreateRequest request, int userId, string guid)
        {
            Guid = guid;
            CreatedByUserId = userId;
            CreatedForUserId = request.CreatedForUserId;
            DateCreated = DateTime.Now.ToString(SettingsConstants.DateTimeFormat);
        }

        public int Id { get; set; }
        public string Guid { get; set; }
        public int CreatedByUserId { get; set; }
        public int CreatedForUserId { get; set; }
        public string DateCreated { get; set; }
    }
}
