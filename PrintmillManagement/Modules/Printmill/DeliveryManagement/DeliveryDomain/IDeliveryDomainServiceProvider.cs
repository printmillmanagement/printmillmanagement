﻿using FluentResults;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Modules.Shared.Paging;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain
{
    public interface IDeliveryDomainServiceProvider
    {
        /// <summary>
        /// Method for retrieving initial data for creating Delivery
        /// </summary>
        /// <param name="user"><see cref="SessionUser"/>/param>
        /// <param name="userGuid"><see cref="string"/>/param>
        /// <returns><see cref="DeliveryInitialDataView"/>Contains all data for creating Delivery</returns>
        Task<DeliveryInitialDataView> SelectInitialDeliveryDataAsync(SessionUser user, string userGuid);

        /// <summary>
        /// Method for retrieving all Deliveries by provided filters
        /// </summary>
        /// <param name="page"><see cref="int"/>Page number for paging /param>
        /// <param name="pageSize"><see cref="int"/>Page size for paging /param>
        /// <param name="request"><see cref="DeliveryFilterRequest"/>Filters for filtering data /param>
        /// <returns><see cref="DeliveryDataView"/>Contains all data for Delivery as list</returns>
        Task<PagedResponse<DeliveryDataView>> SelectAllDeliveriesByFilterAsync(int page, int pageSize, DeliveryFilterRequest request);

        /// <summary>
        /// Method for retrieving Delivery details by delivery GUID
        /// </summary>
        /// <param name="deliveryGuid"><see cref="string"/>/param>
        /// <returns><see cref="DeliveryDetailedDataView"/>Contains detailed Delivery data for single Delivery</returns>
        Task<DeliveryDetailedDataView> SelectDeliveryByGuidAsync(string deliveryGuid);

        /// <summary>
        /// Method for creating new Delivery
        /// </summary>
        /// <param name="user"><see cref="SessionUser"/>/param>
        /// <param name="request"><see cref="DeliveryCreateRequest"/>/param>
        /// <returns><see cref="Result"/>Contains result if Delivery is created or not</returns>
        Task<Result> InsertDeliveryAsync(SessionUser user, DeliveryCreateRequest request);
    }
}
