﻿using Dapper;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryTicketDomain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DB.Adapters
{
    public class DeliveryTicketDataAdapter : IDeliveryTicketDataProvider
    {
        private readonly string _connectionString;
        private readonly ILogger<DeliveryTicketDataAdapter> _logger;

        public DeliveryTicketDataAdapter(
            string connectionString,
            ILogger<DeliveryTicketDataAdapter> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public async Task<bool> InsertDeliveryTicketsAsync(DeliveryTicket deliveryTicket)
        {
            var methodParams = new
            {
                deliveryTicket
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string insertQuery = @"INSERT INTO deliveryticket (DeliveryId, TicketId) " +
                                      "VALUES (@DeliveryId, @TicketId); ";

                await connection.ExecuteAsync(insertQuery, new
                {
                    deliveryTicket.DeliveryId,
                    deliveryTicket.TicketId
                });

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while inserting record in DeliveryTicket table.", methodParams);
                throw;
            }
        }

        public async Task<List<DeliveryTicket>> SelectAllDeliveryTicketsByDeliveryIdAsync(int deliveryId)
        {
            var methodParams = new
            {
                deliveryId
            };

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = "SELECT Id, DeliveryId, TicketId FROM DeliveryTicket WHERE DeliveryId = @deliveryId ";

                var resultToReturn = (await connection.QueryAsync<DeliveryTicket>(selectQuery, new { deliveryId })).ToList();

                return resultToReturn;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records By DeliveryId from DeliveryTicket table.", methodParams);
                throw;
            }
        }
    }
}
