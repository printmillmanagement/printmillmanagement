﻿using Dapper;
using Microsoft.Extensions.Logging;
using MySqlConnector;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models;
using PrintmillManagement.Modules.Shared.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DB.Adapters
{
    public class DeliveryDataAdapter : IDeliveryDataProvider
    {
        private readonly string _connectionString;
        private readonly ILogger<DeliveryDataAdapter> _logger;

        public DeliveryDataAdapter(
            string connectionString,
            ILogger<DeliveryDataAdapter> logger)
        {
            _connectionString = connectionString;
            _logger = logger;
        }

        public async Task<int> InsertDeliveryAsync(Delivery delivery)
        {
            var methodParams = new
            {
                delivery
            };

            try
            {
                using (var connection = new MySqlConnection(_connectionString))
                {
                    string insertQuery = @"INSERT INTO delivery (Guid, CreatedByUserId, CreatedForUserId, DateCreated) " +
                                          "VALUES (@Guid, @CreatedByUserId, @CreatedForUserId, @DateCreated); " +
                                          "SELECT LAST_INSERT_ID();";

                    int lastInsertId = await connection.ExecuteScalarAsync<int>(insertQuery, new
                    {
                        delivery.Guid,
                        delivery.CreatedByUserId,
                        delivery.CreatedForUserId,
                        delivery.DateCreated
                    });

                    return lastInsertId;
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while inserting record in Delivery table.", methodParams);
                throw;
            }
        }

        public async Task<List<DeliveryDataView>> SelectAllDeliveriesByFilterAsync(DeliveryFilterRequest request)
        {
            var methodParams = new
            {
                request
            };

            var deliveries = new List<DeliveryDataView>();

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = @"SELECT del.Id,
		                                      del.Guid,
		                                      createdbyuser.Guid AS createdbyuserguid,
                                              createdbyuser.Id AS createdbyuserid,
		                                      CONCAT(createdbyuser.FirstName, ' ', createdbyuser.LastName) AS createdbyusername,
		                                      createdforuser.Guid AS createdforuserguid,
                                              createdforuser.Id AS createdforuserid,
		                                      CONCAT(createdforuser.FirstName, ' ', createdforuser.LastName) AS createdforusername,
		                                      (SELECT COUNT(*) FROM deliveryticket delticket WHERE del.Id = delticket.DeliveryId) numberoftickets,
		                                      del.DateCreated
                                         FROM delivery AS del
                                         LEFT JOIN user AS createdbyuser ON createdbyuser.Id = del.CreatedByUserId
                                         LEFT JOIN user AS createdforuser ON createdforuser.Id = del.CreatedForUserId";

                if (request.HaveFilter)
                {
                    string filterSQL = "";

                    if (!String.IsNullOrWhiteSpace(request.Guid))
                    {
                        filterSQL += @" del.Guid = @Guid";
                    }
                    if (request.CreatedByUserId.HasValue)
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdbyuser.Id = @CreatedByUserId";
                    }
                    if (!String.IsNullOrWhiteSpace(request.CreatedByUserGuid))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdbyuser.Guid = @CreatedByUserGuid";
                    }
                    if (!String.IsNullOrWhiteSpace(request.CreatedByUsername))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdbyuser.FirstName LIKE @CreatedByUsername ";
                        filterSQL += @" OR createdbyuser.LastName LIKE @CreatedByUsername ";
                    }
                    if (!String.IsNullOrWhiteSpace(request.CreatedForUserGuid))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdforuser.Guid = @CreatedForUserGuid";
                    }
                    if (!String.IsNullOrWhiteSpace(request.CreatedForUsername))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdforuser.FirstName LIKE @CreatedForUsername ";
                        filterSQL += @" OR createdforuser.LastName LIKE @CreatedForUsername ";
                    }

                    selectQuery += $" WHERE {filterSQL}";
                }

                selectQuery += $" ORDER BY del.Id DESC";

                deliveries = (await connection.QueryAsync<DeliveryDataView>(selectQuery, request)).ToList();
                return deliveries;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from Delivery table by provided Filter.", methodParams);
                throw;
            }
        }

        public async Task<List<DeliveryDataView>> SelectAllDeliveriesByFilterPagingAsync(PagingRequest paging, DeliveryFilterRequest request)
        {
            var methodParams = new
            {
                request
            };

            var deliveries = new List<DeliveryDataView>();

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = @"SELECT del.Id,
		                                      del.Guid,
		                                      createdbyuser.Guid AS createdbyuserguid,
                                              createdbyuser.Id AS createdbyuserid,
		                                      CONCAT(createdbyuser.FirstName, ' ', createdbyuser.LastName) AS createdbyusername,
		                                      createdforuser.Guid AS createdforuserguid,
                                              createdforuser.Id AS createdforuserid,
		                                      CONCAT(createdforuser.FirstName, ' ', createdforuser.LastName) AS createdforusername,
		                                      (SELECT COUNT(*) FROM deliveryticket delticket WHERE del.Id = delticket.DeliveryId) numberoftickets,
		                                      del.DateCreated
                                         FROM delivery AS del
                                         LEFT JOIN user AS createdbyuser ON createdbyuser.Id = del.CreatedByUserId
                                         LEFT JOIN user AS createdforuser ON createdforuser.Id = del.CreatedForUserId";

                if (request.HaveFilter)
                {
                    string filterSQL = "";

                    if (!String.IsNullOrWhiteSpace(request.Guid))
                    {
                        filterSQL += @" del.Guid = @Guid";
                    }
                    if (request.CreatedByUserId.HasValue)
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdbyuser.Id = @CreatedByUserId";
                    }
                    if (!String.IsNullOrWhiteSpace(request.CreatedByUserGuid))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdbyuser.Guid = @CreatedByUserGuid";
                    }
                    if (!String.IsNullOrWhiteSpace(request.CreatedByUsername))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdbyuser.FirstName LIKE @CreatedByUsername ";
                        filterSQL += @" OR createdbyuser.LastName LIKE @CreatedByUsername ";
                    }
                    if (!String.IsNullOrWhiteSpace(request.CreatedForUserGuid))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdforuser.Guid = @CreatedForUserGuid";
                    }
                    if (!String.IsNullOrWhiteSpace(request.CreatedForUsername))
                    {
                        if (!String.IsNullOrWhiteSpace(filterSQL)) filterSQL += " AND";

                        filterSQL += @" createdforuser.FirstName LIKE @CreatedForUsername ";
                        filterSQL += @" OR createdforuser.LastName LIKE @CreatedForUsername ";
                    }

                    selectQuery += $" WHERE {filterSQL}";
                }

                selectQuery += $" ORDER BY del.Id DESC LIMIT @PageSize OFFSET @Offset";

                return (await connection.QueryAsync<DeliveryDataView>(selectQuery,
                    new
                    {
                        request.Guid,
                        request.CreatedByUserId,
                        request.CreatedByUserGuid,
                        request.CreatedByUsername,
                        request.CreatedForUserGuid,
                        request.CreatedForUsername,
                        paging.PageSize,
                        paging.Offset
                    })).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting records from Delivery table by provided Filter.", methodParams);
                throw;
            }
        }

        public async Task<DeliveryDataView> SelectDeliveryByGuidAsync(string deliveryGuid)
        {
            var methodParams = new
            {
                deliveryGuid
            };

            var delivery = new DeliveryDataView();

            try
            {
                using var connection = new MySqlConnection(_connectionString);

                string selectQuery = @"SELECT del.Id,
		                                      del.Guid,
		                                      createdbyuser.Guid AS createdbyuserguid,
                                              createdbyuser.Id AS createdbyuserid,
		                                      CONCAT(createdbyuser.FirstName, ' ', createdbyuser.LastName) AS createdbyusername,
		                                      createdforuser.Guid AS createdforuserguid,
                                              createdforuser.Id AS createdforuserid,
		                                      CONCAT(createdforuser.FirstName, ' ', createdforuser.LastName) AS createdforusername,
		                                      (SELECT COUNT(*) FROM deliveryticket delticket WHERE del.Id = delticket.DeliveryId) numberoftickets,
		                                      del.DateCreated
                                         FROM delivery AS del
                                         LEFT JOIN user AS createdbyuser ON createdbyuser.Id = del.CreatedByUserId
                                         LEFT JOIN user AS createdforuser ON createdforuser.Id = del.CreatedForUserId
                                        WHERE del.Guid = @deliveryGuid";

                delivery = (await connection.QueryAsync<DeliveryDataView>(selectQuery, new { deliveryGuid })).FirstOrDefault();

                return delivery;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while selecting record ByGuid from Delivery table.", methodParams);
                throw;
            }
        }
    }
}
