﻿using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models;
using PrintmillManagement.Modules.Shared.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DB.Ports
{
    public interface IDeliveryDataProvider
    {
        /// <summary>
        /// Method for Database Insert Operation toward Delivery table.
        /// </summary>
        /// <param name="ticket"><see cref="Delivery"/>Contains model data for Insert operation</param>
        /// <returns>Boolean flag => true if record is inserted, false if insert fails</returns>
        Task<int> InsertDeliveryAsync(Delivery delivery);

        /// <summary>
        /// Method for Database Select Operation toward Delivery table
        /// </summary>
        /// <param name="paging"><see cref="PagingRequest"/>/param>
        /// <param name="request"><see cref="DeliveryFilterRequest"/>/param>
        /// <returns><see cref="DeliveryDataView"/>Retrieves all Deliveries from DB by filter</returns>
        Task<List<DeliveryDataView>> SelectAllDeliveriesByFilterPagingAsync(PagingRequest paging, DeliveryFilterRequest request);

        /// <summary>
        /// Method for Database Select Operation toward Delivery table
        /// </summary>
        /// <param name="request"><see cref="DeliveryFilterRequest"/>/param>
        /// <returns><see cref="DeliveryDataView"/>Retrieves all Deliveries from DB by filter</returns>
        Task<List<DeliveryDataView>> SelectAllDeliveriesByFilterAsync(DeliveryFilterRequest request);

        /// <summary>
        /// Method for Database Select By Identifier Operation toward Delivery table without sensitive data
        /// </summary>
        /// <param name="userId">/param>
        /// <returns><see cref="User"/>Retrieves one record from DB table based on provided Identifier</returns>
        Task<DeliveryDataView> SelectDeliveryByGuidAsync(string deliveryGuid);
    }
}
