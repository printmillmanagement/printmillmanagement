﻿using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryTicketDomain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DB.Ports
{
    public interface IDeliveryTicketDataProvider
    {
        /// <summary>
        /// Method for Database Insert Operation toward DeliveryTicket table.
        /// </summary>
        /// <param name="deliveryTicket"><see cref="DeliveryTicket"/>Contains model data for Insert operation</param>
        /// <returns>Boolean flag => true if record is inserted, false if insert fails</returns>
        Task<bool> InsertDeliveryTicketsAsync(DeliveryTicket deliveryTicket);

        /// <summary>
        /// Method for Database Select Operation toward DeliveryTicket table
        /// </summary>
        /// <param name="deliveryId"><see cref="int"/>/param>
        /// <returns><see cref="DeliveryDataView"/>Retrieves all Delivered tickets from DB by Delivery Identifier</returns>
        Task<List<DeliveryTicket>> SelectAllDeliveryTicketsByDeliveryIdAsync(int deliveryId);
    }
}
