﻿namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryTicketDomain.Models
{
    public class DeliveryTicket
    {
        public DeliveryTicket()
        {

        }

        public int Id { get; set; }
        public int DeliveryId { get; set; }
        public int TicketId { get; set; }
    }
}
