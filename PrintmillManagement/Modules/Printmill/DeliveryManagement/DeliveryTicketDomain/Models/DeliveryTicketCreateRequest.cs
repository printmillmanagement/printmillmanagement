﻿namespace PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryTicketDomain.Models
{
    public class DeliveryTicketCreateRequest
    {
        public int DeliveryId { get; set; }
        public int TicketId { get; set; }
    }
}
