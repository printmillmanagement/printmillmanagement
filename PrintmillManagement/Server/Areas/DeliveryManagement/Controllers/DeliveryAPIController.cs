﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Server.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Server.Areas.DeliveryManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeliveryAPIController : Controller
    {
        private readonly IDeliveryDomainServiceProvider _deliveryService;

        public DeliveryAPIController(
            IDeliveryDomainServiceProvider deliveryService)
        {
            _deliveryService = deliveryService;
        }

        [Authorize]
        [HttpGet("SelectInitialDeliveryData/{userGuid}")]
        public async Task<DeliveryInitialDataView> SelectInitialDeliveryDataAsync(string userGuid)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);
            if (user is null) throw new Exception("Error occurred while fetching user.");

            return await _deliveryService.SelectInitialDeliveryDataAsync(user, userGuid);
        }

        [Authorize]
        [HttpPost("CreateDelivery")]
        public async Task<IActionResult> CreateDeliveryAsync([FromBody] DeliveryCreateRequest deliveryCreateRequest)
        {
            if (deliveryCreateRequest is null) return Json(new { success = false, message = "Request is not valid." });

            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);
            if (user is null) throw new Exception("Error occurred while fetching user.");

            var result = await _deliveryService.InsertDeliveryAsync(user, deliveryCreateRequest);

            return Json(new { success = result.IsSuccess, message = result.IsSuccess ? result.Successes[0].Message : result.Errors[0].Message });
        }

        [Authorize]
        [HttpPost("SelectAllDeliveriesByFilter/{page:int}/{pageSize:int}")]
        public async Task<IActionResult> SelectAllDeliveriesByFilterAsync(int page, int pageSize, DeliveryFilterRequest request)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);
            if (user is null) throw new Exception("Error occurred while fetching user.");
            
            if (user.UserRole == Enum.GetName(typeof(Roles), Roles.EMPLOYEE))
            {
                request.CreatedByUserId = user.UserId;
            }

            var response = await _deliveryService.SelectAllDeliveriesByFilterAsync(page, pageSize, request);

            return Ok(response);
        }

        [Authorize]
        [HttpGet("SelectDeliveryByGuid/{userGuid}")]
        public async Task<DeliveryDetailedDataView> SelectDeliveryByGuidAsync(string userGuid)
        {
            if (string.IsNullOrWhiteSpace(userGuid)) return new DeliveryDetailedDataView();

            return await _deliveryService.SelectDeliveryByGuidAsync(userGuid);
        }
    }
}
