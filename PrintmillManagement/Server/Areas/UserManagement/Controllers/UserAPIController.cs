﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain;
using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Server.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Server.Areas.UserManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAPIController : Controller
    {
        private readonly IUserDomainServiceProvider _userDomainService;

        public UserAPIController(
            IUserDomainServiceProvider userDomainService)
        {
            _userDomainService = userDomainService;
        }

        [Authorize]
        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUserAsync([FromBody] UserCreateRequest userCreateRequest)
        {
            if (userCreateRequest is null) return Json(new { success = false, message = "Request is not valid." });

            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);
            if (user is null) throw new Exception("Error occurred while fetching user.");

            var result = await _userDomainService.InsertUserAsync(user, userCreateRequest);

            return Json(new { success = result.IsSuccess, message = result.IsSuccess ? result.Successes[0].Message : result.Errors[0].Message });
        }

        [Authorize]
        [HttpPost("GetAllUsers/{page:int}/{pageSize:int}")]
        public async Task<IActionResult> GetAllUsersAsync(int page, int pageSize, UserFilterRequest request)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);
            if (user is null) throw new Exception("Error occurred while fetching user.");

            var response = await _userDomainService.SelectAllUsersByFilterAsync(page, pageSize, request);

            return Ok(response);
        }

        [Authorize]
        [HttpGet("GetUserByGuid/{userGuid}")]
        public async Task<UserDetailedDataView> GetUserByGuidAsync(string userGuid)
        {
            if (string.IsNullOrWhiteSpace(userGuid)) return new UserDetailedDataView();

            return await _userDomainService.SelectUserByGuidAsync(userGuid);
        }
    }
}
