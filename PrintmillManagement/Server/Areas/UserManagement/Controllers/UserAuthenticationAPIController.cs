﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PrintmillManagement.Infrastructure.Configuration;
using PrintmillManagement.Modules.Services.AuthenticationService;
using PrintmillManagement.Modules.Services.AuthenticationService.Models;
using System.Threading.Tasks;

namespace PrintmillManagement.Server.Areas.UserManagement.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class UserAuthenticationAPIController : Controller
    {
        private readonly IAuthenticationDomainServiceProvider _authenticationService;
        private readonly AppSettings _appSettings;
        private readonly AuthenticationConfigurationSettings _authenticationConfigurationSettings;

        public UserAuthenticationAPIController(
            IAuthenticationDomainServiceProvider authenticationService,
            IConfiguration configuration)
        {
            _authenticationService = authenticationService;
            _appSettings = configuration.Get<AppSettings>();
            _authenticationConfigurationSettings = GetAuthenticationConfigurationSettings();
        }

        [HttpPost("Authentication")]
        public async Task<IActionResult> AuthenticationAsync([FromBody] AuthenticationRequest request)
        {
            var result = await _authenticationService.Authenticate(request, _authenticationConfigurationSettings);

            if (result.IsSuccess)
            {
                return Json( new { success = result.IsSuccess, data = result.Value });
            }
            else
            {
                return Json( new { success = result.IsSuccess, message = result.Errors[0].Message });
            }
        }

        private AuthenticationConfigurationSettings GetAuthenticationConfigurationSettings()
        {
            var settings = _appSettings.AuthenticationSettings;

            return new AuthenticationConfigurationSettings(settings.JwtSecurityKey, settings.JwtIssuer, settings.JwtExpiryInDays);
        }
    }
}
