﻿using Microsoft.AspNetCore.Mvc;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Server.Extensions;
using System.Threading.Tasks;

namespace PrintmillManagement.Server.Areas.PrintmillManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemAPIController : Controller
    {
        private readonly ItemDomainService _itemDomainService;

        public ItemAPIController(
            ItemDomainService itemDomainService)
        {
            _itemDomainService = itemDomainService;
        }

        [HttpGet("GetAllItems")]
        public async Task<IActionResult> GetAllItemsAsync()
        {
            var data = await _itemDomainService.SelecAllItemsAsync();

            return Json(new { Items = data });
        }

        [HttpGet("GetItemsByServiceTypeAndMaterialType/{serviceTypeId:int}/{materialTypeId:int}")]
        public async Task<IActionResult> GetItemsByServiceTypeAndMaterialTypeAsync(int serviceTypeId, int materialTypeId)
        {
            var data = await _itemDomainService.SelecItemsByServiceTypeAndMaterialTypeAsync(serviceTypeId, materialTypeId);

            return Json(new { Items = data });
        }

        [HttpPost("CreateItem")]
        public async Task<IActionResult> CreateItemAsync([FromBody] ItemCreateRequest itemCreateRequest)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);

            var result = await _itemDomainService.InsertItemAsync(user, itemCreateRequest);

            if (result.IsSuccess)
            {
                return Json(new { success = result.IsSuccess, message = result.Successes[0].Message });
            }
            else
            {
                return Json(new { success = result.IsSuccess, message = result.Errors[0].Message });
            }
        }
    }
}
