﻿using Microsoft.AspNetCore.Mvc;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Server.Extensions;
using System.Threading.Tasks;

namespace PrintmillManagement.Server.Areas.PrintmillManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceTypeAPIController : Controller
    {
        private readonly IServiceTypeDomainServiceProvider _serviceTypeService;

        public ServiceTypeAPIController(
            IServiceTypeDomainServiceProvider serviceTypeService)
        {
            _serviceTypeService = serviceTypeService;
        }

        [HttpGet("GetAllServices")]
        public async Task<IActionResult> GetAllServicesAsync()
        {
            var data = await _serviceTypeService.SelecAllServicesAsync();

            return Json(new { ServiceTypes = data });
        }

        [HttpPost("CreateServiceType")]
        public async Task<IActionResult> CreateServiceAsync([FromBody] ServiceTypeCreateRequest serviceTypeCreateRequest)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);

            var result = await _serviceTypeService.InsertServiceAsync(user, serviceTypeCreateRequest);

            if (result.IsSuccess)
            {
                return Json(new { success = result.IsSuccess, message = result.Successes[0].Message });
            }
            else
            {
                return Json(new { success = result.IsSuccess, message = result.Errors[0].Message });
            }
        }
    }
}
