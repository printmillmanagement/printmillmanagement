﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Server.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintmillManagement.Server.Areas.PrintmillManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketAPIController : Controller
    {
        private readonly ITicketDomainServiceProvider _ticketService;
        private readonly IDocumentDomainServiceProvider _documentService;

        public TicketAPIController(
            ITicketDomainServiceProvider ticketService,
            IDocumentDomainServiceProvider documentService)
        {
            _ticketService = ticketService;
            _documentService = documentService;
        }

        [Authorize]
        [HttpGet("GetInitialFilterData")]
        public async Task<TicketInitialFilterDataView> GetInitialFilterDataAsync()
        {
            return await _ticketService.GetInitialFilterDataAsync();
        }

        [Authorize]
        [HttpPost("GetAllTicketsByUserCreated/{page:int}/{pageSize:int}")]
        public async Task<IActionResult> GetAllTicketsByUserCreatedAsync(int page, int pageSize, TicketFilterRequest request)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);
            if (user is null) throw new Exception("Error occurred while fetching user.");

            request.CreatedByUserId = user.UserId;

            var response = await _ticketService.SelectAllTicketsByFilterPagingAsync(page, pageSize, request);

            return Ok(response);
        }

        [Authorize]
        [HttpPost("GetAllTicketsByUserProcessed/{page:int}/{pageSize:int}")]
        public async Task<IActionResult> GetAllTicketsByUserProcessedAsync(int page, int pageSize, TicketFilterRequest request)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);
            if (user is null) throw new Exception("Error occurred while fetching user.");
            
            request.ProcessedByUserId = user.UserId;

            var response = await _ticketService.SelectAllTicketsByFilterPagingAsync(page, pageSize, request);

            return Ok(response);
        }

        [Authorize]
        [HttpGet("GetTicketByGuid/{ticketGuid}")]
        public async Task<TicketDetailedDataView> GetTicketByGuidAsync(string ticketGuid)
        {
            if (string.IsNullOrWhiteSpace(ticketGuid)) return new TicketDetailedDataView();

            return await _ticketService.SelectTicketByGuidAsync(ticketGuid);
        }

        [Authorize]
        [HttpPost("CreateTicket")]
        public async Task<IActionResult> CreateTicketAsync([FromForm] TicketCreateRequest request)
        {
            if (request is null || request.File is null) return Json(new { success = false, message = "Request is not valid." });

            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);

            var result = await _ticketService.InsertTicketAsync(user, request);

            return Json(new { success = result.IsSuccess, message = result.IsSuccess ? result.Successes[0].Message : result.Errors[0].Message });
        }

        [Authorize]
        [HttpPost("EditTicket/{ticketGuid}")]
        public async Task<IActionResult> EditTicketAsync([FromForm] TicketEditRequest request, string ticketGuid)
        {
            if (request is null || string.IsNullOrWhiteSpace(ticketGuid)) return Json(new { success = false, message = "Request is not valid." });

            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);

            var result = await _ticketService.UpdateTicketAsync(user, request, ticketGuid);

            return Json(new { success = result.IsSuccess, message = result.IsSuccess ? result.Successes[0].Message : result.Errors[0].Message });
        }

        [Authorize]
        [HttpPost("AcceptTicket/{ticketId:int}")]
        public async Task<IActionResult> AcceptTicketAsync(int ticketId)
        {
            if (ticketId == 0) return Json(new { success = false, message = "Ticket number identifier is missing." });

            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);

            var result = await _ticketService.AcceptTicketAsync(ticketId, (int)TicketStatus.Accepted, user.UserId);

            return Json(new { success = result.IsSuccess, message = result.IsSuccess ? result.Successes[0].Message : result.Errors[0].Message });
        }

        [Authorize]
        [HttpPost("UpdateTicketStatus/{ticketId:int}/{statusId:int}")]
        public async Task<IActionResult> UpdateTicketStatusAsync(int ticketId, int statusId)
        {
            if (ticketId == 0 || statusId < 0) return Json(new { success = false, message = "Ticket number identifier is missing." });

            var result = await _ticketService.UpdateTicketStatusAsync(ticketId, statusId);

            return Json(new { success = result.IsSuccess, message = result.IsSuccess ? result.Successes[0].Message : result.Errors[0].Message });
        }

        [Authorize]
        [HttpPost("ImportSolvedTicket")]
        public async Task<List<TicketImportSolvedResponse>> ImportSolvedTicketAsync([FromForm] TicketImportSolvedRequest request)
        {
            if (request.File is null) throw new Exception("File is missing in request.");

            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);

            return await _ticketService.ImportSolvedTicketAsync(user, request);
        }

        [HttpGet("DownloadDocument/{id:int}")]
        public async Task<IActionResult> DownloadDocumentAsync(int id)
        {
            if (id == 0) return Json(new { success = false, message = "Document number identifier is missing." });

            var documentToDownload = await _documentService.DownloadDocumentAsync(id);

            Response.Headers.Add("Content-Disposition", documentToDownload.ContentDisposition.ToString());

            return File(documentToDownload.Document, "application/octet-stream", documentToDownload.DocumentName);
        }

        [Authorize]
        [HttpPost("DeleteTicket/{id:int}")]
        public async Task<IActionResult> DeleteTicketAsync(int id)
        {
            if (id == 0) return Json(new { success = false, message = "Document number identifier is missing." });

            var result = await _ticketService.DeleteTicketAsync(id);

            return Json(new { success = result.IsSuccess, message = result.IsSuccess ? result.Successes[0].Message : result.Errors[0].Message });
        }
    }
}
