﻿using Microsoft.AspNetCore.Mvc;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Server.Extensions;
using System.Threading.Tasks;

namespace PrintmillManagement.Server.Areas.PrintmillManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaterialAPIController : Controller
    {
        private readonly MaterialDomainService _materialDomainService;

        public MaterialAPIController(
            MaterialDomainService materialDomainService)
        {
            _materialDomainService = materialDomainService;
        }

        [HttpGet("GetAllMaterials")]
        public async Task<IActionResult> GetAllMaterialsAsync()
        {
            var data = await _materialDomainService.SelectAllMaterialsAsync();

            return Json(new { Materials = data });
        }

        [HttpGet("GetMaterialsByServiceTypeId/{serviceTypeId:int}")]
        public async Task<IActionResult> GetMaterialsByServiceTypeIdAsync(int serviceTypeId)
        {
            var data = await _materialDomainService.SelectMaterialsByServiceTypeIdAsync(serviceTypeId);

            return Json(new { Materials = data });
        }

        [HttpPost("CreateMaterial")]
        public async Task<IActionResult> CreateMaterialAsync([FromBody] MaterialCreateRequest materialCreateRequest)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);

            var result = await _materialDomainService.InsertMaterialAsync(user, materialCreateRequest);

            if (result.IsSuccess)
            {
                return Json(new { success = result.IsSuccess, message = result.Successes[0].Message });
            }
            else
            {
                return Json(new { success = result.IsSuccess, message = result.Errors[0].Message });
            }
        }
    }
}
