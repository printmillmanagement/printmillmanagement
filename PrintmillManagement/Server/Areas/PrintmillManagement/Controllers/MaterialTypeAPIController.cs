﻿using Microsoft.AspNetCore.Mvc;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Server.Extensions;
using System.Threading.Tasks;

namespace PrintmillManagement.Server.Areas.PrintmillManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaterialTypeAPIController : Controller
    {
        private readonly MaterialTypeDomainService _materialTypeDomainService;

        public MaterialTypeAPIController(
            MaterialTypeDomainService materialTypeDomainService)
        {
            _materialTypeDomainService = materialTypeDomainService;
        }

        [HttpGet("GetAllMaterialTypes")]
        public async Task<IActionResult> GetAllMaterialTypesAsync()
        {
            var data = await _materialTypeDomainService.SelectAllMaterialTypesAsync();

            return Json(new { Materialtypes = data });
        }

        [HttpGet("GetMaterialTypesByMaterialId/{materialId:int}")]
        public async Task<IActionResult> GetMaterialTypesByMaterialIdAsync(int materialId)
        {
            var data = await _materialTypeDomainService.SelectMaterialTypesByMaterialIdAsync(materialId);

            return Json(new { MaterialTypes = data });
        }

        [HttpPost("CreateMaterialType")]
        public async Task<IActionResult> CreateMaterialTypeAsync([FromBody] MaterialTypeCreateRequest materialTypeCreateRequest)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);

            var result = await _materialTypeDomainService.InsertMaterialTypeAsync(user, materialTypeCreateRequest);

            if (result.IsSuccess)
            {
                return Json(new { success = result.IsSuccess, message = result.Successes[0].Message });
            }
            else
            {
                return Json(new { success = result.IsSuccess, message = result.Errors[0].Message });
            }
        }
    }
}
