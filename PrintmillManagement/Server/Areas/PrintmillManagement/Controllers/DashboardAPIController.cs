﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PrintmillManagement.Client.Pages.Areas.User.UserDomain.Models;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DashboardDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DashboardDomain.Models;
using PrintmillManagement.Modules.Shared;
using PrintmillManagement.Server.Extensions;
using System;
using System.Threading.Tasks;

namespace PrintmillManagement.Server.Areas.PrintmillManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardAPIController : Controller
    {
        private readonly IDashboardDomainServiceProvider _dashboardDomainServiceProvider;

        public DashboardAPIController(
            IDashboardDomainServiceProvider dashboardDomainServiceProvider)
        {
            _dashboardDomainServiceProvider = dashboardDomainServiceProvider;
        }

        [Authorize]
        [HttpGet("SelectInitialDashboardData")]
        public async Task<DashboardInitialDataView> SelectInitialDashboardDataAsync(string userGuid)
        {
            var user = new SessionUser(User.Identity.GetUserId(), User.Identity.GetUserEmail(), User.Identity.GetUserRole(), null);
            if (user is null) throw new Exception("Error occurred while fetching user.");

            if (user.UserRole == Enum.GetName(typeof(Roles), Roles.CUSTOMER))
            {
                return await _dashboardDomainServiceProvider.SelectTicketStatisticsForCreatedByUserAsync(user, userGuid);
            }
            else
            {
                return await _dashboardDomainServiceProvider.SelectTicketStatisticsForProcessedByUserAsync(user, userGuid);
            }
        }
    }
}
