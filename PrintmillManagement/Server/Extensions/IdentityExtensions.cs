﻿using System.Security.Claims;
using System.Security.Principal;

namespace PrintmillManagement.Server.Extensions
{
    public static class IdentityExtensions
    {
        public static int GetUserId(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
            Claim claim = claimsIdentity?.FindFirst(ClaimTypes.NameIdentifier);

            if (claim == null)
            {
                return 0;
            }

            return int.Parse(claim.Value);
        }

        public static string GetUserEmail(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
            Claim claim = claimsIdentity?.FindFirst(ClaimTypes.Email);

            if (claim == null)
            {
                return null;
            }

            return claim.Value;
        }

        public static string GetUserRole(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
            Claim claim = claimsIdentity?.FindFirst(ClaimTypes.Role);

            if (claim == null)
            {
                return null;
            }

            return claim.Value;
        }
    }
}
