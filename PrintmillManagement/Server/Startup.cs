using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using PrintmillManagement.Infrastructure.Configuration;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DB.Adapters;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.DeliveryManagement.DeliveryDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DashboardDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Adapters;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.DocumentDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ItemDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.MaterialTypeDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.ServiceTypeDomain;
using PrintmillManagement.Modules.Printmill.PrintmillManagement.TicketDomain;
using PrintmillManagement.Modules.Printmill.UserManagement.DB.Adapters;
using PrintmillManagement.Modules.Printmill.UserManagement.DB.Ports;
using PrintmillManagement.Modules.Printmill.UserManagement.UserDomain;
using PrintmillManagement.Modules.Services.AuthenticationService;
using PrintmillManagement.Modules.Services.NotificationService.Adapters;
using PrintmillManagement.Modules.Services.NotificationService.Ports;
using PrintmillManagement.Modules.Shared;
using Serilog;
using System.Text;

namespace PrintmillManagement.Server
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }
        private AppSettings _appSettings;

        public Startup(
            IConfiguration configuration)
        {
            Configuration = configuration;
            _appSettings = configuration.Get<AppSettings>();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureCommonServices(services);
            ConfigureSettings(services);
            ConfigureDependencyInjection(services);
            ConfigureAuthentication(services);
            ConfigureHostingSettings(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });
        }

        private static void ConfigureCommonServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddRazorPages();
            services.AddHttpContextAccessor();
            services.AddMemoryCache();
            services.AddCors();
        }

        // Configure strongly typed settings objects
        private void ConfigureSettings(IServiceCollection services)
        {
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.Configure<AppSettingsBase>(Configuration.GetSection("AppSettingsBase"));
            services.Configure<ConnectionString>(Configuration.GetSection("ConnectionString"));
            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));
            services.Configure<AuthenticationSettings>(Configuration.GetSection("AuthenticationSettings"));
        }

        // Configure Dependency Injection for application services configuration
        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            var securityKey = Configuration["SECURITY_KEY"];
            var connectionStringPrintmillManagementDB = CryptoUtility.Decrypt(_appSettings.ConnectionString.PrintmillManagementDB, securityKey);

            services.AddScoped<IAuthenticationDomainServiceProvider, AuthenticationDomainService>();

            services.AddScoped<IEmailServiceProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<EmailServiceAdapter>>();
                var mailSettings = _appSettings.MailSettings;
                return new EmailServiceAdapter(logger, mailSettings, securityKey);
            });

            services.AddScoped<ITicketDomainServiceProvider, TicketDomainService>();
            services.AddScoped<ITicketDataProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<TicketDataAdapter>>();
                return new TicketDataAdapter(connectionStringPrintmillManagementDB, logger);
            });

            services.AddScoped<MaterialDomainService>();
            services.AddScoped<IMaterialDataProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<MaterialDataAdapter>>();
                return new MaterialDataAdapter(connectionStringPrintmillManagementDB, logger);
            });

            services.AddScoped<MaterialTypeDomainService>();
            services.AddScoped<IMaterialTypeDataProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<MaterialTypeDataAdapter>>();
                return new MaterialTypeDataAdapter(connectionStringPrintmillManagementDB, logger);
            });

            services.AddScoped<ItemDomainService>();
            services.AddScoped<IItemDataProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<ItemDataAdapter>>();
                return new ItemDataAdapter(connectionStringPrintmillManagementDB, logger);
            });

            services.AddScoped<IServiceTypeDomainServiceProvider, ServiceTypeDomainService>();
            services.AddScoped<IServiceTypeDataProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<ServiceTypeDataAdapter>>();
                return new ServiceTypeDataAdapter(connectionStringPrintmillManagementDB, logger);
            });

            services.AddScoped<IUserDomainServiceProvider, UserDomainService>();
            services.AddScoped<IUserDataProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<UserDataAdapter>>();
                return new UserDataAdapter(connectionStringPrintmillManagementDB, logger);
            });

            services.AddScoped<IDocumentDomainServiceProvider, DocumentDomainService>();
            services.AddScoped<IDocumentDataProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<DocumentDataAdapter>>();
                return new DocumentDataAdapter(connectionStringPrintmillManagementDB, logger);
            });

            services.AddScoped<IDeliveryDomainServiceProvider, DeliveryDomainService>();
            services.AddScoped<IDeliveryDataProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<DeliveryDataAdapter>>();
                return new DeliveryDataAdapter(connectionStringPrintmillManagementDB, logger);
            });

            services.AddScoped<IDeliveryTicketDataProvider>(sp =>
            {
                var logger = sp.GetService<ILogger<DeliveryTicketDataAdapter>>();
                return new DeliveryTicketDataAdapter(connectionStringPrintmillManagementDB, logger);
            });

            services.AddScoped<IDashboardDomainServiceProvider, DashboardDomainService>();
        }

        // Configure jwt token authentication
        private void ConfigureAuthentication(IServiceCollection services)
        {
            var authenticationSettings = _appSettings.AuthenticationSettings;
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = authenticationSettings.JwtIssuer,
                    ValidAudience = authenticationSettings.JwtIssuer,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authenticationSettings.JwtSecurityKey))
                };
            });
        }

        // Configuration for hosting servers
        private static void ConfigureHostingSettings(IServiceCollection services)
        {
            int limit = 57280000; // up to 55 mb

            services.Configure<KestrelServerOptions>(options =>
            {
                options.Limits.MaxRequestBodySize = limit;  // Limit on request body size
            });

            // TODO - REMOVE IIS CONFIGURATION WHEN MIGRATE TO DOCKER HOSTING
            services.Configure<IISServerOptions>(options =>
            {
                options.MaxRequestBodySize = limit;  // Limit on request body size
            });

            services.Configure<FormOptions>(options =>
            {
                options.ValueLengthLimit = 5000; // Limit on individual form values
                options.MultipartBodyLengthLimit = limit; // Limit on form body size
                options.MultipartHeadersLengthLimit = limit; // Limit on form header size
            });
        }
    }
}
